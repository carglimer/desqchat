let ctx // ctx = $g da ara this
export function initContext(_ctx) {
  ctx = _ctx
}
export function adjustMessage(message) {
  ctx.clog('iad message ', JSON.stringify(message))
  // ctx.clog('ctx.socket=', ctx.socket)
  const user = ctx.store.getters.user
  const card = ctx.store.getters.card
  const receiver = ctx.getReceiver()
  const sender = ctx.getSender()
  ctx.clog('iad user ', JSON.stringify(user))
  if (!receiver) {
    return
  }
  /* if (!receiver.lang) {
    receiver.lang = ctx.gdata.defaultLanguage
  } */
  // message.meta.collName = collName
  // message.meta.intent = intent
  message.meta.status = ctx.enums.status.notSent
  // message.meta.token = user.tokenInfo.token,
  message.meta.sender = sender
  message.meta.receiver = receiver
  // tt2 textData = {senderLang:'de', receiverLang: 'ka', text: 'Haus Tür', translatedText:'სახლი კარი' }
  if (message.data && message.data.textData && message.data.textData.text) {
    message.data.textData.senderLang = sender.lang // ctx.store.getters.locale
    // receiver.lang-s serveri აღარ arkvevs, card-ში მოდის
    message.data.textData.receiverLang = receiver.lang
  }
  if (!message.meta.tsList) {
    const ts = Date.now()
    message.ts = ts
    message.meta.tsList = { [ctx.enums.status.notSent]: ts }
  }
  message.cardId = card.id
  message.senderId = user.id
  message.receiverId = receiver.id
}
// murmanidan mosul user obieqts movargebt chems user obieqts
export function adjustUser(user) {
  if (!user) {
    return null
  }
  const mms = user.params.mediaMaxSize
  return {
    id: user.recId,
    nickname: user.nickName,
    email: user.email,
    mediaMaxSize: {
      orderer: (mms.orderer || mms.otherall).min,
      tester: (mms.tester || mms.otherall).min
    },
    // ourComissionRate: user.params.ourComissionRate,
    // ourVatRate: user.params.ourVatRate,
    ourComission: user.params.ourComission, // -> otherall: { table: 'general_params', rate: '20', min: '0', recId: 2}
    ourVat: user.params.ourVat, // -> otherall: { table: 'general_params', rate: '19', min: '0', recId: 3}
    orderResellOurPrice: user.params.orderResellOurPrice, // -> otherall: { table: 'general_params', rate: '20', min: '12', recId: 5}
    orderResellTesterCommission: user.params.orderResellTesterCommission, // -> otherall: { table: 'general_params', rate: '10', min: '6', recId: 6}

    tokenInfo: {}
  }
}
// minimaluri monacemebi exmapleQuestionnairebis dros
export function adjustUserMMS(user) {
  if (!user) {
    return null
  }
  return {
    id: user.id,
    nickname: null,
    mediaMaxSize: {
      orderer: 200,
      tester: 200,
      handler: 200
    }
  }
}
// murmanidan mosul card obieqts movargebt chems card obieqts
export function adjustCard(_card) {
  if (!_card) {
    return null
  }
  if (!_card.orderer && _card.buyer) {
    _card.orderer = _card.buyer
  }
  let mediaMaxSize = ctx.gdata.mediaMaxSize
  const user = ctx.store.getters.user
  if (user) {
    if (user.id === _card.tester.recId) {
      mediaMaxSize = user.mediaMaxSize.tester
    } else if (user.id === _card.orderer.recId) {
      mediaMaxSize = user.mediaMaxSize.orderer
    }
  }
  const card = {
    id: _card.recId,
    level: _card.level,
    speed: _card.speed, // speed -> ordinary||express||instant aq ar mchirdeba
    link: _card.link,
    vinLasts: _card.vinLasts,
    ordererLocale: _card.ordererLocale,
    testerLocale: _card.testerLocale,
    indicator: {
      chat: {
        pendingMediaSize: 0,
        alreadySentMediaSize: 0
      },
      questionnaire: {
        pendingMediaSize: 0,
        alreadySentMediaSize: 0
      },
      questionnaireStands: {
        actualMustFilledFields: 0,
        actualFilledFields: 0,
        mustFilledFields: 0,
        allFields: 0
      }
    },
    mediaMaxSize,
    // es cardId, orderId notificationistvis schirdeba murmanis mainBack servers
    tester: {
      id: _card.tester.recId,
      cardId: _card.recId,
      orderId: _card.orderId,
      role: 'tester',
      nickname: _card.tester.nickName,
      lang: _card.testerLocale
    },
    orderer: {
      id: _card.orderer.recId,
      cardId: _card.recId,
      orderId: _card.orderId,
      role: 'orderer',
      nickname: _card.orderer.nickName,
      lang: _card.ordererLocale
    }
  }
  if (!_card.orderer && _card.buyer) {
    card.ordererLocale = _card.buyerLocale
    card.buyerLocale = _card.buyerLocale
    card.orderer.lang = _card.buyerLocale
    card.buyer = card.orderer
  }
  // გავიგოთ და დავაყენოთ ჩვენი locale
  adjustLocale(card, user)
  ctx.setCardIndicatorQAllMediaSize(card)
  return card
}
export function adjustLocale(card = ctx.store.getters.card, user = ctx.store.getters.user) {
  // ctx.clog('iad card = ', card)
  if (card && user && !ctx.userIsBuyer(card, user) && !ctx.isExampleQuestionnairePage(card.id)) {
    const locale = ctx.userIsTester(card, user) ? card.tester.lang : card.orderer.lang
    ctx.clog('iad locale = ', locale)
    ctx.updateLocale(locale)
  }
}
export function adjustCardIndicator(_card) {
  if (!_card) {
    return null
  }
  const card = {
    id: _card.id,
    level: _card.level,
    indicator: {
      chat: {
        pendingMediaSize: 0,
        alreadySentMediaSize: 0
      },
      questionnaire: {
        pendingMediaSize: 0,
        alreadySentMediaSize: 0
      },
      questionnaireStands: {
        actualMustFilledFields: 0,
        actualFilledFields: 0,
        mustFilledFields: 0,
        allFields: 0
      }
    }
  }
  return card
}
