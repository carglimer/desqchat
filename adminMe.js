let ctx // ctx = $g da ara this
export function initContext(_ctx) {
  ctx = _ctx
}

export function goToConsolex() {
  // console.log('iam goToConsolex')
  ctx.store.commit('setConsolexDialog', true)
}
// murmanim momca -> tomas fontis ferebia dark da light-istvis
// self aris vue this
export function getDestheme(destheme, self) {
  if (self && self.$vuetify) {
    // console.log('self.$vuetify ', self.$vuetify)
    return self.$vuetify.theme.dark
      ? self.$vuetify.theme.themes.dark[destheme]
      : self.$vuetify.theme.themes.light[destheme]
  }
  return null
}
export function goToRoom() {
  ctx.clog('iam goToRoom')
  ctx.chatEBus.$emit(ctx.enums.events.goToRoom)
  ctx.store.commit('setCard', null)
}
export function goTo(stepNr) {
  // console.log('iam stepNr ', stepNr)
  switch (stepNr) {
    case 1:
      ctx.clog('iam case 1 ')
      goToRoom()
      break
    case 2:
      goToChat()
      break
    case 3:
      goToQuestionnaire()
      break
    case 50:
      goToConsolex()
      break
    default:
      toStepNr(stepNr)
  }
  // ctx.clog('iam ctx.user ', ctx.user)
}
export function goToChat(testCard) {
  ctx.clog('iam testCard ', testCard)
  // ctx.store.commit('setCard', card)
  if (testCard) {
    // ctx.toStepNr(2)
    // gadavecit user und card
    const userId = ctx.store.getters.testUser.recId
    const cardId = testCard.recId
    // es developistvis aris da amitom set null
    ctx.store.commit('setUser', null)
    ctx.store.commit('setCard', null)
    ctx.chatEBus.$emit(ctx.enums.events.goToChat, { userId, cardId, locale: ctx.store.getters.locale })
  } else {
    ctx.goToRoom()
  }
}
export function goToQuestionnaire() {
  ctx.clog('iam goToQuestionnaire')
  const card = ctx.store.getters.card
  if (!card) {
    goToRoom()
    return
  }
  // ctx.toStepNr(3)
  const userId = ctx.store.getters.testUser.recId
  const cardId = card.id
  // es developistvis aris da amitom set null
  ctx.store.commit('setUser', null)
  ctx.store.commit('setCard', null)
  ctx.chatEBus.$emit(ctx.enums.events.goToQuestionnaire, { userId, cardId, locale: ctx.store.getters.locale })
}
export function goToQuestionnaireOrderer() {
  ctx.clog('iam goToQuestionnaireOrderer')
  toStepNr(4)
}
export function toStepNr(n) {
  ctx.clog('iam setStepNr n = ', n)
  ctx.store.commit('setStepNr', n)
}
