const express = require('express')
const app = express()
const bodyParser = require('body-parser')
exports.app = app
// exports.app = app
// const app = module.exports = express();
app.use(bodyParser.json()) // es aucilebelia, ise request.body ar ikitxeboda

app.post('/login', (request, response, next) => {
  console.log('request.body ', request.body)
  if (!request.body) return response.sendStatus(400)
  response.send({
    user: {
      id: '1',
      username: 'jdrydn',
    },
  })
})
