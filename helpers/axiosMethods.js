import axios from 'axios'
import { events, caller, etexts, status } from './enums'

let ctx // ctx = $g da ara this
export function initContext(_ctx) {
  ctx = _ctx
}
// requestObj = { text || textArray, from, to }
export function translateText(requestObj) {
  return new Promise((resolve, reject) => {
    axios
      .post(ctx.gdata.baseUrlAxios + '/translateText', requestObj)
      .then((response) => {
        resolve(response.data)
      })
      .catch((e) => {
        reject(e)
      })
  })
}
// murman -> არსებული ანკეტების გაყიდვის მოდულისათვის შენგან დამჭირდება ასეთი ასინქრონული ფუნქცია: გადმოგეცემა vinLasts (ექვსი სიმბოლო), შენ მიბრუნებ მასივს შემდეგი ობიექტებისას:
// {cardId, vin (სრული), filled: {musts, mustFills, all, fills}}
export function getQStandsArrForCarlinkOrVin6(requestObj) {
  return new Promise((resolve, reject) => {
    axios
      .post(ctx.gdata.baseUrlAxios + '/getQStandsArrForCarlinkOrVin6', requestObj)
      .then((response) => {
        resolve(response.data)
      })
      .catch((e) => {
        reject(e)
      })
  })
}
const forceActual = () => {
  return '?q=' + Date.now()
}
// https://medias.deservice.net/gversions/main_versions.json
// relPath = main_versions.json, relPath = questionnaires/deep/1.0.0.json
export function readActualVersionsRemoteFile(relPath, notCached) {
  // თუ წინ დავუმატებთ forceActual() მაშინ cache-იდან აღარ წამოიღებს და ყოველთვის აქტუალურს წაიკითხავს
  return new Promise((resolve, reject) => {
    axios
      // .get(ctx.gdata.baseUrlMedias + '/gversions/' + relPath)
      .get('/gversions/' + relPath + (notCached ? forceActual() : ''))
      .then((response) => {
        // console.log('aM response.data = ', JSON.stringify(response.data))
        resolve(response.data)
      })
      .catch((e) => {
        reject(e)
      })
  })
}
// contact-is kopmonentidan user-ebis mier gamogzavnisi zaprosebi da survilebi
export const postContactRequest = (requestObj) => {
  // ava testistvis ctx = { gdata: { baseUrlAxios: 'https://qchat.deservice.net' } }
  // console.log('ctx.gdata.baseUrlAxios ', ctx.gdata.baseUrlAxios)
  return new Promise((resolve, reject) => {
    axios
      // nuxt.config-is proxy-shia gadamisamarteba cors-is gamo
      .post(ctx.gdata.baseUrlAxios + '/postContactRequest', requestObj)
      .then((response) => {
        // dabrunda bazidan, rom contactMessage mighebulia
        // console.log('response ', response)
        const rData = response.data
        // console.log('am rData=', rData
        resolve(rData)
      })
      .catch((e) => {
        reject(e)
      })
  })
}
// requestObj) = { cardId: 124, locale } an { cardIdArr: [ 124, 141, 185,.. ], locale }
export const getQuestionnaireValuesByCardId = (requestObj) => {
  // console.log('am ctx=', ctx)
  return new Promise((resolve, reject) => {
    axios
      // nuxt.config-is proxy-shia gadamisamarteba cors-is gamo
      .post(ctx.gdata.baseUrlAxios + '/getQValuesByCardId', requestObj)
      .then((response) => {
        // dabrunda bazidan questionnaireValues
        const questionnaireValuesArr = response.data
        ctx.clog('am questionnaireValuesArr =', questionnaireValuesArr)
        if (!questionnaireValuesArr || questionnaireValuesArr === etexts.noQuestionnaireFound) {
          // reject(questionnaireValuesArr)
          resolve(null)
          return
        }
        if (ctx.isString(questionnaireValuesArr) && questionnaireValuesArr.includes('<!doctype html>')) {
          // reject(questionnaireValuesArr)
          resolve(null)
          return
        }
        // aq values iko JSON.stringify-d da serverma ukve ukan gadmoparsa
        if (ctx.isArray(requestObj.cardId)) {
          // mashin array-s itxovs. gvaxsovdes, rom arrays dros satargnmic rom ikos ar itargmneba.
          resolve(questionnaireValuesArr)
        } else {
          // satargnmi tu aris, comments itargmneba da kovel questionItems emateba translatedComment
          resolve(questionnaireValuesArr[0])
        }
      })
      .catch((err) => {
        ctx.clog('am catch e: ', err)
        ctx.chatEBus.$emit(ctx.enums.events.networkError, err)
        ctx.showSnackbar(err, 2000, 'red', ctx.enums.caller.questionnaire)
        reject(err)
      })
  })
}
// vekitxebi chems servers da imas moaqvs murmanis back-idan
// data aris an user an card, requestObj = { userId: 15 } { userId: [6, 15] }, { cardId: 15 } { cardId: [6, 15] }
// sheizleba gamovikeno rogorc promise daa aseve rogorc callback
export const getDataById = (requestObj) => {
  // ava testistvis ctx = { gdata: { baseUrlAxios: 'https://qchat.deservice.net' } }
  return new Promise((resolve, reject) => {
    axios
      // nuxt.config-is proxy-shia gadamisamarteba cors-is gamo
      .post(ctx.gdata.baseUrlAxios + '/getDataById', requestObj)
      .then((response) => {
        // console.log('am response =', response)
        // dabrunda bazidan user an card
        const rData = response.data
        // console.log('am rData=', rData)
        resolve(rData)
      })
      .catch((e) => {
        // console.log('am e=', e)
        reject(e)
      })
  })
}
// Qmedia-ს წაშლა შეიძლება თუ სერვერზეა მაშინაც, როცა ამას ჩექერი აკეთებს
// requestObj = { toRemove: '/questionnaires/1620597851020_1874893.jpeg', qValues: { _id: '60942c64de5a355f3cdb344f', cardId: 17, ... values: '{"1":{},"2"..'}}
export const removeQMediaOnMongo = (requestObj) => {
  console.log('aM removeQMediaOnMongo requestObj = ', requestObj)
  return new Promise((resolve, reject) => {
    axios
      // nuxt.config-is proxy-shia gadamisamarteba cors-is gamo
      .post(ctx.gdata.baseUrlAxios + '/removeQMediaOnMongo', requestObj)
      .then((response) => {
        if (response && response.data && response.data.success) {
          resolve()
        } else {
          reject(response)
        }
      })
      .catch((e) => {
        reject(e)
      })
  })
}
// igive async-it
export const removeQMediaOnMongo2 = async (requestObj) => {
  // console.log('aM removeQMediaOnMongo requestObj = ', requestObj)
  try {
    // nuxt.config-is proxy-shia gadamisamarteba cors-is gamo
    const response = await axios.post(ctx.gdata.baseUrlAxios + '/removeQMediaOnMongo', requestObj)
    if (response && response.data && response.data.success) {
      return
    } else {
      throw new Error(response)
    }
  } catch (e) {
    throw new Error(e)
  }
}
// chat da questionnaire xmarobs. chat socket-it aketebs, magram alternativad esec aris
export const uploadMedia = ({ message, blob }) => {
  // const callbacks = message.callbacks
  let _blob = message.data.blob
  if (blob) {
    _blob = blob
  }
  // const { callbacks, ...msg } = message
  delete message.data
  const ts = message.meta.tsList[status.notSent]
  const postUrl = message.meta.caller === caller.questionnaire ? '/uploadMediaQuestionnaire' : '/uploadMediaChat'
  const uploadProgressEvent =
    message.meta.caller === caller.questionnaire
      ? events.questionnaireMediaUploadProgress
      : events.chatMediaUploadProgress
  const formData = new FormData()
  // payload object string-ad unda gaigzavnos
  formData.append('message', JSON.stringify(message))
  formData.append('blob', _blob, _blob.name) // es blob.name aucileblad bolos unda ikos
  const test = false
  if (test) {
    return new Promise((resolve, reject) => {
      if (test) {
        reject(new Error('testError'))
      } else resolve({ result: 'done', meta: {} })
    })
  }
  return new Promise((resolve, reject) => {
    axios
      .post(ctx.gdata.baseUrlAxios + postUrl, formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        onUploadProgress: (progressEvent) => {
          // console.log('am progressEvent ', progressEvent)
          const uploadPercentage = Math.round((progressEvent.loaded * 100) / progressEvent.total)
          ctx.chatEBus.$emit(uploadProgressEvent, { ts, progress: uploadPercentage })
        }
      })
      .then((res) => {
        ctx.chatEBus.$emit(uploadProgressEvent, { ts, progress: 100, done: true })
        // res.data: {result: "done", meta: {size: 58761, ext: "jpeg", type: "image", tsList: {…}, relPath: "/docs/1600609099979_58761.jpeg"}}
        resolve(res.data)
      })
      .catch((e) => {
        reject(e)
      })
  })
}

export const getInvoiceByCardIdAndServiceType = (requestObj) => {
  // requestObj = { cardId, serviceType, admin }
  ctx.clog('am getInvoiceByCardIdAndServiceType requestObj = ', requestObj)
  return new Promise((resolve, reject) => {
    axios
      .post(ctx.gdata.baseUrlAxios + '/getInvoiceByCardIdAndServiceType', requestObj)
      .then((resp) => {
        ctx.clog('aM resp ', resp) // resp.data = invoice
        if (resp.data === etexts.noInvoiceFound) {
          resolve(null)
        } else resolve(resp.data)
      })
      .catch((err) => {
        reject(err)
      })
  })
}
export const uploadInvoice = (invoice) => {
  ctx.clog('am uploadInvoice invoice = ', invoice)
  return new Promise((resolve, reject) => {
    axios
      .post(ctx.gdata.baseUrlAxios + '/uploadInvoice', invoice)
      .then((resp) => {
        ctx.clog('aM resp ', resp) // resp.data = { update: false||true, insertedId: upsertedId||null }
        resolve(resp.data)
      })
      .catch((err) => {
        reject(err)
      })
  })
}
// invoiceData = { cardId, lang }
export const downloadInvoice = (invoiceData) => {
  const options = {
    // params: { access_token: access_token },
    headers: { 'Content-Type': 'application/json', Accept: 'application/pdf' },
    responseType: 'arraybuffer' // responseType: 'blob'
  }
  return new Promise((resolve, reject) => {
    axios
      .post(ctx.gdata.baseUrlAxios + '/downloadInvoice', invoiceData, options)
      .then((resp) => {
        ctx.clog('aM resp ', resp)
        ctx.clog('aM resp.data.toString ', resp.data.toString())
        if (/pdf$/gi.test(resp.headers['content-type'])) {
          // headers: { content-type: "invNr/pdf" }
          const contentType = resp.headers['content-type']
          const invNr = contentType.substr(0, contentType.length - 4)
          const downloadUrl = window.URL.createObjectURL(new Blob([resp.data], { type: 'application/pdf' }))
          const link = document.createElement('a')
          link.href = downloadUrl
          link.setAttribute('download', 'invoice' + invNr + '.pdf') // any other extension
          document.body.appendChild(link)
          link.click()
          link.remove()
          resolve()
        } else {
          // error

          let text = JSON.parse(Buffer.from(resp.data).toString('utf8'))
          ctx.clog('aM err text = ', text)
          if (text && text.error) {
            text = text.error
          }
          reject(text)
        }
      })
      .catch((err) => {
        reject(err)
      })
  })
}
export const uploadQuestionnaire = (questionnaireValues) => {
  // ctx.clog('aM questionnaireValues=', questionnaireValues)
  // questionnaireValues aq dakopirebuli modis
  // const questionnaireValuesCopy = ctx.cloneObj(questionnaireValues)
  // cavshalot openArr, radgan mongo-shi aghar gvchirdeba
  delete questionnaireValues.openArr
  // shevinaxot questionnaireValues.values rogorc string, radgan mongo-shi key-shi certili ar sheizleba
  questionnaireValues.values = JSON.stringify(questionnaireValues.values)
  let postUrl = '/uploadQuestionnaire'
  if (questionnaireValues.cardId < 0) {
    postUrl = '/uploadExampleQuestionnaire'
  }
  return new Promise((resolve, reject) => {
    axios
      .post(ctx.gdata.baseUrlAxios + postUrl, questionnaireValues)
      .then((response) => {
        // ctx.clog('aM response ', response)
        if (response && response.data) {
          if (response.data.err) {
            reject(response.data.err)
          } else {
            resolve(response.data.questionnaireId)
          }
        }
      })
      .catch((err) => {
        reject(err)
      })
  })
}

export const downloadFile = (fileData) => {
  const options = {
    // params: { access_token: access_token },
    headers: { 'Content-Type': 'application/json', Accept: 'application/pdf' },
    responseType: 'arraybuffer' // responseType: 'blob'
  }
  return new Promise((resolve, reject) => {
    axios
      .post(ctx.gdata.baseUrlAxios + '/downloadFile', fileData, options)
      .then((resp) => {
        ctx.clog('aM resp ', resp)
        resolve(resp)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

// pendingMessage =  message
export const sendPendingMessage = (pendingMessage, handleSuccess, handleErr) => {
  // ctx.clog('aM pendingMessage=', pendingMessage)
  axios
    .post(ctx.gdata.baseUrlAxios + '/sendPendingMessage', pendingMessage)
    .then((response) => {
      // ctx.clog('aM response ', response)
      if (handleSuccess) {
        // movida message.meta
        handleSuccess(pendingMessage, response.data)
      }
    })
    .catch((e) => {
      if (handleErr) {
        handleErr(pendingMessage, e)
      }
    })
}

// requestObj) = { cardId: 124 } or { cardIdArr: [ 124, 141, 185,.. ] }
/* export const getQuestionnaireValuesByCardId2 = (requestObj, handleSuccess, handleErr) => {
  // console.log('requestObj=', requestObj)
  // es testia if (requestObj) { return }
  axios
    // nuxt.config-is proxy-shia gadamisamarteba cors-is gamo
    .post(ctx.gdata.baseUrlAxios + '/getQValuesByCardId', requestObj)
    .then((response) => {
      // dabrunda bazidan questionnaireValues
      const questionnaireValuesArr = response.data
      ctx.clog('am questionnaireValuesArr2 =', questionnaireValuesArr)
      if (!questionnaireValuesArr || questionnaireValuesArr === etexts.noQuestionnaireFound) {
        if (handleErr) {
          handleErr(questionnaireValuesArr)
        }
        return
      }
      for (const questionnaireValues of questionnaireValuesArr) {
        // aq values aris JSON.stringify-d da ukan unda gardavqmnat
        questionnaireValues.values = JSON.parse(questionnaireValues.values)
        ctx.clog('am questionnaireValues.values =', questionnaireValues.values)
      }
      if (handleSuccess) {
        if (requestObj.cardId) {
          handleSuccess(questionnaireValuesArr[0])
        } else {
          // mashin array-s itxovs
          handleSuccess(questionnaireValuesArr)
        }
      }
    })
    .catch((e) => {
      if (handleErr) {
        handleErr(e)
      }
    })
} */
// vekitxebi chems servers da imas moaqvs murmanis back-idan
// data aris an user an card requestObj = {userId||cardId}
export const getDataById2 = (requestObj, handleSuccess, handleErr) => {
  ctx.clog('am requestObj', JSON.stringify(requestObj))
  // es testia
  // if (requestObj) { return }
  axios
    // nuxt.config-is proxy-shia gadamisamarteba cors-is gamo
    .post(ctx.gdata.baseUrlAxios + '/getDataById', requestObj)
    .then((response) => {
      // dabrunda bazidan messageList
      const rData = response.data
      // console.log('am rData=', rData)
      if (handleSuccess) {
        handleSuccess(rData)
      }
    })
    .catch((e) => {
      if (handleErr) {
        handleErr(e)
      }
    })
}
export const uploadInvoice2 = (invoice, handleSuccess, handleErr) => {
  axios
    .post(ctx.gdata.baseUrlAxios + '/uploadInvoice', invoice)
    .then((response) => {
      ctx.clog('aM response ', response)
      // ctx.clog('aM response ', response)
      if (response && response.data) {
        if (response.data.err && handleErr) {
          handleErr(response.data.err)
        } else if (handleSuccess) {
          handleSuccess(response.data.invoiceId)
        }
      }
    })
    .catch((err) => {
      if (handleErr) {
        handleErr(err)
      }
    })
}
// authenticate-shi sign = 'login' or 'signup'
export const authenticate = (userData, handleSuccess, handleError) => {
  if (!userData || !userData.meta || !userData.meta.intent) {
    // ctx.clog('aM userData false, meta not exists! ')
    return
  }
  const sign = userData.meta.intent
  // ctx.clog('loginUrl= '+ process.env.loginUrl)
  // ctx.clog('aM userData ', JSON.stringify(userData))
  axios
    .post(ctx.gdata.baseUrlAxios + '/' + sign, userData)
    .then((response) => {
      // ctx.clog('aM response.data ', JSON.stringify(response.data))
      /*
      // dabrunda bazidan { 'user_returned': true, user, tokenInfo }
      {"result":"user_returned", an 'user_created'
      "user":{
        "_id":"5d6ae3b14f4c182674954eea",
        "meta":{"collName":"users","type":"text","intent":"signup","ts":1567286192871},
        "data":{"username":"ert","email":"gia-lomidze@web.de","name":"Fisher","salutation":"Herr","tel":"017794654678","whatsapp":"0177946522","salt":"9879504383"},
        "chatData":{"senderLang":"de","suggestions":["Guten Tag, was kann ich für Sie tun ?"],"avatarSrc":"http://localhost:4128/tfotos/5d6ae3b14f4c182674954eea/avatar/1567600555465_14285.png","chatetName":"Herr Lomidze ori"}
        "tokenInfo":{"expiresIn":3600,"token":"eyJhbGciOiJIUzI1NiIs..."}}
      },
      'serverData':{"fileSendMode":"siofu","chatUrl":"http://localhost:4129","imageSrcBase":"http://localhost:4128/tfotos/","avatarSrcDefault":"http://localhost:4128/avatar_default/avatar.png","messageTsFormat":"HH:mm","uploadFileMaxSize":2,"uploadFileChunkSize":1024000,"fotosUploadUrl":"http://localhost:4128/uploadFoto","flagsImageBase":"http://localhost:4128/flags/"}
      */

      if (handleSuccess) {
        handleSuccess(response.data)
      }
    })
    .catch((err) => {
      if (sign === 'signup') {
        handleError(err, handleSuccess)
      } else {
        handleError(err)
      }
    })
}
export const uploadQuestionnaire2 = (questionnaireValues, handleSuccess, handleErr) => {
  // ctx.clog('aM questionnaireValues=', questionnaireValues)
  // davakopirot questionnaireValues da
  // shevinaxot questionnaireValues.values rogorc string, radgan mongo-shi key-shi certili ar sheizleba
  const questionnaireValuesCopy = JSON.parse(JSON.stringify(questionnaireValues))
  // cavshalot openArr, radgan mongo-shi aghar gvchirdeba
  delete questionnaireValuesCopy.openArr
  questionnaireValuesCopy.values = JSON.stringify(questionnaireValuesCopy.values)
  axios
    .post(ctx.gdata.baseUrlAxios + '/uploadQuestionnaire', questionnaireValuesCopy)
    .then((response) => {
      // ctx.clog('aM response ', response)
      if (response && response.data) {
        if (response.data.err && handleErr) {
          handleErr(response.data.err)
        } else if (handleSuccess) {
          handleSuccess(response.data.questionnaireId)
        }
      }
    })
    .catch((err) => {
      if (handleErr) {
        handleErr(err)
      }
    })
}
export const uploadMedia2 = (message) => {
  const callbacks = message.callbacks
  const blob = message.data.blob
  delete message.baseUrlAxios
  delete message.callbacks
  delete message.data

  const formData = new FormData()
  // payload object string-ad unda gaigzavnos
  formData.append('message', JSON.stringify(message))
  formData.append('blob', blob, blob.name) // es blob.name aucileblad bolos unda ikos
  axios
    .post(ctx.gdata.baseUrlAxios + '/uploadMedia', formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      onUploadProgress: (progressEvent) => {
        const uploadPercentage = Math.round((progressEvent.loaded * 100) / progressEvent.total)
        callbacks.handleUploadMediaPercentage(uploadPercentage)
        // ctx.clog('aM uploadPercentage= ' + uploadPercentage)
        // ctx.uploadPercentage = parseInt( pr )
      }
    })
    .then((res) => {
      // res.data: {result: "done", meta: {size: 58761, ext: "jpeg", type: "image", tsList: {…}, relPath: "/docs/1600609099979_58761.jpeg"}}
      if (callbacks.handleUploadMediaSuccess) {
        // afterUploadCallback -> to bolo suratia an/da gvainteresebs rodis damtavrda misi atvirtva
        // ctx.clog('res = ', res)
        callbacks.handleUploadMediaSuccess(res.data)
      }
    })
    .catch((e) => {
      if (callbacks.handleUploadMediaError) {
        callbacks.handleUploadMediaError(message, e)
      }
    })
}
export const sendEmail = (user, handleSuccess, handleErr) => {
  // baseUrl = process.env.baseUrl + '/auth'
  const baseUrl = user.baseUrl
  // delete baseUrl key
  delete user.baseUrl
  // ctx.clog('aM email=', user.data.email)
  /* 
  user =  {"_id":"5da22550688838112289f77c",
    "meta":{"collName":"users","type":"text","intent":"signup","ts":1570907478745},
    "data":{"username":"ttwebde","email":"tt@web.de","name":null,"salutation":"Herr","telefon":"017898798778","whatsapp":"gia-lomidze@web.de","contactType":"Telefon","salt":"2648596041"},
    "chatData":{"senderLang":"de"},
    "tokenInfo":{"expiresIn":3600,"token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZGEyMjU1MDY4ODgzODExMjI4OWY3N2MiLCJpYXQiOjE1NzEwODg0MDEsImV4cCI6MTU3MTA5MjAwMX0.C45JpK2KtxKL1IX0OJZTSw6wDM7P3tQc7lz9QjAfM_Q","tokenExpiration":1571092002343}}
  */
  const confirmation = {
    email: user.data.email,
    lang: user.chatData.senderLang,
    subject: 'confirmation'
  }
  axios
    .post(baseUrl + '/sendEmail', confirmation)
    .then((response) => {
      // ctx.clog('aM response.data ', response.data)
      // dabrunda bazidan
      if (handleSuccess) {
        handleSuccess(response.data)
      }
    })
    .catch((e) => {
      if (handleErr) {
        handleErr(e)
      }
    })
}
