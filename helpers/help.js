let clicks = 0
let timer
const timeout = 300
export function sevenClick(sevenClick) {
  clearTimeout(timer)
  clicks++
  // console.log('this.clicks = ', this.clicks);
  timer = setTimeout(function () {
    // console.log('clicks = ', clicks)
    if (clicks === 7) {
      // console.log('11 doubleClick')
      if (sevenClick) {
        sevenClick()
      }
    }
    clicks = 0
  }, timeout)
}
export function multiClick(doubleClick, tripleClick, tetraClick) {
  clearTimeout(timer)
  clicks++
  // console.log('this.clicks = ', this.clicks);
  timer = setTimeout(function () {
    // console.log('clicks = ', clicks)
    if (clicks === 2) {
      // console.log('11 doubleClick')
      if (doubleClick) {
        doubleClick()
      }
    }
    if (clicks === 3) {
      // console.log('tripleClick')
      if (tripleClick) {
        tripleClick()
      }
    }
    if (clicks === 4) {
      // console.log('tetraClick')
      if (tetraClick) {
        tetraClick()
      }
    }
    clicks = 0
  }, timeout)
}
export function slugify(name) {
  return name
    .toString()
    .toLowerCase()
    .trim()
    .replace(/\s+/g, '-') // Replace spaces with -
    .replace(/&/g, '-and-') // Replace & with 'and'
    .replace(/[^\w-]+/g, '') // Remove all non-word chars
    .replace(/--+/g, '-') // Replace multiple - with single -
}
export const suggestions = ['text1', 'openChatAcceptDialog', 'text3', 'text4']
export const chatColors = {
  // ხედა შუბლის ფონი და ტექსტი
  header: {
    bg: '#4e8cff',
    text: '#ffffff'
  },
  // დასაქოქი ღილაკის ფონი
  launcher: {
    bg: '#4e8cff'
  },
  // ძირითადი ფონი
  messageList: {
    bg: '#ffffff'
  },
  // ჩემი გაგზავნილის ფონი
  sentMessage: {
    bg: '#4e8cff',
    text: '#ffffff',
    messageNotReceived: 'var(--v-des_bg_less2-base, #5f5f5f)',
    messageIsReceived: 'success'
  },
  systemMessage: {
    bg: 'var(--v-des_bg_system-base, #B3E5FC)',
    text: 'black'
  },
  // მიღებულის ფონი
  receivedMessage: {
    bg: '#eaeaea',
    text: '#222222'
  },
  // ქვედა ჩასაწერი ველის ფონი და ტექსტი
  userInput: {
    bg: '#f4f7f9',
    text: '#565867'
  }
}
