const io = require('socket.io-client')
let payload = null
let g
// socket, callback, message, store, setSocket
export function initSocket(_payload) {
  payload = _payload
  g = payload.g
  g.clog('iS initSocket payload = ', payload)
  const store = g.store
  let socket = g.socket
  // let socket = g.socket
  const setSocket = g.setSocket
  g.clog('iS g.socket ', g.socket)
  // this.$g.clog('data: ', JSON.stringify({socket:this.socket!==null,isAdmin:this.$g.store.getters.isAdmin,token:this.$g.store.getters.token,user:this.$g.store.getters.user}))
  if (socket) {
    g.clog('iS socket.connected = ', socket.connected)
    if (socket.connected) {
      handleConnectCallbeck(payload)
      return
    }
    g.clog('iS socket disconnected ')
    // tu socket arsebobs is tavistavad cdilobs sheertebas da ganmeorebit aghar aris sachiro misi sheqmna
    // magram amasobashi iqneb locale shecvala da amitom jobia xelaxla davqovot socket
    g.removeSocket()
  }
  handleDisconnectCallbeck()
  g.clog('iS create new socket ')
  const user = store.getters.user
  // const locale = store.getters.locale
  if (!user) {
    g.showSnackbar(g.t('qchat.snackbar.user_is_null_not_defined'), 3000, 'error', g.enums.caller.chat)
    return
  }
  // const queryObj = { userId: user.id, lang: locale, tokenoff: true }
  const queryObj = { userId: user.id, tokenoff: true }
  // let queryText = 'userId=' + user.id + '&' + 'lang=' + locale
  if (user.tokenInfo) {
    // queryText += '&' + 'token=' + user.tokenInfo.token
    queryObj.token = user.tokenInfo.token
  }
  // mxolod satestod, mashin token aghar schirdeba
  // if (process.env.tokenoff) {
  // queryText += '&tokenoff=true'
  // }
  g.clog('iS queryObj ', queryObj)
  const query = { query: queryObj }
  // this.$g.clog('iS this.$g.store.getters.user.serverData.chatUrl ', this.$g.store.getters.user.serverData.chatUrl)
  socket = io(g.gdata.chatUrl, query)
  g.clog('iS socket = ', socket)
  payload.initSocketListeners && payload.initSocketListeners(socket)
  setSocket(socket)
  // davqoqot socket.io-stream-is event listener
  // es listener mxolod ertxel unda mienichos, amitom aq jobia ikos
  /* payload.socketIoStream.getFileStream(
    socket,
    payload.socketIoStream.errorHandler,
    payload.socketIoStream.successHandler
  ) */
  // this.socket.connect() da mere callback()
  initSocketHandler() // ({ socket, callback, message, initSocketListeners, store })
}
function handleConnectCallbeck() {
  const params = payload.params
  g.clog('iS handleConnectCallbeck params= ', params)
  if (payload.callback) {
    payload.callback(params)
  }
}
// es qoqavs chatCore-shi loadReceiverAllMessages(true)
function handleDisconnectCallbeck() {
  if (payload.connectionErrorCallback) {
    payload.connectionErrorCallback(true)
  }
}
// socket, callback, message, initSocketListeners
function initSocketHandler() {
  const socket = g.socket
  const store = g.store
  socket.on('connect', () => {
    g.clog('iS connect payload = ', payload)
    handleConnectCallbeck(payload)
    store.commit('setSocketArrowColor', 'green darken-4') // success იყო
    // radgan socket sheertda, tu aris pendingMessages gavagzavnot
    payload.sendPendingMessages()
  })
  // Fired upon a connection error
  socket.on('connect_error', (err) => {
    payload.g.clog('iS connect_error', err)
    // payload.g.clog('iS connect_error socket = ', socket)
    store.commit('setSocketArrowColor', 'error')
  })
  socket.on('connect_timeout', () => {
    payload.g.clog('iS connect_timeout')
    store.commit('setSocketArrowColor', 'error')
  })
  socket.on('error', (err) => {
    payload.g.clog('iS err ', err)
    store.commit('setSocketArrowColor', 'error')
  })
  socket.on('disconnect', () => {
    payload.g.clog('iS disconnect')
    store.commit('setSocketArrowColor', 'error')
  })
  socket.on('reconnect', () => {
    payload.g.clog('iS reconnect')
    // chatEBus.$emit(cbt.SNACKBAR_INFO, { text: 'reconnected', timeout, color: 'success' })
    store.commit('setSocketArrowColor', 'success')
  })
  socket.on('reconnect_attempt', () => {
    // payload.g.clog('iS reconnect_attempt') <- mushaobs
    store.commit('setSocketArrowColor', 'yellow')
  })
  socket.on('reconnecting', () => {
    // payload.g.clog('iS reconnecting')
    store.commit('setSocketArrowColor', 'yellow')
  })
  socket.on('reconnect_error', () => {
    // payload.g.clog('iS reconnect_error') <- mushaobs
    store.commit('setSocketArrowColor', 'yellow')
  })
  socket.on('reconnect_failed', () => {
    // payload.g.clog('iS reconnect_failed')
    // chatEBus.$emit(cbt.SNACKBAR_INFO, { text: 'reconnect_failed', timeout, color: 'error' })
    store.commit('setSocketArrowColor', 'error')
  })
}
