// import { isImage, isVideo, isAudio, getExtensionFromFilename, getTsAndSizeFromFilename } from '../helpers/methods'
let ctx // ctx = $g da ara this
export function initContext(_ctx) {
  ctx = _ctx
}

export function getMediaMaxSize(card) {
  return card.mediaMaxSize * 1000000
}
function countMediaSizes() {
  const card = ctx.store.getters.card
  const alreadySentMediaSize =
    +card.indicator.chat.alreadySentMediaSize + +card.indicator.questionnaire.alreadySentMediaSize || 0
  const pendingMediaSize = +card.indicator.chat.pendingMediaSize + +card.indicator.questionnaire.pendingMediaSize || 0
  return { alreadySentMediaSize, pendingMediaSize }
}
export function allMediaSizes() {
  const mediaSizes = countMediaSizes()
  return mediaSizes.pendingMediaSize + mediaSizes.alreadySentMediaSize
}

function fileSizeError(payload) {
  /* 
    payload = { file, questionnaireIndex }
    file = 
    lastModified: 1561198961745
    name: "57335dab-5eb0-47af-9f69-138e6c46fb4a.jpg"
    size: 382948
    type: "image/jpeg"
    webkitRelativePath: ""
    */
  // const card = ctx.store.getters.card
  // ctx.clog('idb card.indicator ', card.indicator)
  // ctx.clog('idb mediaSizes ', countMediaSizes())
  ctx.clog('idb payload ', payload)
  if (!payload || !payload.file) {
    return true
  }
  const _caller = payload.questionnaireIndex ? ctx.enums.caller.questionnaire : ctx.enums.caller.chat
  const type = payload.file.type
  const size = payload.file.size
  let mediaMaxSize = ctx.gdata.fileMaxSize
  if (type.includes('audio')) {
    mediaMaxSize = ctx.gdata.audioMaxSize
  } else if (type.includes('video')) {
    mediaMaxSize = ctx.gdata.videoMaxSize
  } else if (type.includes('image')) {
    mediaMaxSize = ctx.gdata.imageMaxSize
  }
  if (size > mediaMaxSize * 1000000) {
    const errorText = ctx.t('qchat.snackbar.file_size_restriction').replace('xxx', mediaMaxSize)
    ctx.showSnackbar(errorText, 5000, 'error', _caller)
    return true
  } else {
    return false
  }
}
function allMediaSizeError(fileSize, caller) {
  const card = ctx.store.getters.card
  const mediaMaxSize = card.mediaMaxSize
  const mediaSizes = countMediaSizes()
  const size = mediaSizes.pendingMediaSize + mediaSizes.alreadySentMediaSize
  // const pendingMediaSize = +card.indicator.chat.pendingMediaSize + +card.indicator.questionnaire.pendingMediaSize
  /* ctx.clog('idb card.indicator ', card.indicator)
  ctx.clog('idb mediaMaxSize ', mediaMaxSize)
  ctx.clog('idb mediaSizes ', mediaSizes)
  ctx.clog('idb fileSize ', fileSize)
  ctx.clog('idb size ', size) */
  const sizeError = +fileSize + +size > +mediaMaxSize * 1000000
  ctx.clog('idb sizeError ', sizeError)
  if (sizeError) {
    const errorText = ctx.t('qchat.snackbar.file_size_restriction').replace('xxx', mediaMaxSize)
    ctx.showSnackbar(errorText, 5000, 'error', caller)
  }
  return sizeError
}
// filetype = image/jpeg
function splitTypeAndExt(filetype) {
  if (!filetype || !filetype.includes('/')) {
    return null
  }
  const arr = filetype.split('/')
  return { type: arr[0], ext: arr[1] }
}

function createPayloadAndResolve(media, payload, resolve) {
  // media.ts = Date.now()
  media.tsList = { [ctx.enums.status.notSent]: Date.now() }
  const payload2 = {
    cardId: ctx.store.getters.card.id,
    mediaType: media.type,
    sent: ctx.enums.status.notSent,
    media
  }
  if (payload.questionnaireIndex) {
    payload2.questionnaireIndex = payload.questionnaireIndex
    payload2.caller = ctx.enums.caller.questionnaire
  } else payload2.caller = ctx.enums.caller.chat
  resolve(payload2)
}
function addMediaFromPc(payload, type) {
  return new Promise((resolve, reject) => {
    // payload = { file, questionnaireIndex, maxSize }
    ctx.clog('idb addVideoFromPc payload.file = ' + payload.file)
    const file = payload.file
    ctx.clog('idb file.type = ', file.type)
    const media = {}
    const typeAnExt = splitTypeAndExt(file.type)
    if (typeAnExt) {
      media.type = typeAnExt.type
      media.ext = typeAnExt.ext
    } else {
      media.type = type
      media.ext = ctx.getExtensionFromFilename(file.name)
    }
    media.size = file.size
    media.blob = file
    createPayloadAndResolve(media, payload, resolve)
  })
}
export function setFiles({ files, questionnaireIndex, getItemMedias }) {
  const _caller = questionnaireIndex ? ctx.enums.caller.questionnaire : ctx.enums.caller.chat
  let allSize = 0
  for (const file of files) {
    allSize += file.size
  }
  // თუ ფაილების ზომების ჯამი არ აღემატება maMediaSize-ს
  if (!allMediaSizeError(allSize, _caller)) {
    for (const file of files) {
      const payload = { file }
      if (questionnaireIndex) {
        payload.questionnaireIndex = questionnaireIndex
      }
      // chavceret idb-shi
      setFile({ file, questionnaireIndex })
        .then((mediaType) => {
          if (getItemMedias) {
            // mashin questionnaire-ia
            getItemMedias()
          } else {
            getMedias()
          }
        })
        .catch((err) => {
          ctx.clog('idb setFiles err = ', err)
        })
    }
  }
}
// pc-dan file-bis amorchevisas
export function setFile(payload) {
  /* 
    payload = { file, questionnaireIndex }
    file = 
    lastModified: 1561198961745
    name: "57335dab-5eb0-47af-9f69-138e6c46fb4a.jpg"
    size: 382948
    type: "image/jpeg"
    webkitRelativePath: ""
    */
  ctx.clog('idb setFile payload = ', payload)
  return new Promise((resolve, reject) => {
    // gadavamocmot failis sidide
    if (fileSizeError(payload)) {
      reject(new Error('fileSizeError'))
      return
    }
    const file = payload.file
    ctx.clog('idb file ', file)
    let type = 'file'
    if (ctx.isImage(file)) {
      type = 'image'
    } else if (ctx.isVideo(file)) {
      type = 'video'
    } else if (ctx.isAudio(file)) {
      type = 'audio'
    }
    if (type !== 'file') {
      ctx.clog('idb addMediaFromPc type =', type)
      addMediaFromPc(payload)
        .then((payload2) => addMedia(payload2))
        .then(() => resolve(type))
        .catch((err) => {
          addMediaError(err)
        })
    } else {
      // type = file, mag. test.txt
      ctx.clog('idb addMedia ', file)
      const _caller = payload.questionnaireIndex ? ctx.enums.caller.questionnaire : ctx.enums.caller.chat
      const ext = ctx.getExtensionFromFilename(file.name)
      const filename = file.name
      const payload2 = {
        cardId: ctx.store.getters.card.id,
        mediaType: type,
        sent: ctx.enums.status.notSent,
        media: {
          blob: file,
          tsList: { [ctx.enums.status.notSent]: Date.now() },
          size: file.size,
          type,
          ext,
          filename
        },
        caller: _caller
      }
      if (payload.questionnaireIndex) {
        payload2.questionnaireIndex = payload.questionnaireIndex
      }
      addMedia(payload2)
        .then(() => resolve(type))
        .catch((err) => {
          addMediaError(err)
        })
    }
  })
}
function addMediaError(err) {
  ctx.clog('err addMediaError = ', err)
}
export function addMedia(payload) {
  return new Promise((resolve, reject) => {
    ctx.clog('idb addMedia payload = ', payload)
    if (allMediaSizeError(payload.media.size, payload.caller)) {
      reject(new Error('allMediaSizeError'))
      return
    }
    const sent = payload.sent
    const media = payload.media
    const request = indexedDB.open(ctx.gdata.dbName, 1)
    request.onsuccess = (ev) => {
      const db = ev.target.result
      let transaction
      let store
      let addData
      if (payload.caller === ctx.enums.caller.chat) {
        // mashin chatia
        transaction = db.transaction('chatMedias', 'readwrite')
        store = transaction.objectStore('chatMedias')
        addData = { ts: media.tsList.notSent, cardId: payload.cardId, sent, media }
      }
      // mashin questionnaire-ia
      else {
        transaction = db.transaction('questionnaireMedias', 'readwrite')
        store = transaction.objectStore('questionnaireMedias')
        addData = {
          ts: media.tsList.notSent,
          cardId: payload.cardId,
          // uploaded: 0,
          questionnaireIndex: payload.questionnaireIndex,
          sent,
          media
        }
      }
      // chavceret
      store.add(addData)
      // data.forEach(el => store.add(el))
      transaction.onerror = (ev) => {
        console.error('idb Ein Fehler ist aufgetreten!', ev.target.error.message)
        reject(ev.target.error.message)
      }
      transaction.oncomplete = (ev) => {
        ctx.clog('idb Daten wurden erfolgreich hinzugefügt!')
        if (payload.questionnaireIndex) {
          // anketis shemtxvevashi vitvlit AllQMediaSize-s
          setCardIndicatorQAllMediaSize()
        }
        resolve()
      }
    }
  })
}

// payload = { questionnaireIndex, downloaded, itemValue }
export function getMedias(payload) {
  ctx.clog('idb getMedias payload: ', payload)
  if (payload && payload.questionnaireIndex) {
    getMediasForQuestionnaire(payload)
  } else {
    getMediasForChat()
  }
}
function getMediasForChat() {
  if (!ctx.store.getters.card) {
    return
  }
  const cardId = ctx.store.getters.card.id
  const payload = { cardId }
  readMedias(payload)
    .then((idbMedias) => {
      if (!idbMedias) {
        idbMedias = []
      }
      ctx.clog('idb getMediasForChat idbMedias', idbMedias)
      setCardIndicatorParams(idbMedias, 'chat', 'pendingMediaSize')
      // romlebic gaagzavna magram uinternetobis gamo aq aris, pendingMessage-ebshia da aq ar gvinda
      const pendingMessageList = ctx.store.getters.pendingMessageLists[cardId]
      ctx.clog('idb getMediasForChat pendingMessageList', pendingMessageList)
      const pendingIdbIds = []
      if (pendingMessageList && pendingMessageList.length) {
        for (const pendingMessage of pendingMessageList) {
          ctx.clog('idb pendingMessage = ', pendingMessage)
          // aq sheizleba textMessage-ebic ikos, amitom
          if (pendingMessage.ts) {
            pendingIdbIds.push(pendingMessage.ts)
          }
        }
      }
      const idbMediaList = []
      for (const idbMedia of idbMedias) {
        if (!pendingIdbIds.includes(idbMedia.ts)) {
          idbMediaList.push(idbMedia)
        }
      }
      ctx.clog('idb getMediasForChat idbMediaList', idbMediaList)
      ctx.chatEBus.$emit(ctx.enums.events.idbMediasChat, idbMediaList)
    })
    .catch((err) => {
      ctx.clog('idb getMediasForChat err ', err)
    })
}
function getTypeFromFilename(filename) {
  if (filename) {
    if (ctx.isImage({ name: filename })) {
      return 'image'
    } else if (ctx.isVideo({ name: filename })) {
      return 'video'
    } else if (ctx.isAudio({ name: filename })) {
      return 'audio'
    } else return 'file'
  }
}
export function getMediasForQuestionnaire({ questionnaireIndex, downloaded, itemValue }) {
  if (!ctx.store.getters.card) {
    return
  }
  // if (downloaded) {
  // mashin orderer-ma chamotvirta mongodan da idbMedia-ebi unda shevqmnat
  const idbFiles = []
  const idbMedias = []
  ctx.clog('idb itemValue', itemValue)
  if (itemValue && itemValue.attachments) {
    const keys = ['medias', 'files']
    keys.forEach((key) => {
      // if (itemValue.attachments.medias) {
      if (itemValue.attachments[key]) {
        for (const relPath of itemValue.attachments[key]) {
          // relPath -> /questionnaires/1606943728590_379017.jpeg   deletePrefix = delete_
          if (relPath.includes(ctx.gdata.deletePrefix)) {
            continue
          }
          const filename = relPath.replace('/questionnaires/', '')
          const ext = ctx.getExtensionFromFilename(filename)
          const ts = ctx.getTsAndSizeFromFilename(filename)[0]
          const size = ctx.getTsAndSizeFromFilename(filename)[1]
          const type = getTypeFromFilename(filename)
          const media = {
            relPath,
            ext,
            type,
            size,
            tsList: { [ctx.enums.status.notSent]: ts }
          }
          if (key === 'medias') {
            idbMedias.push({ media })
          } else idbFiles.push({ media })
        }
      }
    })
  }
  const allIdbMedias = [...idbFiles, ...idbMedias]
  // console.log('idb allIdbMedias', allIdbMedias)
  if (!ctx.userIsTester()) {
    ctx.clog('idb getMediasForQuestionnaire userIsnotTester = ')
    // setCardIndicatorParams(allIdbMedias, 'questionnaire', 'pendingMediaSize')
    ctx.chatEBus.$emit(ctx.enums.events.idbMediasQuestionnaire + questionnaireIndex, { allIdbMedias, downloaded })
  }
  // tu testeria mashin idb-danac moaqvs
  else {
    const payload = { cardId: ctx.store.getters.card.id, questionnaireIndex }
    readMedias(payload)
      .then((idbMedias) => {
        ctx.clog('idb getMediasForQuestionnaire idbMedias = ', idbMedias)
        idbMedias && allIdbMedias.push(...idbMedias)
        // setCardIndicatorParams(allIdbMedias, 'questionnaire', 'pendingMediaSize')
        ctx.clog('idb card = ', ctx.store.getters.card)
        ctx.chatEBus.$emit(ctx.enums.events.idbMediasQuestionnaire + questionnaireIndex, { allIdbMedias })
      })
      .catch((err) => {
        ctx.clog('idb getMediasForQuestionnaire err ', err)
      })
  }
}
export function setCardIndicatorParams(idbMedias, target, prop) {
  const card = ctx.store.getters.card
  if (!card) {
    return
  }
  let size = 0
  if (!idbMedias) {
    idbMedias = []
  }
  for (const idbMedia of idbMedias) {
    if (idbMedia.media.size) {
      size += +idbMedia.media.size
    }
  }
  ctx.clog('idb setCardIndicatorParams size = ', size)
  ctx.store.commit('setCardIndicatorParams', {
    card,
    target,
    prop,
    size
  })
}
// gamotvlis am card-is questionnaire-shi sul ramdeni pendingMediaSize aris da daucers cards
export function setCardIndicatorQAllMediaSize(card = ctx.store.getters.card) {
  // const card = ctx.store.getters.card
  if (!card) {
    return
  }
  const cardId = card.id
  ctx.clog('idb setCardIndicatorQAllMediaSize card = ', card)
  const payload = { cardId, questionnaireIndex: 'allQMediaSize' }
  // ctx.clog('idb setCardIndicatorParams size = ', size)
  readMedias(payload)
    .then((allMediaSize) => {
      ctx.clog('idb getMediasForQuestionnaire allMediaSize = ', allMediaSize)
      ctx.store.commit('setCardIndicatorParams', {
        card,
        target: 'questionnaire',
        prop: 'pendingMediaSize',
        size: allMediaSize
      })
      const qValues = ctx.store.getters.questionnaireValues[cardId]
      if (qValues && qValues.attachSize) {
        ctx.store.commit('setCardIndicatorParams', {
          card,
          target: 'questionnaire',
          prop: 'alreadySentMediaSize',
          size: qValues.attachSize
        })
      }
    })
    .catch((err) => {
      ctx.clog('idb setCardIndicatorQAllMediaSize err ', err)
    })
}
export function readMedias(payload) {
  // payload = { cardId:'123', questionnaireIndex:'1.2' }
  ctx.clog('idb readMedias payload ', payload)
  const readAllQMediaSize = payload.questionnaireIndex === 'allQMediaSize'
  return new Promise((resolve, reject) => {
    const request = window.indexedDB.open(ctx.gdata.dbName, 1)
    request.onsuccess = (ev) => {
      const db = ev.target.result
      // ctx.clog('idb db ', db)
      let objectStoreName
      let indexName // cardId || cardIdAndQuestionnaireIndex
      let indexValue // 123_1.1 || 123
      if (payload.questionnaireIndex) {
        objectStoreName = 'questionnaireMedias'
        if (payload.questionnaireIndex === 'all' || readAllQMediaSize) {
          /* indexName = 'cardIdAndUploaded'
            // mchirdeba im mediebis casakitxad, romlebic jer ar atvirtula serverze
            indexValue = [payload.cardId, 0] */
          indexName = 'cardId'
          indexValue = payload.cardId
        } else {
          indexName = 'cardIdAndQuestionnaireIndex'
          indexValue = [payload.cardId, payload.questionnaireIndex]
        }
      } else {
        objectStoreName = 'chatMedias'
        indexName = 'cardId'
        indexValue = payload.cardId
      }
      // objectstore.index(indexName).openCursor(IDBKeyRange.only(payload.cardId))
      const transaction = db.transaction(objectStoreName, 'readonly')
      const store = transaction.objectStore(objectStoreName)
      const index = store.index(indexName)
      // addData = { cardId: data.cardId, file: data.file }
      const request = index.openCursor(IDBKeyRange.only(indexValue))
      request.onerror = (ev) => {
        console.error('idb Anfrage fehlgeschlagen!', ev.target.error.message)
        reject(ev.target.error.message)
      }
      let fetchFiles = []
      let allMediaSize = 0
      request.onsuccess = (ev) => {
        const cursor = ev.target.result
        ctx.clog('idb query.onsuccess cursor = ', cursor)
        if (cursor && cursor.value) {
          ctx.clog('idb cursor.value =', cursor.value)
          const idbMedia = cursor.value
          ctx.clog('idb readMedias idbMedia =', idbMedia)
          if (readAllQMediaSize) {
            allMediaSize += idbMedia.media.size
          } else fetchFiles.push(idbMedia)
          cursor.continue()
        } else {
          ctx.clog('idb Keine Einträge mehr vorhanden!')
          ctx.clog('idb fetchFiles.length = ', fetchFiles.length)
          if (readAllQMediaSize) {
            resolve(allMediaSize)
          } else {
            // sort array descending timestamp
            fetchFiles.sort(
              (a, b) => b.media.tsList[ctx.enums.status.notSent] - a.media.tsList[ctx.enums.status.notSent]
            )
            if (!fetchFiles.length) {
              fetchFiles = null
            }
            resolve(fetchFiles)
          }
        }
      }
    }
  })
}
export function readMediaById(payload) {
  // payload = { id:'12', questionnaireIndex:'1.2' }
  ctx.clog('idb readMediaById payload ', payload)
  return new Promise((resolve, reject) => {
    const request = window.indexedDB.open(ctx.gdata.dbName, 1)
    request.onsuccess = (ev) => {
      const db = ev.target.result
      let objectStoreName
      if (payload.questionnaireIndex) {
        objectStoreName = 'questionnaireMedias'
      } else {
        objectStoreName = 'chatMedias'
      }
      const transaction = db.transaction(objectStoreName, 'readonly')
      const store = transaction.objectStore(objectStoreName)
      const request = store.get(payload.id)
      request.onerror = (ev) => {
        console.error('idb Anfrage fehlgeschlagen!', ev.target.error.message)
        reject(ev.target.error.message)
      }
      request.onsuccess = (ev) => {
        // ctx.clog('ev ', ev)
        const idbMedia = ev.target.result
        ctx.clog('idb query.onsuccess idbMedia = ', idbMedia)
        resolve(idbMedia)
      }
    }
  })
}
export function removeMedia(idbMedia, questionnaireIndex, caller) {
  const payload = {
    cardId: ctx.store.getters.card.id,
    questionnaireIndex,
    media: idbMedia
  }
  ctx.clog('idb removeMedia payload = ', payload)
  _removeMedia(payload)
    .then((deleteId) => {
      ctx.clog('idb deleteId = ', deleteId)
      // cavshalot pendingMessageList-shic
      ctx.store.commit('removePendingMessageByIdbMediaFromList', idbMedia)
      ctx.clog('idb caller = ', caller)
      if (caller === ctx.enums.caller.chatFilemessage) {
        // onlyPendingMessages
        ctx.chatEBus.$emit(ctx.enums.events.loadReceiverAllMessages, true)
      } else if (caller === ctx.enums.caller.chatUit) {
        getMedias()
      } else {
        getMedias({ questionnaireIndex })
      }
    })
    .catch((err) => {
      ctx.clog('idb removeMedia err ', err)
    })
}
export function _removeMedia(payload) {
  // payload = { cardId:'123', questionnaireIndex: '1.2', media: { id: 2, file: File} }
  ctx.clog('idb _removeMedia payload ', payload)
  // ctx.clog('351 ctx ', ctx)
  const deleteId = payload.media.ts
  return new Promise((resolve, reject) => {
    const request = window.indexedDB.open(ctx.gdata.dbName, 1) // ctx.store.getters.indexedDBName
    request.onsuccess = (ev) => {
      const db = ev.target.result
      let objectStoreName
      // let isChat = true
      if (payload.questionnaireIndex) {
        // isChat = false
        objectStoreName = 'questionnaireMedias'
      } else {
        objectStoreName = 'chatMedias'
      }
      const transaction = db.transaction(objectStoreName, 'readwrite')
      const store = transaction.objectStore(objectStoreName)
      const request = store.delete(deleteId)
      request.onerror = (ev) => {
        console.error('Anfrage fehlgeschlagen!', ev.target.error.message)
        reject(ev.target.error.message)
      }
      // ctx.clog('i transaction.complete = ', ctx)
      transaction.oncomplete = (ev) => {
        ctx.clog('idb Daten wurden erfolgreich entfernt!')
        if (payload.questionnaireIndex) {
          // anketis shemtxvevashi vitvlit AllQMediaSize-s
          setCardIndicatorQAllMediaSize()
        }
        resolve(deleteId)
      }
    }
  })
}
export function deleteDB(createDB) {
  const req = indexedDB.deleteDatabase(ctx.gdata.dbName)
  req.onsuccess = () => {
    // ctx.clog('Deleted database successfully')
    ctx.showSnackbar(ctx.t('qchat.snackbar.idb_deleted'), 2000, 'error')
    // shevqmeniot xelaxla
    if (createDB) {
      createDB(ctx.gdata.dbName)
    }
  }
  req.onerror = () => {
    // ctx.clog("Couldn't delete database")
    ctx.showSnackbar(ctx.t('qchat.snackbar.idb_not_deleted'), 2000, 'error')
  }
  req.onblocked = () => {
    // ctx.clog("Couldn't delete database due to the operation being blocked")
    ctx.showSnackbar(ctx.t('qchat.snackbar.idb_delete_blocked'), 2000, 'error')
  }
}
export function createDB() {
  if (!('indexedDB' in window)) {
    ctx.clog('dieses browser doesnt support IndexedDB')
    ctx.showSnackbar(ctx.t('qchat.snackbar.not_support_idb'), 2000, 'error')
    return
  }
  if (!ctx.gdata.dbName) {
    ctx.clog('idb ctx.gdata.dbName = ', ctx.gdata.dbName)
    return
  }
  ctx.clog('idb createDB ctx.dbName= ', ctx.gdata.dbName)
  const request = indexedDB.open(ctx.gdata.dbName, 1)
  request.onupgradeneeded = (event) => {
    const db = event.target.result
    let n = 0
    // Object stores should contain objects of the same "type" (not JavaScript data type)
    // { cardId: '123', files: [], images: []}
    // { cardIdAndQuestionnaireIndex: '123_1.2', files: [], images: []}
    if (!db.objectStoreNames.contains('chatMedias')) {
      const objectStore = db.createObjectStore('chatMedias', {
        keyPath: 'ts'
      })
      // objectStore.createIndex('indexName', 'property', options)
      objectStore.createIndex('cardId', 'cardId', { unique: false })
      n++
    }
    if (!db.objectStoreNames.contains('questionnaireMedias')) {
      const objectStore = db.createObjectStore('questionnaireMedias', {
        keyPath: 'ts' /* ,
        autoIncrement: true */
      })
      objectStore.createIndex('cardId', 'cardId', { unique: false })
      objectStore.createIndex('cardIdAndQuestionnaireIndex', ['cardId', 'questionnaireIndex'], { unique: false })
      // mchirdeba im mediebis casakitxad, romlebic jer ar atvirtula serverze
      // mobjectStore.createIndex('cardIdAndUploaded', ['cardId', 'uploaded'], { unique: false })
      n++
    }
    if (n === 0) {
      ctx.clog('IDB exists!')
    } else {
      ctx.clog(`IDB was created with ${n} objectStores!`)
    }
  }
  request.onsuccess = (ev) => {
    ctx.clog('DB exists8877!!', ev)
  }
}
// jer ar vxmarobt, radgan uploaded aghar gvchirdeba, magram sxva rameshi idb-update-shi sheizleba gamogvadges
export function putMedia(payload) {
  // mashin questionnaire-ia
  return new Promise((resolve, reject) => {
    ctx.clog('idb putMedia payload = ', payload)
    const request = indexedDB.open(ctx.gdata.dbName, 1)
    request.onsuccess = (ev) => {
      const db = ev.target.result
      const transaction = db.transaction('questionnaireMedias', 'readwrite')
      const store = transaction.objectStore('questionnaireMedias')

      const id = payload.idbId
      const uploadedRequest = store.get(id)
      uploadedRequest.onsuccess = () => {
        const data = uploadedRequest.result
        data.uploaded = payload.uploaded
        const updatedRequest = store.put(data)
        updatedRequest.onsuccess = () => {
          ctx.clog('idb Data was succesfully updated!')
          resolve()
        }
      }
      transaction.onerror = (ev) => {
        console.error('idb beim update ist ein Fehler aufgetreten!', ev.target.error.message)
        reject(ev.target.error.message)
      }
      /* transaction.oncomplete = ev => {
          ctx.clog('idb oncomplete Data was succesfully updated!')          
          resolve()
        } */
    }
  })
}
