import Vue from 'vue'
import VueI18n from 'vue-i18n'
// import Vuex from 'vuex'
import { Store } from 'vuex'

import TestData from './testData'
import chats from './store/modules/chats'
import chatsPersistent from './store/modules/chatsPersistent'
import commonPersistent from './store/modules/commonPersistent'
import questionnaires from './store/modules/questionnaires'
import questionnairesPersistent from './store/modules/questionnairesPersistent'
import * as enums from './helpers/enums'

import Imprint from './staticPages/Imprint.vue'
import Privacy from './staticPages/Privacy.vue'
// import Contact from './staticPages/Contact.vue'
import Orderers from './staticPages/Orderers.vue'
import Testers from './staticPages/Testers.vue'
import TestWebsite from './staticPages/tutorials/TestWebsite.vue'
import Product from './staticPages/products/Product.vue'
import Orders from './staticPages/products/Orders.vue'
import Pledge from './staticPages/products/Pledge.vue'
import Binding from './staticPages/products/Binding.vue'
// revocation-> widerruf ar gvaqvs
// import revocation from './staticPages/Revocation.vue'
import TutorialRegistration from './staticPages/tutorials/Registration.vue'
import TutorialQuestionnaire from './staticPages/tutorials/Questionnaire.vue'
import TutorialRoom from './staticPages/tutorials/Room.vue'
import TutorialChat from './staticPages/tutorials/Chat.vue'
import TutorialOrderer from './staticPages/tutorials/Orderer.vue'
import TutorialTester from './staticPages/tutorials/Tester.vue'
import TutorialInvoice from './staticPages/tutorials/Invoice.vue'
import OnYoutube from './staticPages/tutorials/OnYoutube.vue'
import Gtc from './staticPages/Gtc.vue'
import Principles from './staticPages/Principles.vue'

import Indicator from './indicator/Indicator.vue'
import IndicatorQuestionnaire from './indicator/IndicatorQuestionnaire.vue'
import Chat from './chats/ChatWindow'
import QuestionnaireUploader from './questionnaire/QuestionnaireUploader'
import QuestionnaireExampleBasic from './questionnaire/QuestionnaireExampleBasic'
import QuestionnaireExampleAdvanced from './questionnaire/QuestionnaireExampleAdvanced'
import QuestionnaireExamplePhone from './questionnaire/QuestionnaireExamplePhone'
import Invoice from './invoice/Invoice'
import { CreateInvoice } from './invoice/createInvoice'
import Contact from './contact'
// import TooltipMe from './DesTooltip'
// import InvoiceUploader from './invoice/InvoiceUploader'
// import InvoiceDownloader from './invoice/InvoiceDownloader'
import Questionnaire from './questionnaire/Questionnaire'
import Consolex from './Consolex'

import * as idbs from './idbMedia/'
import * as ims from './invoice/'
import * as cims from './invoice/createInvoice'
import * as qms from './questionnaire/'
import * as ads from './adjust'
import * as adms from './adminMe'
import * as axms from './helpers/axiosMethods'
import * as mms from './helpers/methods'
import * as mmcts from './helpers/methodsCtx'
import * as pnts from './push/push-notifications'
// kvela methodebi, romlebic outsourcedMethodsArr-is js failebshia aqac micvdomadi xdeba
// mag. cloneObj() method from methods.js -> this.$g.cloneObj()
const initContextArr = [idbs, ims, cims, qms, ads, adms, mms, axms, mmcts, pnts]
// const initContextArr = [...outsourcedMethodsArr, axms]

// amati export xdeba
const components = {
  Chat,
  Questionnaire,
  Indicator,
  IndicatorQuestionnaire,
  QuestionnaireUploader,
  Invoice,
  // TooltipMe,
  TutorialRegistration,
  TutorialQuestionnaire,
  TutorialRoom,
  TutorialChat,
  TutorialOrderer,
  TutorialTester,
  TutorialInvoice,
  OnYoutube,
  Imprint,
  Privacy,
  Gtc,
  Principles,
  Contact,
  TestWebsite,
  Orderers,
  Testers,
  Product,
  Orders,
  Pledge,
  Binding,
  QuestionnaireExampleBasic,
  QuestionnaireExampleAdvanced,
  QuestionnaireExamplePhone,
  Consolex
}

// amas mxolod vikenebt, roca QuestionnaireUploadBasic an QuestionnaireUploadAdvanced gvinda shevavsot
// const isQuestionnaireExample = process.env.G_QUESTIONNAIRE_EXAMPLE && JSON.parse(process.env.G_QUESTIONNAIRE_EXAMPLE)

const MEDIAS_BASE_URL = process.env.MEDIAS_BASE_URL // 'https://medias.deservice.net' // chat-ის და questionnaire-ის მედიას სადაც ვინახავთ
const BASE_URL_AXIOS = '/qchat' // qchat mxolod chemia, baseIp nuxt.config.js-is proxy-shi sheicvleba https://(test)qchat.deservice.net -it
const GQCHAT_BASE_URL = process.env.GQCHAT_BASE_URL // 'https://qchat.deservice.net' // 'http://136.243.102.152:4129' //
let BASE_IP_LOCALHOST = process.env.BASE_IP_LOCALHOST // 'http://192.168.178.20'
// const BASE_IP_LOCALHOST = 'localhost'
if (process.env.HOST === 'localhost') {
  BASE_IP_LOCALHOST = 'http://localhost'
}
const SERVER_PORT = process.env.SERVER_PORT // '4129'
// const CHAT_NAMESPACE = 'chat'
const qchat = 'qchat'
const versionsFilename = 'main_versions.json'
// es aris chemi samushao konstantebi
const gdata = {
  adminNicknames: ['check763', 'gcarcheck763', 'gOrderer62'], // imati nickname-ebi, visac ufleba aqvs sheqmnas questionnaireExample-ebi
  pendingMessagesInterval: 2000, //  process.env.PENDING_MESSAGES_INTERVAL
  isLocalhost: process.env.HOST === 'localhost' || process.env.HOST === '0.0.0.0',
  // process.env.QUESTIONNAIRE_VERSION murmanis mxares iqneba. tu ar dacera me mainc davcer
  // eseni chaiwereba jenkins-shi-> 136.243.102.152:8000-> dtm:.. ->deServiceFront->Konfigurieren->Buildverfahren-shi env-ebad
  /* packageVersion: '1.1.0',  qchatVersionData
  questionnaireDeepVersion: '1.0.0', // xelit kovel jerze me unda chavcero es version aq
  questionnairePhoneVersion: '1.0.0', // xelit
  */
  // xelit kovel cvlilebaze me unda chavcero es versions glimerHetzner-ze -> /var/lib/docker/volumes/desmedias/_data/gversions/main_versions.json
  versions: {
    // tu package-shi meore cifri sheicvala, mashin localStorage-shi caishleba qchat-is key anu desqchat-is nacili caishleba
    package: '1.0.0',
    gtc: '1.0.0',
    privacy: '1.0.0',
    questionnaire: {
      deep: '1.0.0',
      phone: '1.0.0'
    }
  },
  adminData: {
    email: 'info@deservice.net',
    nickName: 'adminGlimer',
    recId: 0,
    // serviceNames -> G1, G2...
    prices: {
      G1: 20, // ramden %-s ვიღებთ მომსახურებისთვის G1-carcheck
      G2: 15,
      adminResell: 12, // 12 Euro vor resell
      testerResell: 5 // 5 Euro für resell für checker
    },
    privateData: {
      isJur: true,
      descrip: 'Glimer Consulting GmbH',
      countryAlpha2: 'de'
    },
    invoiceData: {
      zip: '29223',
      location: 'Celle',
      address: 'Kampstr. 18',
      countryAlpha2: 'de',
      vatNumber: 'DE13265465',
      vatRate: '19'
    }
  },
  deletePrefix: 'delete_',
  defaultLanguage: process.env.G_DEFAULT_LANG || 'de',
  filenameMaxlength: process.env.G_FILENAME_MAX_LENGTH || 10, // 10 b
  commentMaxSize: process.env.G_QUESTIONNAIRE_COMMENT_MAX_SIZE || 500, // 500 b
  chatTextMaxSize: process.env.G_CHATTEXT_MAX_SIZE || 500, // 500 b
  mediaMaxSize: process.env.G_MEDIA_MAX_SIZE || 200, // 200 mb
  videoMaxSize: process.env.G_VIDEO_MAX_SIZE || 50, // 50 mb
  audioMaxSize: process.env.G_AUDIO_MAX_SIZE || 20, // 20 mb
  imageMaxSize: process.env.G_IMAGE_MAX_SIZE || 10, // 10 mb
  fileMaxSize: process.env.G_FILE_MAX_SIZE || 10, // 10 mb
  chatMediaSendMode: process.env.G_CHAT_MEDIA_SEND_MODE || 'axios', // axios, socketStream, siofu      siofu sr gvinda
  videoRecordingMaxTimeMS: process.env.G_VIDEO_RECORDING_MAX_TIME_MS || 30000, // in millisec mobile-ში არ მოქმედებს
  mediaUploadingErrorTimeoutMS: process.env.G_MEDIA_UPLOADING_ERROR_TIMEOUT__MS || 60000, // serverze mediis atvirtvisas tu chamoekida 60 camshi mainc gaxsnis
  messageTsFormat: process.env.G_MESSAGE_TS_FORMAT || 'HH:mm', // 'DD.MM.YY HH:mm:ss'
  dbName: process.env.DESERVICE_IDB_NAME
}
// es sia shesavsebia kvela linkit
const idToAttributs = {
  imprintCeoPerson: { attr: 'innerHTML', value: 'Gia Lomidze' },
  imprintResponsiblePerson: { attr: 'innerHTML', value: 'Gia Lomidze' },
  imprintVat: { attr: 'innerHTML', value: 'DE813674850' },
  imprintTax: { attr: 'innerHTML', value: '17/200/50209' },
  contactWhatsappNr: { attr: 'innerHTML', value: '+49 157-39260200' },
  imprintTel: { attr: 'innerHTML', value: '+49 157-39260200' },
  contactEmail: { attr: 'innerHTML', value: 'info@deservice.net' },
  skypeName: { attr: 'innerHTML', value: 'deservice net' },
  imprintEmail: { attr: 'innerHTML', value: 'admin@deservice.net' },
  privacyEmail: { attr: 'innerHTML', value: 'admin@deservice.net' },
  principlesEmail: { attr: 'innerHTML', value: 'admin@deservice.net' },
  privacyStandDate: { attr: 'innerHTML', value: '01.01.2022' },
  gtcStandDate: { attr: 'innerHTML', value: '01.01.2022' },
  testDeservice: { attr: 'href', value: 'https://www.test.deservice.net/' },
  imprintEuDisputs: { attr: 'href', value: 'http://ec.europa.eu/consumers/odr/' },
  privacyGooglePlugin: { attr: 'href', value: 'http://tools.google.com/dlpage/gaoptout?hl=de' },
  safetyCarPurchase: { attr: 'href', value: 'https://sicherer-autokauf.de/' }
}

// marto testistvis vxmarobt
// const reset = false
const chatWindowHeight = '700px' //  process.env.CHAT_WINDOW_HEIGHT
const fullScreenHeight = '100vh' //  process.env.FULLSCREEN_HEIGHT
// 1 context = { app, store, route, params, query, env, isDev, isHMR, redirect, error }
// export default class QChat {
class QChat {
  // constructor(app, store)
  constructor() {
    if (process.env.G_LOCALHOST && JSON.parse(process.env.G_LOCALHOST) && process.env.HOST === '0.0.0.0') {
      this.getLocalIp()
    }
    // this rom sxva context-shi ar moxvdes
    const autoBind = require('auto-bind')
    autoBind(this)
    this.consoleText = ''
    this.qIndicatorDefaultData = null
    this.chatEBus = new Vue()
    this.enums = enums
    // Vue.use(Vuex)
    // this.store = new Vuex.Store({})
    this.store = new Store({})
    // this.app = new Vue({ store: this.store })
    Vue.use(VueI18n)
    this.initOutsourcedMethods()
    this.initGData()
    this.initStore()
    this.initI18n()
    /* .then(() => {
        this.initStore()
        this.initI18n()
      })
      .catch((e) => {
        this.clog('i initGData error = ', e)
      }) */
  }

  initStore() {
    this.persistedModules = { commonPersistent, chatsPersistent, questionnairesPersistent }
    this.modules = { chats, questionnaires }
    if (process.env.G_RESET && JSON.parse(process.env.G_RESET)) {
      this.clearLocalStorage()
      this.deleteDB(true)
    }
    // this.createDB() sxvagan idbMedia/index.js-shi aris, magram aq micvdomadia radgan this.initOutsourcedMethods() ukve moxda
    this.createDB()
    // tu qchat-is packageVersion-is meore ricxvi sheicvala, mashin qchat-is localStorage clear
    /* if (this.checkIfQchatPackageVersionSecondNumberHasChanged()) {
      this.clog('i checkIfQchatPackageVersionSecondNumberHasChanged')
      this.clearLocalStorage()
    } */
    this.restoreData() // chavceret commonPersistent, chatsPersistent, questionnairesPersistent -ebshi shenaxuli mnishvnelobebi
    // this.clog('i chatsPersistent ', chatsPersistent)
    Object.keys(this.persistedModules).forEach((key) => {
      this.store.registerModule(key, this.persistedModules[key])
    })
    Object.keys(this.modules).forEach((key) => {
      this.store.registerModule(key, this.modules[key])
    })

    const testData = new TestData(this.store, this.gdata)
    testData.init()
    // this.clog('i 2 store = ', this.store)
    // this.clog('i process.env.G_LOCALHOST = ', process.env.G_LOCALHOST)
    this.subscribe()
    this.store.commit('setQchatVersions', this.gdata.versions)
    this.clog('i 2 this.store.getters.fontWasWarned = ', this.store.getters.fontWasWarned)
    // this.store.commit('setFontWasWarned', this.store.getters.fontWasWarned)
    // this.store.commit('setOpenArr', this.store.getters.openArr)
    this.setInternetListener()
  }

  checkIfQchatPackageVersionSecondNumberHasChanged() {
    if (localStorage.getItem(qchat)) {
      const qchatValues = Object.assign({}, JSON.parse(localStorage.getItem(qchat)))
      // this.clog('i qchatValues2 = ', qchatValues)
      if (!qchatValues.commonPersistent) {
        return true
      }
      const qvd = qchatValues.commonPersistent.qchatVersions
      // this.clog('i qvd = ', qvd)
      if (!qvd) {
        return true
      }
      // const qvd = this.store.getters.qchatVersionData
      const secondNumberSaved = qvd.package.split('.')[1]
      const secondNumberNew = this.gdata.versions.package.split('.')[1]
      return secondNumberSaved !== secondNumberNew
    }
    return false
  }

  // ამ მეთოდის გამოყენებას სჭირდება chrome-ში ჯერ დაყენება: chrome://flags -> DISABLE the flag -> Anonymize local IPs exposed by WebRTC
  getLocalIp() {
    // console.log('i getLocalIp = ')
    window.RTCPeerConnection =
      window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection || false
    const ips = []
    if (window.RTCPeerConnection) {
      const pc = new RTCPeerConnection({ iceServers: [] })
      const noop = function () {}
      pc.createDataChannel('')
      pc.createOffer(pc.setLocalDescription.bind(pc), noop)
      pc.onicecandidate = (event) => {
        if (event && event.candidate && event.candidate.candidate) {
          const s = event.candidate.candidate.split('\n')
          const ip = s[0].split(' ')[4]
          if (ip.includes('192')) {
            BASE_IP_LOCALHOST = 'http://' + ip
            // console.log('i BASE_IP_LOCALHOST = ', BASE_IP_LOCALHOST)
            this.gdata.baseUrlAxios = BASE_IP_LOCALHOST + ':' + SERVER_PORT
            this.gdata.baseUrl = BASE_IP_LOCALHOST + ':' + SERVER_PORT
            return
          }
          ips.push(ip)
        }
      }
    }
    return null
  }

  setInternetListener() {
    window.addEventListener('online', () => {
      this.clog('i online')
      this.showSnackbar(this.t('qchat.snackbar.your_are_online'), 2000, 'green', enums.caller.chat)
      this.showSnackbar(this.t('qchat.snackbar.your_are_online'), 2000, 'green', enums.caller.questionnaire)
    })
    window.addEventListener('offline', () => {
      this.clog('i offline')
      this.showSnackbar(this.t('qchat.snackbar.your_are_offline'), 2000, 'error', enums.caller.chat)
      this.showSnackbar(this.t('qchat.snackbar.your_are_offline'), 2000, 'error', enums.caller.questionnaire)
    })
  }

  clearLocalStorage() {
    this.clog('i clearLocalStorage ')
    localStorage.removeItem(qchat)
    // localStorage.clear()
  }

  // kvela methodebi, romlebic outsourcedMethodsArr-is js failebshia aqac micvdomadi xdeba
  // mag. cloneObj() method from methods.js -> this.$g.cloneObj()
  initOutsourcedMethods() {
    idbs.initContext(this)
    initContextArr.forEach((el) => {
      Object.entries(el).forEach(([name, func]) => {
        this[name] = func
      })
    })
  }

  initI18n() {
    const messages = {}
    for (const el of this.store.getters.languages) {
      // messages[el] = require('./locales/' + el + '/' + el + '.json')
      messages[el] = require('./locales/' + el + '.json')
    }
    const i18n = new VueI18n({
      locale: this.store.getters.locale,
      fallbackLocale: this.store.getters.languages[0],
      messages
    })
    // Vue.prototype.$i18n = this.app.i18n
    this.app = new Vue({ store: this.store, i18n })
    this.t = this.app.$t
    this.$i18n = this.app.$i18n
    // this.clog('this.app.$t = ', this.app.$t)
  }

  updateReaderLocale(locale) {
    if (locale && this.$i18n && this.clog) {
      this.$i18n.locale = locale
      this.clog('i this.$i18n.locale = ', locale)
    }
  }

  updateLocale(locale) {
    if (locale) {
      this.store.commit('setLocale', locale)
    }
    this.clog('i this.store.getters.locale = ', this.store.getters.locale)
  }

  initGData() {
    let baseUrlAxios = BASE_URL_AXIOS
    let baseUrl = GQCHAT_BASE_URL
    if (process.env.G_LOCALHOST && JSON.parse(process.env.G_LOCALHOST)) {
      // dev-is dros orive ertnairia
      baseUrlAxios = BASE_IP_LOCALHOST + ':' + SERVER_PORT
      baseUrl = BASE_IP_LOCALHOST + ':' + SERVER_PORT
    }
    // this.clog('i baseUrl = ', baseUrl)
    this.gdata = {
      ...gdata,
      isMobile: mmcts.isMobileBrowser(),
      baseUrlMedias: MEDIAS_BASE_URL,
      baseUrlAxios,
      baseUrl, // MediaItem fileUrl xmarobs suratebis gamosachenad
      chatUrl: baseUrl // + '/' + CHAT_NAMESPACE // chat xmarobs socket-istvis
    }

    // this.pendingIntervalTimer = null
    this.socket = null // localStorage.getItem('gsocket')
    this.sockets = []
    // marto testistvis sheizleba vixmarot
    this.gTestData = {
      chatWindowHeight,
      fullScreenHeight
    }
    this.gdata.versions = this.getGVersions()
    /* return new Promise((resolve, reject) => {
      this.getGVersions()
        .then((versions) => {
          this.gdata.versions = versions
          if (this.checkIfQchatPackageVersionSecondNumberHasChanged()) {
            this.clog('i QchatPackageVersionSecondNumberHasChanged')
            this.clearLocalStorage()
          }
          // this.clog('i this.gdata.versions = ', this.gdata.versions)
          resolve()
        })
        .catch((e) => {
          this.clog('i getGVersions error = ', e)
          reject(e)
        })
    }) */
  }

  subscribe() {
    // The handler is called after every mutation  mutation.type = mag. 'setFontWasWarned'
    this.store.subscribe((mutation, state) => {
      // const localStorageItem = {}
      Object.keys(this.persistedModules).forEach((key) => {
        const persistedModule = this.persistedModules[key]
        // tu es persistedModule-is mutation aris
        if (persistedModule.mutations && persistedModule.mutations[mutation.type]) {
          /* console.log('i persistedModule = ', persistedModule)
          console.log('i mutation = ', mutation)
          console.log('i key = ', key)
          console.log('i state = ', state)
          console.log('i persistedModule.mutations[mutation.type] = ', persistedModule.mutations[mutation.type]) */
          if (state[key]) {
            let localStorageItem = JSON.parse(localStorage.getItem(qchat))
            if (localStorageItem) {
              localStorageItem[key] = state[key]
            } else {
              localStorageItem = { [key]: state[key] }
            }
            // localStorage.setItem(key, JSON.stringify(state[key]))
            localStorage.setItem(qchat, JSON.stringify(localStorageItem))
          }
        }
      })
    })
  }

  restoreData() {
    // this.clog('i localStorage.getItem(qchat) = ', localStorage.getItem(qchat))
    if (localStorage.getItem(qchat)) {
      const qchatValues = Object.assign({}, JSON.parse(localStorage.getItem(qchat)))
      // this.clog('i qchatValues = ', qchatValues)
      Object.keys(this.persistedModules).forEach((key) => {
        // this.clog('i module key = ', key)
        if (qchatValues[key]) {
          const state = qchatValues[key]
          // this.clog('i state = ', state)
          this.persistedModules[key].state = () => state
          // Replace the state object with the stored item
          // this.store.replaceState(Object.assign(state, JSON.parse(localStorage.getItem(modulename))))
        }
      })
    }
    // this.clog('i this.modules.questionnaires.state().questionnaireValues = ', this.modules.questionnaires.state().questionnaireValues)
  }

  goToHyperlink(ev) {
    this.clog('i ev', ev)
    // const from = ev.target.id
    if (ev.target.dataset && ev.target.dataset.to) {
      const to = ev.target.dataset.to
      const split = to.split('-')
      // const t =
      this.hyperlink({
        group: split[0],
        goal: split[1],
        id: split[2]
      })
      // console.log('387 t', t)
    }
  }

  // tu rame href link-ebia staticTexts html failebshi, chavcerot href linkebi
  /* setAttrsToHtmlTextFiles1() {
    const sometext = document.createTextNode('Hallöchen')
    const whatsapp = document.createTextNode('0123456')
    document.getElementById('contactEmail').appendChild(sometext)
    document.getElementById('contactWhatsappNr').appendChild(whatsapp)
  } */

  setAttrsToHtmlTextFiles() {
    // hyperlink elements style and onclick
    const main = document.getElementById('main')
    const hyperlinkStyle = document.createElement('style')
    hyperlinkStyle.innerHTML = '.hyperlink { cursor:pointer; color:DodgerBlue; text-decoration: underline; }'
    //   '.hyperlink { cursor:pointer; color:blue; } .hyperlink:hover { text-decoration: underline; }'
    // const fontColor = this.$vuetify.theme.dark ? '#F5F8FE' : '#F5F8FE'
    if (main) {
      // main.style = 'color:' + fontColor
      main.appendChild(hyperlinkStyle)
    }
    document.getElementsByClassName('hyperlink').forEach((elem) => {
      // console.log('elem = ', elem)
      if (elem) {
        elem.onclick = this.goToHyperlink
        // elem.style = 'cursor:pointer;color:steelblue;text-decoration:underline;'
      }
    })
    Object.keys(idToAttributs).forEach((id) => {
      const obj = idToAttributs[id]
      const elem = document.getElementById(id)
      if (elem) {
        elem[obj.attr] = obj.value
      }
    })
  }

  setObjectProperty(obj, key, value) {
    this.store.commit('setObjectProperty', { obj, key, value })
  }

  pushToArray(arr, value) {
    this.store.commit('pushToArray', { arr, value })
  }

  // აღარ ვხმარობთ, რადგანს სერვერი აღარ არკვევს lang-ს
  adjustReceiverLanguage(typingMessage) {
    const senderLang = typingMessage.meta.sender.lang
    const receiver = this.getReceiver()
    this.clog('i senderLang = ', senderLang)
    this.clog('i receiver.lang = ', receiver.lang)
    if (receiver.lang !== senderLang) {
      // receiver.lang = senderLang
      this.setObjectProperty(receiver, 'lang', senderLang)
    }
  }

  setSocket(socket) {
    this.clog('i socket = ', socket)
    this.removeSocket()
    this.socket = socket
    this.sockets.push(socket)
  }

  removeSocket() {
    this.clog('i removeSocket this.socket = ', this.socket)
    for (let sock of this.sockets) {
      sock.disconnect()
      sock = null
    }
    if (this.socket) {
      this.socket.disconnect()
      this.socket = null
    }
  }

  // caller = chat||questionnaire
  showSnackbar(text, timeout, color, caller) {
    if (!caller) {
      caller = enums.caller.chat
    }
    const payload = { text, timeout, color, caller }
    this.store.commit('showSnackbar', payload)
  }

  isMy(message) {
    // console.log('i message ', message)
    const user = this.store.getters.user
    // console.log('i user ', user)
    return user && message && message.senderId === user.id
  }

  userIsTester(card = this.store.getters.card, user = this.store.getters.user) {
    let isTester = false
    if (card && card.tester && user) {
      isTester = card.tester.id === user.id
    }
    if (this.isExampleQuestionnaireCreating()) {
      isTester = true
    }
    return isTester
  }

  userIsOrderer(card = this.store.getters.card, user = this.store.getters.user) {
    // const user = this.store.getters.user
    // const card = _card || this.store.getters.card
    let isOrderer = false
    if (card && card.orderer && user) {
      isOrderer = card.orderer.id === user.id
    }
    if (this.isExampleQuestionnaireCreating()) {
      isOrderer = false
    }
    return isOrderer
  }

  userIsBuyer(card = this.store.getters.card, user = this.store.getters.user) {
    let isBuyer = false
    if (card && card.buyer && user) {
      isBuyer = card.buyer.id === user.id
    }
    if (this.isExampleQuestionnaireCreating()) {
      isBuyer = false
    }
    return isBuyer
  }

  onTextInput(v, size) {
    if (!v) {
      return ''
    }
    if ((v || '').length <= size - 1) {
      return true
    }
    v = v.substring(0, size - 2) + '..'
    return `Max ${size} characters`
  }

  getRole(user, card) {
    if (!user || !card) {
      return null
    }
    const tester = card.tester
    const orderer = card.orderer
    let role = null
    if (user.id === tester.id) {
      role = tester.role
    } else if (user.id === orderer.id) {
      role = orderer.role
    }
    return role
  }

  getReceiver() {
    const card = this.store.getters.card
    if (!card) {
      return null
    }
    this.clog('i getReceiver card = ', JSON.stringify(card))
    const tester = card.tester
    const orderer = card.orderer
    const user = this.store.getters.user
    if (!user || !tester || !orderer) {
      return null
    }
    const receiver = user.id === tester.id ? orderer : tester
    this.clog('i receiver = ', receiver)
    return receiver
  }

  getSender() {
    const card = this.store.getters.card
    if (!card) {
      return null
    }
    this.clog('i getReceiver card = ', JSON.stringify(card))
    const tester = card.tester
    const orderer = card.orderer
    const user = this.store.getters.user
    if (!user || !tester || !orderer) {
      return null
    }
    const sender = user.id === tester.id ? tester : orderer
    this.clog('i sender = ', sender)
    return sender
  }

  getSender2() {
    const user = this.store.getters.user
    const card = this.store.getters.card
    if (!user || !card) {
      return null
    }
    return {
      id: user.id,
      lang: this.store.getters.locale,
      nickname: user.nickname,
      role: this.getRole(user, this.store.getters.card)
    }
  }

  downloadFileFromServer(relPath) {
    // const relPath = idbMedia.media.relPath
    const filename = mms.getFilenamefromPath(relPath)
    // relPath = /chats/1599165318843_49098.jpeg
    const _caller = relPath && relPath.includes('/chats/') ? enums.caller.chat : enums.caller.questionnaire
    this.clog('i downloadFileFromServer relPath = ', relPath)
    axms
      .downloadFile({ relPath })
      .then((resp) => {
        const downloadUrl = window.URL.createObjectURL(new Blob([resp.data]))
        const link = document.createElement('a')
        link.href = downloadUrl
        link.setAttribute('download', filename) // any other extension
        document.body.appendChild(link)
        link.click()
        link.remove()
        this.showSnackbar(this.t('qchat.media.succesfully_downloaded'), 2000, 'green', _caller)
      })
      .catch((err) => {
        this.showSnackbar(this.t('qchat.media.download_error') + err, 4000, 'red', enums.caller.questionnaire)
      })
  }

  clog(p1, p2) {
    if ((this.store && this.store.getters.debug) || (process.env.G_LOGS && JSON.parse(process.env.G_LOGS))) {
      if (p1 && typeof p2 !== 'undefined') {
        console.log(p1, p2)
      } else if (p1) {
        console.log(p1)
      }
    }
    // console.log('i this.store.getters.consolexDialog:', this.store.getters.consolexDialog)
    /* if (this.store.getters.consolexDialog) {
      if (p1 && typeof p2 !== 'undefined') {
        // this.consoleText = this.consoleText + '\r\n' + p1 + p2
        this.store.commit('setConsoleText', this.store.getters.consoleText + '<br/>' + p1 + p2)
      } else if (p1) {
        // this.consoleText = this.consoleText + '\r\n' + p1
        this.store.commit('setConsoleText', this.store.getters.consoleText + '<br/>' + p1)
      }
    } else if (process.env.G_LOGS && JSON.parse(process.env.G_LOGS)) {
      if (p1 && typeof p2 !== 'undefined') {
        console.log(p1, p2)
      } else if (p1) {
        console.log(p1)
      }
    } */
  }

  // ramdenia mustFilledFields, allFields orive level-shi
  getQIndicatorDefaultData() {
    // const levels = ['basic', 'advanced', 'phone']
    const levels = Object.keys(enums.levels)
    if (this.qIndicatorDefaultData) {
      let inited = true
      for (const level of levels) {
        if (this.qIndicatorDefaultData[level].allFields === 0) {
          inited = false
          break
        }
      }
      if (inited) {
        return this.qIndicatorDefaultData
      }
    }
    const data = {}
    for (const level of levels) {
      const questionnaireLevel = this.store.getters.questionnaire({ locale: 'de', card: { level } })
      data[level] = {}
      if (!questionnaireLevel || !questionnaireLevel.items) {
        data[level].mustFilledFields = 0
        data[level].allFields = 0
      } else {
        let mustFilledFields = 0
        let allFields = 0
        const items = questionnaireLevel.items
        for (const item of items) {
          for (const it of item.children) {
            allFields++
            if (it.mandatory) {
              mustFilledFields++
            }
          }
        }
        data[level].mustFilledFields = mustFilledFields
        data[level].allFields = allFields
      }
    }
    this.qIndicatorDefaultData = data
    // console.log('i data = ', data)
    return data
  }

  getGVersions() {
    return mms.readGVersionsJsonfile()
  }

  getGVersions2() {
    return new Promise((resolve, reject) => {
      axms
        .readActualVersionsRemoteFile(versionsFilename, true)
        .then((resData) => {
          resolve(resData)
        })
        .catch((e) => {
          reject(e)
        })
    })
  }

  getCarouselTexts(locale) {
    return this.getOtherTextsFromArr('carousel', locale)
  }

  getFaqTexts(locale) {
    // return this.getOtherTextsFromArr('faqs', locale)
    // murmani rom sheascorebs mere es unda ikos
    return this.getFaqTextsFromFile('faqs', locale)
  }

  getFeedbackTexts(locale) {
    return this.getOtherTextsFromArr('feedbacks', locale)
  }

  getProductTexts(locale) {
    return this.getOtherTextsFromObj('products', locale)
  }

  getOtherTextsFromObj(filename, locale) {
    const json = mms.readOtherTextsJSonfile(filename)
    const textObj = {}
    // es itvaliscinebs, rom romelighac nacili ar gamochndes
    Object.keys(json.texts).forEach((key1) => {
      const value1 = json.texts[key1]
      if (value1.show) {
        const obj = {}
        Object.keys(value1).forEach((key2) => {
          const value2 = value1[key2]
          const val = value2[locale] ? value2[locale] : value2
          // cavshalot show property
          delete obj.show
          obj[key2] = val
        })
        textObj[key1] = obj
      }
    })
    return textObj
  }

  getOtherTextsFromArr(filename, locale) {
    const json = mms.readOtherTextsJSonfile(filename)
    const textArr = []
    for (const item of json.texts) {
      const obj = {}
      Object.keys(item).forEach((key) => {
        const value = item[key]
        let val = value[locale] ? value[locale] : value
        // ეს კერძო შემთხვევაა, როცა carousel-ში მობილურის დროს პატარა სურათები გვინდა
        if (key === 'image' && filename === 'carousel' && this.gdata.isMobile) {
          // image -> mobimg0.webp
          val = 'mob' + val
        }
        obj[key] = val
      })
      textArr.push(obj)
    }
    // console.log('i textArr = ', textArr)
    return textArr
  }

  getFaqTextsFromFile(filename, locale) {
    const json = mms.readOtherTextsJSonfile(filename)
    const textObj = {}
    Object.keys(json.texts).forEach((key) => {
      const value = json.texts[key]
      const textArr = []
      for (const item of value) {
        const obj = {}
        Object.keys(item).forEach((key2) => {
          obj[key2] = item[key2][locale]
        })
        textArr.push(obj)
      }
      textObj[key] = textArr
    })
    // droebit gavaertianot -> textObj = { orderers: [...], testers: [...] }
    // return [...textObj.orderers, ...textObj.testers]
    return textObj
  }

  translate(params) {
    return axms.translateText(params)
  }

  push(data) {
    this.clog('i push data.data = ', data.data)
  }

  createResellAdminInvoices({ cardIds, serviceType }) {
    if (!cardIds || !mms.isArray(cardIds)) {
      return
    }
    const uploadPromises = []
    cardIds.forEach((cardId) => {
      uploadPromises.push(this.createResellAdminInvoice({ cardId, serviceType }))
    })
    Promise.all(uploadPromises)
      .then((invoiceIdArr) => {
        this.clog('cC invoiceIdArr = ', invoiceIdArr)
      })
      .catch((err) => {
        this.showSnackbar(
          this.t('qchat.invoice.upload.error.server_data') + ' err: ' + err,
          5000,
          'error',
          enums.caller.invoice
        )
      })
  }

  createResellAdminInvoice({ cardId, serviceType }) {
    this.clog('i createResellAdminInvoices = ', { cardId, serviceType })
    const requestObj = { resellCardId: cardId }
    return new Promise((resolve, reject) => {
      // ჯერ წავიკითხოთ resellCard
      axms
        .getDataById(requestObj)
        .then((data) => {
          this.clog('i fetchCardDetails success data = ', data)
          if (data && data.length) {
            const cardDetails = data[0]
            this.clog('i data cardDetails = ', cardDetails)
            const admin = true
            const invClass = new CreateInvoice({
              cardId,
              serviceType,
              cardDetails,
              admin,
              editable: !admin
            })
            // შევქმნათ invoice
            const invoiceObj = invClass.exportResellInvoice(admin) // invoiceObj -> { adminInv }
            this.clog('i fetchCardDetails invoiceObj = ', invoiceObj)
            if (invoiceObj) {
              axms
                .uploadInvoice(invoiceObj)
                .then((invoiceId) => {
                  resolve(invoiceId)
                  // handleUploadInvoiceSuccess(invoiceId)
                })
                .catch((err) => {
                  // handleUploadInvoiceError(err)
                  reject(err)
                })
            }
          }
        })
        .catch((err) => {
          this.clog('i fetchCardDetails failed, err = ', err)
          this.showSnackbar(
            this.t('qchat.invoice.upload.error.server_data') + ' err: ' + err,
            5000,
            'error',
            enums.caller.invoice
          )
          reject(err)
        })
    })
  }
}
Vue.prototype.$g = new QChat()
Vue.prototype.$enums = enums
initContextArr.forEach((el) => {
  if (el.initContext) {
    el.initContext(Vue.prototype.$g)
  }
})
// const getQuestionnaireStands = Vue.prototype.$g.getQuestionnaireValidateStands მურმანი აღარ ხმარობს
const createResellAdminInvoices = Vue.prototype.$g.createResellAdminInvoices // ქმნის ადმინის ინვოისებს resell-ის დროს ( { cardIds, serviceType } )
const translate = Vue.prototype.$g.translate // { text, from, to }, promise
const getQStandsArrForVin6 = Vue.prototype.$g.getQStandsArrForVin6 // vin6, returns promise-> [ { cardId, vin, locale, filled: { actualFilledFields, actualMustFilledFields, mustFilledFields, allFields } }, ... ]
const getQStandsArrForCarlink = Vue.prototype.$g.getQStandsArrForCarlink // link, returns promise-> [ { cardId, vin, locale, filled: { actualFilledFields, actualMustFilledFields, mustFilledFields, allFields } }, ... ]
const getCardIdArrQStands = Vue.prototype.$g.getCardIdArrQStands // cardIdArr, callback
const getCarouselTexts = Vue.prototype.$g.getCarouselTexts // param-> locale returns []
const getProductTexts = Vue.prototype.$g.getProductTexts // param-> locale returns []
const getFaqTexts = Vue.prototype.$g.getFaqTexts // param-> locale returns []
const getFeedbackTexts = Vue.prototype.$g.getFeedbackTexts // param-> locale returns []

const functions = {
  createResellAdminInvoices,
  getQStandsArrForVin6,
  getQStandsArrForCarlink,
  translate,
  getCardIdArrQStands,
  getCarouselTexts,
  getProductTexts,
  getFaqTexts,
  getFeedbackTexts
}
// export default { components, functions, ctx: Vue.prototype.$g }
// ამ ხრიკით გადმომცემს მურმანი hyperlink ფუნქციას
export default (hyperlink) => {
  Vue.prototype.$g.hyperlink = hyperlink
  return { components, functions, ctx: Vue.prototype.$g }
}
// Vue.prototype.$tr = g.app.$t
// Vue.prototype.$i18n = g.app.$i18n
// this.$i18n = this.app.$i18n
// console.log('i getProductTexts = ', getProductTexts('de'))
/* getCardIdArrQStands([10, 11, 2, 20], resp => {
  console.log('i getCardIdArrQStands resp = ', resp)
}) */
