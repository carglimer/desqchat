// import { uploadInvoice, downloadInvoice } from '../helpers/axiosMethods'
import moment from 'moment'
import { invoiceParams } from '../helpers/enums'
import { stp, toFixed2 } from '../helpers/methods'
let ctx // ctx = $g da ara this
export function initContext(_ctx) {
  ctx = _ctx
}

export class CreateInvoice {
  // constructor(app, store)
  constructor({ cardId, serviceType, cardDetails, admin, editable }) {
    this.cardId = cardId
    // this.role = role
    this.serviceType = serviceType
    this.editable = editable
    this.cardDetails = cardDetails
    this.admin = admin // memgoni ar aris aq sachiro

    this.invoiceItems = {}
    this.infoTexts = {}
    this.invoice = null
    this.summary = null
    this.stp = stp
    this.toFixed2 = toFixed2
  }

  exportResellInvoice(admin) {
    const inv = this.createInvoice(admin)
    this.roundPrice(inv)
    this.addTexts(inv)
    if (admin) {
      return { adminInv: inv }
    } else {
      return { inv }
    }
  }

  addTexts(inv) {
    const textsDe = ctx.readLangJSonfile('de').qchat.invoice
    const textsEn = ctx.readLangJSonfile('en').qchat.invoice
    const texts = { de: textsDe, en: textsEn }
    inv.texts = texts
  }

  roundPrice(inv) {
    if (inv) {
      inv.price = this.toFixed2(inv.price)
    }
  }

  toFixed2(price) {
    if (!price) {
      return 0
    }
    return Number.parseFloat(price).toFixed(2)
  }

  getName(privateData) {
    let name
    if (privateData.isJur) {
      name = privateData.descrip
    } else name = privateData.firstName + ' ' + privateData.lastName
    return name
  }

  getAdminPrice() {
    // ctx.clog('inv invoiceParams = ', invoiceParams)
    const cardDetails = this.cardDetails
    const user = ctx.store.getters.user
    // es cardDetails-shi unda ikos serviceName
    // const serviceName = cardDetails.serviceName
    const serviceName = invoiceParams.G1
    // dasamushavebelia
    const prices = ctx.gdata.adminData.prices
    // const adminPrice = (cardDetails.price * prices[serviceName]) / 100
    let adminPrice
    if (this.serviceType === invoiceParams.check) {
      const ourComissionRate =
        user && user.ourComission && user.ourComission.otherall && user.ourComission.otherall.rate
          ? user.ourComission.otherall.rate
          : prices[serviceName]
      adminPrice = (cardDetails.price * ourComissionRate) / 100
    } else if (this.serviceType === invoiceParams.resell) {
      // mashin cardDetails.price igive adminis price aris
      adminPrice = cardDetails.priceBuyer // asec sheizleba -> prices.adminResell
    }
    return adminPrice
  }

  // client invoiceNr
  createInvoiceNr(customerId) {
    // Rechnung-Nr: 3-250221.174533
    const formatedTs = moment.unix(Date.now() / 1000).format('DDMMYY.HHmmss') // 'DD.MM.YY HH:mm:ss'
    return customerId + '-' + formatedTs
  }

  // de -> Deutschland
  getCountryFromCountryAlpha2(code) {
    return ctx.readCountryFromCountryCodesJSonfile(code)
  }

  createInvoice(admin) {
    ctx.clog('inv createInvoice admin = ', admin)
    const issuerAndReceiver = this.createIssuerAndReceiver(admin)
    const issuer = issuerAndReceiver.issuer // invoice emmitent
    const receiver = issuerAndReceiver.receiver // invoice receiver
    const itemValues = this.getItemValues()
    // const date = moment.unix(Date.now() / 1000).format('DD.MM.YY') // '08/12/2020',
    // const dueDate = moment.unix(Date.now() / 1000).format('DD.MM.YY') // '18/12/2020',
    const ts = Date.now()
    const date = ts
    const dueDate = ts
    const nr = admin ? null : this.createInvoiceNr(issuer.recId)
    const vat = issuer.invoiceData.vatRate
    let testerPrice
    if (this.serviceType === invoiceParams.check) {
      testerPrice = itemValues.price
    } else if (this.serviceType === invoiceParams.resell) {
      testerPrice = this.toFixed2(this.cardDetails.priceTester)
    }
    const price = admin ? this.getAdminPrice() : testerPrice
    const invoice = {
      cardId: this.cardDetails.recId,
      admin: !!admin, // : issuer.admin
      serviceType: this.serviceType,
      ts,
      date,
      dueDate,
      nr,
      price,
      from: {
        nickname: { value: issuer.nickName, show: true },
        customerNr: { value: issuer.recId, show: true },
        vatCode: { value: this.stp(issuer.invoiceData.vatNumber), show: issuer.privateData.isJur },
        name: { value: this.getName(issuer.privateData), show: false },
        address: { value: this.stp(issuer.invoiceData.address), show: false },
        zipLocation: {
          value: this.stp(issuer.invoiceData.zip) + ' ' + this.stp(issuer.invoiceData.location),
          show: true
        },
        country: { value: this.getCountryFromCountryAlpha2(issuer.invoiceData.countryAlpha2), show: true }
      },
      to: {
        nickname: { value: receiver.nickName, show: true },
        customerNr: { value: receiver.recId, show: true },
        vatCode: { value: this.stp(receiver.invoiceData.vatNumber), show: receiver.privateData.isJur },
        name: { value: this.getName(receiver.privateData), show: false },
        address: { value: this.stp(receiver.invoiceData.address), show: false },
        zipLocation: {
          value: this.stp(receiver.invoiceData.zip) + ' ' + this.stp(receiver.invoiceData.location),
          show: true
        },
        country: { value: this.stp(this.getCountryFromCountryAlpha2(receiver.invoiceData.countryAlpha2)), show: true }
      },
      items: [
        {
          serviceName: itemValues.serviceName,
          description: itemValues.description,
          // speed: itemValues.speed,
          // level: itemValues.level,
          // quantity: 1,
          price
        }
      ],
      data: {
        agreed: false, // tu daetanxma gamgzavni kvela pirobas sakutari chascorebebis gatvaliscinebit
        nr,
        date: moment.unix(date / 1000).format('DD/MM/YY'), // '08/12/2020',
        dueDate: moment.unix(dueDate / 1000).format('DD/MM/YY'), // '18/12/2020',
        paymentType: 'paypal',
        paid: 0,
        currency: '€',
        juridical: issuer.privateData.isJur,
        vat, // : tester.invoiceData.vatRate, // '16',
        total: price,
        notes: {
          vatNoteValue: ''
        }
      }
    }
    ctx.clog('inv invoice = ', invoice)
    // მურმან -> შენ რომ გამოიძახებ qchat/resellCards-ს და მიიღებ ინფორმაციას ამ resellCard-ის შესახებ და როდესაც გამოსაგზავნ ობიექტში issuer.role="admin",
    // მაშინ admin: {}-ის დონეზე ჩამიდე კიდევ ერთი ელემენტ-ობიექტი: inform: { testerId: tester.recId, price: priceTester }
    if (this.serviceType === invoiceParams.resell) {
      invoice.testerId = this.cardDetails.tester.recId
      invoice.priceTester = this.toFixed2(this.cardDetails.priceTester)
    }
    this.invoice = invoice
    this.editable && this.initEditableData()
    this.initInvoiceItems()
    this.initInvoiceValues()
    return invoice
  }

  getItemValues() {
    const cardDetails = this.cardDetails
    let serviceName, description // description object არის, საიდანაც მიიღება -> Fahrzeugcheck-Normal-Allgemein
    const speed = cardDetails.speed
    const level = cardDetails.level
    const price = this.toFixed2(cardDetails.price)

    switch (this.serviceType) {
      case invoiceParams.check:
        serviceName = invoiceParams.G1
        description = { description: invoiceParams.check, speed, level }
        break
      case invoiceParams.resell:
        serviceName = invoiceParams.G2
        description = { description: invoiceParams.resell, level }
        break
      default:
        serviceName = invoiceParams.G1
        description = { description: invoiceParams.check, level }
        break
    }
    return {
      serviceName,
      description, // speed, level,
      price
    }
  }

  createIssuerAndReceiver(admin) {
    let issuer = {}
    let receiver = {}
    if (this.serviceType === invoiceParams.check) {
      if (admin) {
        // admin to tester
        issuer = ctx.gdata.adminData
        receiver = this.cardDetails.tester
      } else {
        // tester to orderer
        issuer = this.cardDetails.tester
        receiver = this.cardDetails.orderer
      }
    } else if (this.serviceType === invoiceParams.resell) {
      if (admin) {
        // admin to buyer -> admin amocers 12-evroian invoice-s
        issuer = ctx.gdata.adminData
        receiver = this.cardDetails.buyer
      } else {
        // tester to admin  -> tester amoucers admin 6-evroian invoice-s
        issuer = this.cardDetails.tester
        receiver = ctx.gdata.adminData
      }
    }
    return { issuer, receiver }
  }

  initEditableData(invoice = this.invoice) {
    if (!invoice) {
      return
    }
    const cardId = invoice.cardId
    let editableData = ctx.store.getters.invoices[cardId]
    if (!editableData) {
      // risi shecvlac sheuzlia testers
      editableData = {
        invoiceNr: invoice.data.nr,
        editedName: '',
        editedAdress: '',
        vat: +invoice.data.vat,
        vatNoteValue: ''
      }
      ctx.store.commit('setEditableData', { cardId, editableData })
    }
  }

  initInvoiceItems(invoice) {
    let adminInvoice = true
    if (!invoice) {
      invoice = this.invoice
      adminInvoice = false
    }
    if (!invoice) {
      return
    }
    invoice.headers = [
      { name: 'serviceName', header: 'service_name', text: '', width: '10%' },
      { name: 'description', header: 'description', text: '', width: '75%' }, // editable: false
      // { name: 'speed', header: 'speed', text: '', width: '20%' },
      // { name: 'level', header: 'level', text: '', width: '20%' },
      { name: 'bruttoValue', header: 'price', text: 0, width: '15%' }
    ]
    // tu adminInvoice izaxebs danarcheni ar gvchirdeba
    if (adminInvoice) {
      return
    }
    this.invoiceItems = {
      meta: { headerBg: '#C8E6C9' }, // green lighten-4
      // serviceItems-shi pirveli rigi srulia da aqedan vitvlit header-ebsac
      serviceItems: [],
      vatNettoItems: [
        {
          meta: { bg: '#D7CCC8' }, // brown lighten-4
          items: [
            { name: 'vatText', header: 'description', text: '', width: '40%' }, // header: 'speed'
            {
              name: 'vat',
              header: 'description', // header: 'level',
              text: 0,
              width: '20%',
              editable: true,
              type: 'number',
              align: 'center'
            },
            { name: 'vatValue', header: 'price', text: 0, width: '15%' }
          ]
        },
        {
          meta: { bg: '#D7CCC8' }, // brown lighten-4
          items: [
            { name: 'nettoText', header: 'description', text: '', width: '25%' }, // header: 'level'
            { name: 'nettoValue', header: 'price', text: 0, width: '15%' }
          ]
        }
      ]
    }
    invoice.items.forEach(() => {
      this.invoiceItems.serviceItems.push({
        meta: { bg: '#D7CCC8' }, // brown lighten-4
        // aq headers emtxveva pirvel xazs
        items: invoice.headers
      })
    })
  }

  initInvoiceValues(invoice = this.invoice) {
    if (!invoice) {
      return
    }
    const cardId = invoice.cardId
    const editableData = ctx.store.getters.invoices[cardId]

    this.juridical = invoice.data.juridical
    this.brutto = +invoice.data.total
    this.vat = editableData ? editableData.vat : +invoice.data.vat
    if (!this.juridical) {
      this.vat = 0
    }
    for (let i = 0; i < this.invoiceItems.serviceItems.length; i++) {
      const el = this.invoiceItems.serviceItems[i]
      for (const it of el.items) {
        if (it.name === 'serviceName') {
          // ar vtargmnit -> g2
          it.text = invoice.items[i].serviceName
        }
        if (it.name === 'description') {
          // vtargmnit
          it.text = '' // = ctx.t('qchat.invoice.descriptions.' + invoice.items[i].description)
          // description = object { description, speed, level }
          Object.keys(invoice.items[i].description).forEach((key) => {
            const value = invoice.items[i].description[key]
            it.text += ctx.t('qchat.invoice.' + key + '.' + value) + '-'
          })
          it.text = ctx.removeLastCharacterFromString(it.text)
        }
        /* if (it.name === 'speed') {
          it.text = ctx.t('qchat.invoice.speed.' + invoice.items[i].speed)
        }
        if (it.name === 'level') {
          it.text = ctx.t('qchat.invoice.level.' + invoice.items[i].level)
          // it.text = this.invoice.items[i].level
        } */
      }
    }
    this.countNettoBrutto()
  }

  countNettoBrutto(vat) {
    if (vat) {
      this.vat = vat
    }
    // vitvlit, roca erti servisia mxolod
    for (const item of this.invoiceItems.serviceItems) {
      for (const it of item.items) {
        if (it.name === 'bruttoValue') {
          it.text = this.toFixed2(this.brutto) + ' ' + this.invoice.data.currency
          ctx.clog('brutto = ', this.brutto)
        }
      }
    }
    for (const item of this.invoiceItems.vatNettoItems) {
      for (const it of item.items) {
        if (it.name === 'vat') {
          ctx.clog('vat = ', this.vat)
          it.text = this.vat
        }
        if (it.name === 'nettoValue') {
          this.netto = (+this.brutto * 100) / (100 + +this.vat)
          ctx.clog('netto = ', this.netto)
          it.text = this.toFixed2(this.netto) + ' ' + this.invoice.data.currency
        }
      }
    }
    // aq meored loop radgan jer netto-s gamotvla unda vacado
    for (const item of this.invoiceItems.vatNettoItems) {
      for (const it of item.items) {
        if (it.name === 'vatValue') {
          const vatValue = this.brutto - this.netto
          ctx.clog('vatValue = ', vatValue)
          it.text = this.toFixed2(vatValue) + ' ' + this.invoice.data.currency
        }
      }
    }
  }
}
