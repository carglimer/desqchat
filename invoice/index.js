// import { uploadInvoice, downloadInvoice } from '../helpers/axiosMethods'
let ctx // ctx = $g da ara this
export function initContext(_ctx) {
  ctx = _ctx
}
// აქ ენა არ გვაინტერესებს, მხოლოდ მონაცემები გვჭირდება
export function getInvoiceFromMongoByCardId({ cardId, serviceType, admin }) {
  if (!cardId) {
    ctx.showSnackbar(ctx.t('qchat.invoice.download.error.card_not_defined'), 2000, 'red', ctx.enums.caller.invoice)
    return
  }
  const requestObj = { cardId, serviceType }
  // serverze mnishvneloba aqvs admin===undefined tu ara
  if (admin) {
    requestObj.admin = admin
  }
  ctx.clog('ii requestObj = ', requestObj)
  ctx
    .getInvoiceByCardIdAndServiceType(requestObj)
    .then((invoiceArr) => {
      ctx.clog('ii data success invoiceArr = ', invoiceArr)
      if (invoiceArr) {
        handleGetInvoiceSuccess(invoiceArr)
      } else {
        ctx.showSnackbar(ctx.t('qchat.invoice.download.error.no_invoice'), 2000, 'red', ctx.enums.caller.invoice)
      }
    })
    .catch((err) => {
      handleGetInvoiceError(err)
      // ctx.showSnackbar(err, 2000, 'red', ctx.enums.caller.questionnaire)
    })
}
// cardId = ctx.store.getters.card.id
// data = { cardId, admin:true||false}
export function downloadInvoiceFromServer(data) {
  ctx.clog('ii downloadInvoiceFromServer data = ', data)
  // აქ უნდა განვსაზღვროთ თუ რომელ ენაზე გვინდა ჩამოტვირთვა
  // if tester, mashin de, else if orderer lang !=de, mashin en
  if (!data.cardId && ctx.store.getters.card) {
    data.cardId = ctx.store.getters.card.id
    ctx.clog('ii downloadInvoiceFromServer2 cardId = ', data.cardId)
  }
  if (!data.cardId) {
    ctx.clog('ii downloadInvoiceFromServer returned cardId = ', data.cardId)
    return
  }
  let lang = 'de'
  if (ctx.userIsTester()) {
    lang = 'de'
  } else if ((ctx.userIsOrderer() || ctx.userIsBuyer()) && ctx.store.getters.locale !== 'de') {
    lang = 'en'
  }
  const invoiceData = { ...data, lang }
  ctx.clog('ii invoiceData = ', invoiceData)
  ctx
    .downloadInvoice(invoiceData)
    .then(() => {
      handleDownloadInvoiceSuccess(invoiceData)
    })
    .catch((err) => {
      handleDownloadInvoiceError(err, invoiceData)
    })
}
function handleGetInvoiceSuccess(invoiceArr) {
  ctx.clog('ii handleGetInvoiceSuccess ')
  ctx.chatEBus.$emit(ctx.enums.events.invoiceGetSucceeded, invoiceArr)
}
function handleGetInvoiceError(err) {
  ctx.clog('ii handleGetInvoiceError err = ', err)
  // ctx.chatEBus.$emit(ctx.enums.events.invoiceDownloadError, err)
  ctx.showSnackbar(ctx.t('qchat.invoice.download.error.failed'), 2000, 'error', ctx.enums.caller.invoice)
}
function handleDownloadInvoiceSuccess(invoiceData) {
  ctx.clog('ii handleDownloadInvoiceSuccess ')
  // showSnackbar(text, timeout, color, _caller) {
  ctx.chatEBus.$emit(ctx.enums.events.invoiceDownloaded, invoiceData)
  // ctx.showSnackbar(ctx.t('qchat.invoice.download.succesfully_downloaded'), 2000, 'green', ctx.enums.caller.invoice)
}
function handleDownloadInvoiceError(err, invoiceData) {
  ctx.clog('ii handleUploadInvoiceError err = ', err)
  ctx.chatEBus.$emit(ctx.enums.events.invoiceDownloadError, { err, invoiceData })
  // error-istvis infoDialog fanjara amodis invoice.vue-shi
}
export function uploadClientAndAdminInvoiceToServer({ inv, adminInv, invoke }) {
  ctx.clog('ii uploadClientAndAdminInvoiceToServer { inv, adminInv, invoke } = ', { inv, adminInv, invoke })
  uploadInvoiceToServer({ inv, adminInv })
    .then((respArr) => {
      ctx.clog('ii respArr = ', respArr)
      if (!respArr || (respArr && respArr.err)) {
        handleUploadInvoiceError('Error inv respArr.error : ' + respArr)
        return
      }
      // resp = { update: false||true, insertedId: upsertedId||null }
      // invoiceIdArr[0] -> checker-is invoisis aris
      handleUploadInvoiceSuccess({ resp: respArr[0], invoke })
      /* uploadInvoiceToServer(adminInv)
        .then((invoiceId) => {
          handleUploadInvoiceSuccess(invoiceId)
        })
        .catch((err) => {
          handleUploadInvoiceError('Error adminInv: ' + err)
        }) */
    })
    .catch((err) => {
      handleUploadInvoiceError('Error inv: ' + err)
    })
}
export function uploadInvoiceToServer(invoice) {
  ctx.clog('ii uploadInvoiceToServer invoice = ', invoice)
  return new Promise((resolve, reject) => {
    ctx
      .uploadInvoice(invoice)
      .then((invoiceId) => {
        resolve(invoiceId)
        // handleUploadInvoiceSuccess(invoiceId)
      })
      .catch((err) => {
        // handleUploadInvoiceError(err)
        reject(err)
      })
  })
}
function handleUploadInvoiceSuccess(responseData) {
  ctx.clog('ii handleUploadInvoiceSuccess responseData = ', responseData)
  // showSnackbar(text, timeout, color, _caller) {
  ctx.chatEBus.$emit(ctx.enums.events.invoiceUploaded, responseData)
  // ctx.chatEBus.$emit(ctx.enums.events.invoiceUploadedForUploader, responseData)
}
function handleUploadInvoiceError(err) {
  ctx.clog('ii handleUploadInvoiceError err = ', err)
  ctx.chatEBus.$emit(ctx.enums.events.invoiceUploadError, err)
  // ctx.chatEBus.$emit(ctx.enums.events.invoiceUploadErrorForUploader, err)
  // error-istvis infoDialog fanjara amodis invoice.vue-shi
}
