<div id="main" style="font-size: 18px;">  
  <h3 style="text-align: center; color: red">
    Wunschfahrzeug vor Ort checken lassen
  </h3>
  <br />
  <br />  
  <h4>Was kann alles schiefgehen bei einem Fahrzeugkauf über Internetportale?</h4>
  <br />  
  <p>Wie die Erfahrung sagt, ziemlich viel.</p>
  <p>Hier wird eine typische Reihenfolge aufgeführt, womit normalerweise der Fahrzeugkauf übers Internet abgewickelt wird:</p>
  <ul style2="list-style-type:none;">
    <li>Es wird auf einer Fahrzeuganbieterplattform (z.B. ebay.de, ebay-kleinanzeigen.de, mobile.de, autoscout24.de oder andere) ein Auto ausgesucht.</li>
    <br />
    <li>Der Fahrzeugverkäufer wird über Telefon, Chat oder E-Mail kontaktiert, einige Fragen (wie Zustand, Vorbesitzer, zusätzliche Fotos vom Fahrzeug, Preis und so weiter) werden gestellt.</li>
    <br />
    <li>Besteht das Interesse immer noch, wird ein Besichtigungstermin vereinbart.</li>
    <br />
    <li>Der Käufer versucht jemanden aus seinem Bekanntenkreis zu finden, der ihm beim Fahrzeugchecken behilflich sein kann (der Autokauf ist ein finanzieller Kraftakt und niemand will auf einer Rostgurke, einem Unfallfahrzeug oder Mängelstauexponat sitzenbleiben), um schließlich gemeinsam zum Fahrzeug zu fahren.</li>
    <br />
    <li>Das Fahrzeug wird besichtigt, gecheckt, der Preis noch einmal verhandelt und letztendlich wird das Auto gekauft oder vom Kauf abgesehen.</li>
  </ul>
  <br />
  <br />
  <h4 class="bold">Somit ist alles klar, alles gut. Aber können dabei Probleme entstehen?</h4>
  <br />
  <p>Leider, auch hier zeigt die Erfahrung: ja, und zwar recht viele.</p>
  <p>Hier sind einige davon aufgeführt:</p>
  <ul style="list-style-type: none">
    <li>- Nicht selten sind die Fahrzeugverkäufer mit den vielen Fahrzeuginteressenten überfordert, verlieren leicht den Überblick und bieten gleich mehreren potentiellen Käufern die Fahrzeugbesichtigung an (nach dem Motto: wer zuerst kommt, bekommt das Ding). Dabei wird übersehen, dass andere schon unterwegs sind, bereits Unkosten an Zeit und Geld gemacht haben und der Zoff ist damit vorprogrammiert.</li>
    <br />
    <li>- Das Auto entspricht nicht oder gar nicht all dem, was im Inserat beschrieben wurde.</li>
    <br />
    <li>- Im Inserat wurde der Verkäufer als 'privat' angegeben, tatsächlich erschien ein verdeckter Autohändler, wodurch die Erwartung, ein Auto günstig und aus direkter Hand zu kaufen, zunichte gemacht wird.</li>
    <br />
    <li>- Es ist ein Betrüger am Werk und dieses Auto existiert gar nicht oder die Fahrzeugpapiere sind manipuliert worden.</li>
    <br />
    <li>- Es findet sich niemand, der zu diesem Besichtigungstermin beim Fahrzeugchecken helfen kann.</li>
    <br />
    <li>- Das Fahrzeug ist so heiß begehrt, dass möglichst schnell ein Kaufvertrag geschlossen werden soll, aber der Käufer befindet sich weit vom Fahrzeugstandort entfernt.</li>
  </ul>
  <br />
  <br />
  <h4>Genau dieser Probleme haben wir uns angenommen und die einleuchtende Lösung gefunden:</h4>
  <br />
  <p>Was wenn ein Netzwerk von engagierten Fahrzeugkennern geschaffen wird, die für kleines Geld bereit sind in Ihrem Umkreis Fahrzeugbesichtigungsaufträge zu bekommen, zum Fahrzeugstandort zu fahren, dort das Auto nach einem umfangreichen digitalen Fragebogen zu checken, dabei der Checker per App mit dem Auftraggeber in ständigem Kontakt bleibt, falls nötig Fotos, Audio oder Videofiles vom Fahrzeug dem Auftraggeber sendet und die zusätzlichen Fragen an den Fahrzeugverkäufer live weiterleitet?</p>
  <p>Somit ist der Käufer in der Lage eine Entscheidung zu treffen, die vergleichbar mit jener ist, wenn man selbst vor Ort wäre.</p>
  <p>Am Ende bewertet der Auftraggeber den Checker mit einem 5 Sterne Bewertungssystem und zusätzlichem Text. Daraus wird ein Rating gebildet, welches die künftigen Auftraggeber einsehen können.</p>
  <p>Durch Bewertungen wird erheblich dazu beigetragen, dass die Checker ihren Aufgaben gewissenhaft nachgehen.</p>
  <span>Der Fahrzeugcheck kann in 2 Stufen durchgeführt werden:</span>
  <br />  
  <span class="hyperlink" data-to="infos-QuestionnaireExampleBasic">allgemeinen</span> und in <span class="hyperlink" data-to="infos-QuestionnaireExampleAdvanced">erweiterten</span> Qualität. Dafür wurden 2 Stufen von Fragebogen zusammengefasst. Außerdem lässt sich das Feature 'dringend' aktivieren.
  <br />
  <br />
  <h4>Der ganze Vorgang funktioniert folgendermaßen:</h4>
  <br />
  <p>
    Nachdem der Fahrzeugsuchende ein Wunschauto auf einer Internetplattform ausgesucht hat, kopiert dieser dessen Link und gibt über deservice.net den Auftrag unter Angabe der PLZ des Fahrzeugstandortes. Die Fahrzeugchecker, die bei deservice.net angemeldet sind, hinterlassen das PLZ-Areal, welches sie bedienen können.
  </p>
  <ul style="list-style-type: decimal; list-style-position: inside">
    <li>Die Checker werden informiert, die diese PLZ bedienen können.</li>
    <li>Die Checker, die in dem Moment bereit sind, diesen Auftrag entgegenzunehmen, melden sich beim Auftraggeber.</li>
    <li>Der Kunde (Auftraggeber) entscheidet sich anhand der folgenden Daten zum Checker: Rating, Preis, Ausführungsstufe (<span class="hyperlink" data-to="infos-QuestionnaireExampleBasic">allgemein,</span> <span class="hyperlink" data-to="infos-QuestionnaireExampleAdvanced">erweitert</span>) und Dringlichkeit, welchen Checker er für den Auftrag auswählt und gibt diesem den Auftrag und den Link des Fahrzeuges.</li>
    <li>Der Auftraggeber überweist den vom Checker festgelegten Preisbetrag auf das Konto von deservice.net.</li>
    <li>Der Checker wird von deservice.net informiert, dass die Bezahlung steht und es losgehen kann.</li>
    <li>Zwischen dem Kunden und dem Checker wird ein Web-Chat aktiviert, über den sie ständig miteinander in Kontakt treten können.</li> 
    <li>Der Checker nimmt den Kontakt mit dem Fahrzeugverkäufer auf und fährt zum Auto.</li>
    <li>Falls nach der Kontaktaufnahme aus welchem Grund auch immer zum Auto nicht gefahren werden kann, wird der Auftraggeber darüber informiert und dieser kann den Auftrag mit maximal 25% des Preises stornieren.</li>
    <li>Dort angekommen füllt er den Fahrzeugcheckfragebogen aus, steht dabei per integrierter Chat-Funktion mit dem Auftraggeber in Verbindung und sendet auf Wunsch zusätzliche Fotos oder Videos. Der Checker bleibt für den Auftraggeber die ganze Zeit anonym, er kennt nur seinen Nicknamen. Falls der Auftraggeber aus dem Ausland kommt, wird die ganze Chat-Kommunikation in die jeweilige Sprache mittels Google-Translator live übersetzt.</li>
    <li>Der Fragebogen besteht aus erforderlichen (in Fragebogen mit Schneeflocken-Symbol gekennzeichent) und freiwilligen Feldern. Sind alle ereforderlichen Felder ausgefüllt, gilt der Auftrag als erfüllt.</li>
    <li>Ist der Auftraggeber mit dem Fahrzeug zufrieden, verhandelt er mit dem Checker nochmal den Endpreis und trifft die Entscheidung zum Autokauf oder Nichtkauf.</li>
    <li>Damit ist dieser Auftrag für den Checker erledigt und die Kommunikation zwischen ihnen ist nicht mehr möglich.</li>
    <li>Im Falle eines Kaufwunsches, kann der Checker Email-Kontakt zwischen den Parteien erstellen, indem er beim Verkäufer eine Emailadresse erfragt und an den Auftraggeber weiterleitet. Der sendet dem Fahrzeugverkäufer den von ihm ausgefüllten und unterzeichneten Kaufvertrag oder Vorvertrag und bekommt ihn von diesem unterzeichnet zurück.</li>
    <li>Der Auftraggeber bewertet diesen Checker auf deservice.net mit einem 5 Sterne-System und zusätzlichem Bewertungstext.</li>
  </ul>
  <br />
</div>