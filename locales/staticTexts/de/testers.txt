<div id="main" style="font-size: 18px;">
<h3 style="text-align: center;color:red;">
  Fahrzeugchecker
</h3>
<br>
<br>
<h4>
  <p>Sind Sie Autohändler und haben Sie ab und zu freie Zeit um Ihr Augenmerk auf ein eventuelles Kaufobjekt in Ihrer Nähe zu richten oder einfach Ihre Kaffeekasse aufzubessern?</p>
  <p>Sind Sie kein Autohändler, haben Sie ab und zu freie Zeit, sich mehr oder weniger mit Autos auskennen und wollen Sie Ihr Taschengeld aufbessern?</p>
</h4>
<p>Dann sind Sie bei uns genau richtig!</p>
<p>Wir suchen die engagierten Leute, die bereit sind, Fahrzeugcheckaufträge zu bekommen und gewissenhaft auszuführen.</p>
<p>Die Aufträge kommen von den Personen sowohl vom Inland als auch vom Ausland, die sich im Internet auf der Suche nach einem Wunschauto begeben haben.</p>
<p>Was soll ein Fahrzeugchecker können?</p>
<p>Je nachdem, wie Sie sich mit Autos auskennen, haben wir einen Fragenkatalog in zwei Stufen zusammengefasst:  <span class="hyperlink" data-to="infos-QuestionnaireExampleBasic">allgemein</span> und <span class="hyperlink" data-to="infos-QuestionnaireExampleAdvanced">erweitert</span>.</p>
<p>Welche Aufgaben kommen auf einen Fahrzeugchecker zu?</p>
<ul style2="list-style-type:none;">
  <li>Einen Auftrag entgegennehmen</li>
  <br>
  <li>Sich mit dem Fahrzeugverkäufer in Verbindung setzen, einen Besichtigungstermin im Namen des Auftraggebers vereinbaren.</li>
  <br>
  <li>Zum Fahrzeugstandort in seiner Nähe fahren und das Auto besichtigen.</li>
  <br>
  <li>Das Fahrzeug nach dem Fragenkatalog(hier links) durchchecken, der von deservice.net zur Verfügung gestellt wird.</li>
  <br>
  <li>Während der Fahrzeugprüfung live per Chat mit dem <span class="hyperlink" data-to="tutorials-orderer">Kunden</span> in Kontakt bleiben und auf eventuelle zusätzliche Fragen antworten und/oder die Antworten des Verkäufers (wie Vorbesitzer, Unfallschäden, Endpreis u.s.w) an den Auftraggeber weiterleiten.</li>
  <br>
  <li>Im Falle des Kaufwunsches seitens der Auftraggeber die Emailadresse des Verkäufers erfragen und dem Auftraggeber weiterleiten, damit die beiden den Kauvertrag unterzeichnen und miteinander austauschen.</li>
</ul>
<br>
<p></p>
<p>Damit es später zu keinem Missverständnis kommt, bleibt der Fahrzeugchecker die ganze Zeit für den Auftraggeber anonym, nur der Nickname des Checkers wird preisgegeben.</p>
<p>Der ganze Vorgang funktioniert folgendermaßen:</p>
<p>
Nachdem der Fahrzeugsuchende ein Wunschauto auf einer Internetplattform ausgesucht hat, kopiert er dessen Link und gibt über deservice.net den Auftrag unter Angabe der PLZ des Fahrzeugstandortes. Die Fahrzeugchecker, die bei deservice.net angemeldet sind, hinterlassen das Enfernungsradius und damit das PLZ-Areal, welches sie bedienen können. 
</p>
<ul style="list-style-type: decimal; list-style-position: inside">
    <li>Die Checker werden informiert, die diese PLZ bedienen können.</li>
    <li>Die Checker, die in dem Moment bereit sind, diesen Auftrag entgegenzunehmen, melden sich beim Auftraggeber.</li>
    <li>Der Kunde (Auftraggeber) entscheidet sich anhand der folgenden Daten zum Checker: Rating, Preis, Ausführungsstufe (<span class="hyperlink" data-to="infos-QuestionnaireExampleBasic">allgemein,</span> <span class="hyperlink" data-to="infos-QuestionnaireExampleAdvanced">erweitert</span>) und Dringlichkeit, welchen Checker er für den Auftrag auswählt und gibt diesem den Auftrag und den Link des Fahrzeuges.</li>
    <li>Der Auftraggeber überweist den vom Checker festgelegten Preisbetrag auf das Konto von deservice.net.</li>
    <li>Der Checker wird von deservice.net informiert, dass die Bezahlung steht und es losgehen kann.</li>
    <li>Zwischen dem Kunden und dem Checker wird ein Web-Chat aktiviert, über den sie ständig miteinander in Kontakt treten können.</li> 
    <li>Der Checker nimmt den Kontakt mit dem Fahrzeugverkäufer auf und fährt zum Auto.</li>
    <li>Falls nach der Kontaktaufnahme aus welchem Grund auch immer zum Auto nicht gefahren werden kann, wird der Auftraggeber darüber informiert und dieser kann den Auftrag mit maximal 25% des Preises stornieren.</li>
    <li>Dort angekommen füllt er den Fahrzeugcheckfragebogen aus, steht dabei per integrierter Chat-Funktion mit dem Auftraggeber in Verbindung und sendet auf Wunsch zusätzliche Fotos oder Videos. Der Checker bleibt für den Auftraggeber die ganze Zeit anonym, er kennt nur seinen Nicknamen. Falls der Auftraggeber aus dem Ausland kommt, wird die ganze Chat-Kommunikation in die jeweilige Sprache mittels Google-Translator live übersetzt.</li>
    <li>Der Fragebogen besteht aus erforderlichen (in Fragebogen mit Schneeflocken-Symbol gekennzeichent) und freiwilligen Feldern. Sind alle ereforderlichen Felder ausgefüllt, gilt der Auftrag als erfüllt.</li>
    <li>Ist der Auftraggeber mit dem Fahrzeug zufrieden, verhandelt er mit dem Checker nochmal den Endpreis und trifft die Entscheidung zum Autokauf oder Nichtkauf.</li>
    <li>Damit ist dieser Auftrag für den Checker erledigt und die Kommunikation zwischen ihnen ist nicht mehr möglich.</li>
    <li>Im Falle eines Kaufwunsches, kann der Checker Email-Kontakt zwischen den Parteien erstellen, indem er beim Verkäufer eine Emailadresse erfragt und an den Auftraggeber weiterleitet. Der sendet dem Fahrzeugverkäufer den von ihm ausgefüllten und unterzeichneten Kaufvertrag oder Vorvertrag und bekommt ihn von diesem unterzeichnet zurück.</li>
    <li>Der Auftraggeber bewertet diesen Checker auf deservice.net mit einem 5 Sterne-System und zusätzlichem Bewertungstext.</li>
  </ul>
  <br />
</div>