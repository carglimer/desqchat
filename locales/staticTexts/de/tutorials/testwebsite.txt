<div id="main" style="font-size: 18px;">  
  <h3 style="text-align: center; color: red">
  Übungsplatz - die Funktionen und Möglichkeiten der Webseite mithilfe der Testseite kennenlernen
  </h3>
  <br />
  <br />  
  <p>Die Funktionen, die unsere Webseite bietet, sind ziemlich umfangreich und komplex.</p>
  <p>Um den Kunden und den Checkern die Eingewöhnung in die verschiedenen Funktionen des deservice.net zu erleichtern, haben wir für Testzwecke die Webseite <a id="testDeservice" target="_blank">www.test.deservice.net</a> eingerichtet, wo unsere Nutzer absolut unverbindlich alle Funktionen und Möglichkeiten der Webseite ausprobieren können. Sie können mehrere Konten anlegen und auf einem Konto als Checker und auf dem Anderen als Auftraggeber auftreten. So werden Sie imstande sein, verschiedene Situationen zu modellieren, wie: Aufträge erteilen, mit Startguthaben bezahlen, entgegennehmen, stornieren, Friste verlängern, Rechnung ausstellen, Bewertungen schreiben und vieles mehr.</p>
  <p>Nachdem Sie sich auf der Test-Webseite ausgetobt haben, empfehlen wir Ihnen, die Rolle des Checkers auf 'pausieren' zu stellen, damit Sie von anderen Testnutzern nicht als Checker angeschrieben bzw. belästigt werden.</p>
  <br />
  <h4>Las uns versuchen zu erklären, warum wir es für notwendig hielten, eine Übungswebsite einzurichten.</h4>
  <br />
  <p>Die Aufgaben des Auftraggebers (der Kunde) sind schnell erzählt:</p>  
  <p>Im Netz ein Wunschfahrzeug auswählen, im Radius des Fahrzeugstandortes anhand der Bewertungen und Preise einen Checker auswählen, Auftrag erteilen, die Vergütung für Checker auf das Paypal-Konto der Webseite einzahlen, während der Fahrzeugprüfung ständigen Kontakt mit dem Checker halten, den hochgeladenen Fragebogen studieren und am Ende die Arbeit des Checkers bewerten.</p>
  <p>Die Aufgaben des Checkers können auch kurz gefasst werden:</p>
  <p>Den Auftrag entgegennehmen, den Fahrzeugverkäufer kontaktieren, dorthin fahren, das Fahrzeug nach umfangreichem Online-Fragebogen durchchecken, etwaige zusätzlichen Fragen des Kundes beantworten, den Fragebogen hochladen, dem Auftraggeber die Online-Rechnung ausstellen und die Vergütung erhalten.</p>
  <p>Klingt alles nicht sehr kompliziert, aber es können verschiedene Situationen während der Ausführung eines Auftrags entstehen. Einige Fragen und Antworten darauf sind hier aufgeführt worden:</p>
  <p>1. Auftrag wurde erteilt, bezahlt, der Checker hat sich beim Fahrzeugverkäufer gemeldet und er hat das Fahrzeug schon verkauft oder kann die Fahrzeugbesichtigung im vorgegebenen Zeitraum nicht gewähren.</p>
  <p>Auflösung: In diesem Fall kann der Kunde den Auftrag stornieren mit oder ohne Entschädigung für den Checker.</p>
  <p>Nachdem der Auftrag eröffnet wurde, können sich der Kunde und der Checker über Chatfenster und Infofenster miteinander kommunizieren.</p>
  <img class="testwebsite">
  <br />
  <br />
  <p>Der Auftrag kann einvernehmlich storniert werden. Die Stornierung erfolgt vom Kunden. Er bestimmt auch die Höhe der Entschädigung und zwar maximal bis zu 25 % des Auftragsvolumens. Die Entschädigung kann über das Infofenster festgelegt und durch das Klicken auf 'Anbieten' - Button dem Checker mitgeteilt werden, welcher wiederum das Angebot ablehnen oder dem zustimmen kann.</p>
  <p>Der Checker kann über das Infofenster dem Kunden um die Fristverlängerung bieten. Der Kunde wiederum darf der Anfrage zustimmen oder sie ablehnen.</p>
  <p>Die Gründe für Stornierung oder Fristverlängerung werden im Chat zwischen den Seiten besprochen und dürfte daraus keine Konflikte entstehen.</p>
  <p>2. Der Checker war bereits auf dem Weg zum Fahrzeugstandort als der Fahrzeugbesitzer ihm den Fahrzeugverkauf mitgeteilt hat.</p>
  <p>Auflösung: In diesem Fall kann der Auftrag wie im oberen Fall einvernehmlich mit oder ohne Entschädigung storniert werden.</p>
  <p>3. Der Kunde möchte ein Fahrzeug prüfen lassen, welches bereits von einem anderen Kunden in Auftrag gegeben wurde und der Prüfungsfragebogen erstellt und hochgeladen wurde.</p>
  <p>Auflösung: Wir haben diese Möglichkeit berücksichtigt und Auf der Webseite 'Können wir helfen?' - Bereich eingebaut.</p>
  <img class="testwebsite">
  <p>Hier können Sie die PLZ des Fahrzeugstandortes eingeben und Sie werden auf das nächste Fenster weitergeleitet, wo die Checkers aufgelistet werden, die für diese PLZ zuständig sind. Außerdem können Sie hier den Fahrzeuglink und/oder die letzten 6 Zeichen der Fahrzeugidentifikationsnummer (Falls Ihnen bekannt) eingeben. Anhand dieser Daten wird geprüft ob dieses Fahrzeug schon mal gecheckt wurde. Falls ja, kann der Kunde den vorhandenen Fragebogen für einen geringen Preis direkt kaufen.</p>
  <p>Wie Sie hier sehen, es können viele Situationen entstehen. Deshalb haben wir uns entschieden für Übungszwecke eine parallele Webseite einzurichten.</p>
    
</div>