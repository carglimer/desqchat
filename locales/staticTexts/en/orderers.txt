<div id="main" style="font-size: 18px;">
<h3 style="text-align: center;color:red;">
  Have the vehicle of your choice checked on site
</h3>
<br>
<br>
<h4>What can go wrong when buying a vehicle via internet portals?</h4>
<p>As experience suggests, quite a bit.</p>
<p>A typical order is listed here in which the vehicle purchase is normally carried out over the Internet:</p>
<ul style2="list-style-type:none;">
  <li>A car is selected on a vehicle provider platform (e.g. ebay.de, ebay-kleinanzeigen.de, mobile.de, autoscout24.de or others).</li>
  <br>
  <li>The vehicle seller is contacted by phone, chat or email, some questions (such as condition, previous owner, additional photos of the vehicle, price and so on.) Are asked.</li>
  <br>
  <li>If there is still interest, an appointment will be made.</li>
  <br>
  <li>The buyer tries to find someone from his circle of friends who can help him with the vehicle check (buying a car is a financial feat and nobody wants to be stuck on a rusty pickle, an accident vehicle or a defective jam exhibit), and they drive there together.</li>
  <br>
  <li>The vehicle is viewed, checked, the price is traded again and ultimately the car is bought or not bought.</li>
</ul>
<br>
<br>
<h4>So everything is clear, everything is fine. But can this cause problems?</h4>
<br />
<p>Unfortunately, as reality shows, yes, and quite a few.</p>
<p>Here are some of them:</p>
<ul style="list-style-type:none;">
  <li>- It is not uncommon for vehicle sellers to be overwhelmed by the large number of people interested in vehicles, to easily lose track and offer several potential buyers to view the vehicle (according to the motto: whoever comes first gets the thing). This overlooks the fact that others are already on the way, have already made expenses in terms of time and money and the Zoff is inevitable.</li>
  <br>
  <li>- The car does not correspond or not at all to what was described in the advertisement.</li>
  <br>
  <li>- In the advertisement the seller was stated as 'private', in fact a covert car dealer appeared and the expectation of buying a car cheaply and directly from the company was destroyed.</li>
  <br>
  <li>- There is a fraudster at work and this car does not even exist or the vehicle documents have been tampered with.</li>
  <br>
  <li>- There is no one who can help with the vehicle check on this inspection appointment.</li>
  <br>
  <li>- The vehicle is so popular that a sales contract should be concluded as quickly as possible, but the buyer is far from the vehicle location.</li>
</ul>
<br>
<br>
<h4>We have taken on exactly these problems and the obvious solution has been found:</h4>
<br>
<p>What if a network of committed vehicle checkers is created who are willing to get vehicle inspection orders in your area for little money, drive to the vehicle location, check the car there according to an extensive digital questionnaire, while the checker remains in constant contact with the client via app , if necessary, sends photos, audio or video files from the vehicle to the client and forwards the additional questions live to the vehicle seller?</p>
<p>Thus, the buyer is able to make a decision that is comparable to that as if he were there.</p>
<p>In the end, the client rates the checker with a 5 star rating system and additional text. From this a rating is formed, which future clients can see.</p>
<p>Assessments go a long way towards ensuring that checkers approach their tasks carefully.</p>
<span>Vehicle check can be carried out in 2 stages:</span>
<br>
<span class="hyperlink" data-to="infos-QuestionnaireExampleBasic">basic</span> and in <span class="hyperlink" data-to="infos-QuestionnaireExampleAdvanced">advanced</span> quality. For this, 2 levels of questionnaires were combined. In addition, the 'urgent' feature can be activated.</span>
<br>
<br> 
<h4>The whole process works something like this:</h4>
<br> 
<p>
After the vehicle seeker has selected the vehicle of their choice on an Internet platform, copies its link and places the order via deservice.net, stating the postcode of the vehicle location. The vehicle checkers who are registered with DeService leave the postcode area that they can use.
</p>
<ul style="list-style-type: decimal; list-style-position: inside">
  <li>The checkers are informed who can serve this postcode.</li>
  <li>The checkers, who are ready to accept this order at that moment, contact the client.</li>
  <li>The orderer decides on the basis of the following data about the checker: rating, price, execution level (<span class="hyperlink" data-to="infos-QuestionnaireExampleBasic">basic,</span> <span class="hyperlink" data-to="infos-QuestionnaireExampleBasic">advanced</span>) and urgency, which checker to select for the order and gives the order and the link to the vehicle.</li>
  <li>The orderer transfers the price set by the checker to the deservice.net account.</li>
  <li>The checker is informed by the deservice.net.netvice that the payment has been made and it can start.</li>
  <li>A web chat is activated between the client and the checker, through which they can constantly contact each other.</li>
  <li>The checker contacts the vehicle seller and drives to the car.</li>
  <li>If, for whatever reason, after contacting us, driving to the car does not make sense (e.g. the car has already been sold or the vehicle buyer could not be reached), the orderer will be informed and the order will be canceled with the agreement of the client (cancellation costs may not exceed 20% of the service price).</li>
  <li>Once there, he fills out the vehicle check questionnaire, communicates with the client via the messenger app, and sends additional photos or videos to the client on request. The checker remains anonymous to the client at all times, he only knows his nickname. If the client comes from abroad, all chat communication is translated live into the respective language.</li>
  <li>The questionnaire consists of required (marked with the snowflake symbol in the questionnaire) and voluntary fields. If all required fields are filled in, the order is considered fulfilled.</li>
  <li>If the client is satisfied with the vehicle, he negotiates the final price again via the checker and makes the decision to buy or not to buy the car.</li>
  <li>This has done this job for the checker and communication between them is no longer possible.</li>
  <li>In the event of a purchase request, the checker can establish email contact between the parties by asking the seller for an email address and forwarding it to the client. He sends the vehicle seller the completed and signed sales contract or preliminary contract and receives it back signed by him.</li>
  <li>The client rates this checker on deservice.net with a 5 star system and additional text.</li>
 </ul>
 <br>
</div>