<div id="main">
<h3 >
  Principle of communication between deservice.net users
</h3>
<br>
<h4 >
  Chat communication
</h4>
Members can send messages to each other via deservice.net. We support open communication between our users. However, we do not allow our users to use these options to send spam, offers to buy and sell vehicles outside of deservice.net, threats, insults or hate speech. Members are also not permitted to offer, refer to, or solicit email addresses, telephone numbers or other contact information, physical addresses, web addresses or links in the messaging systems.
<br>
Please note that we cannot exercise any control over communication if it takes place outside of deservice.net. General information on the misuse of personal data, especially in the area of vehicle sales, can be found on the website <a id="safetyCarPurchase" target="_blank"> the initiative 'Safer Car Buying' </a> on the Internet.
<br>
<br>
<h4 >
  Threats
</h4>
We do not tolerate threats of physical violence. It doesn't matter which method is used - via chat, email or our public forums.
<br>
Please report this to the relevant law enforcement authorities. You should also consider contacting your telephone company if you have been threatened by phone or contacting the other member's Internet service provider if you received the threat via email.
<br>
<br>
<h4 >
  Insulting, vulgar or hateful language
</h4>
Users are prohibited from using offensive, vulgar or hateful language in their communications with other members on deservice.net.
<br>
<br>
<h4 >
  Removal of review comments with inappropriate content
</h4>
The following evaluation comments can be removed:
  <ol style="list-style-type:1;">
  <li>Reviews that contain information that can be used to identify other users, such as: B. Address, telephone number, email address or the real name of a user. In addition, we can also remove reviews that can be used to access personal data of other users.</li>
  <li>Reviews that are not related to deservice.net. This also applies to comments, replies or additional comments that refer to another order</li>
  <li>Reviews that contain comments on political, religious, social or other topics and are not related to deservice.net</li>
  <li>Reviews that contain degrading, vulgar, obscene or racist language, as well as comments that use adult language or threaten physical violence</li>
  <li>Negative review comments that directly contradict a positive review rating</li>
  <li>Reviews that contain links or advertising messages that encourage users to buy items outside of deservice.net</li>
  <li>Reviews that contain references to a criminal investigation or investigation by deservice.net or PayPal</li>
</ol>
<br>
<br>
Email:  <span id="principlesEmail"></span>
<br>
<br>
</div>