<div id="main" style="font-size: 18px;">
<h3 style="text-align: center;color:red;">
  Carchecker
</h3>
<br>
<br>
<h4>
  <p>Are you a car dealer and you have free time to focus on a possible purchase item in your area or simply to top up your coffee budget?</p>
  <p>Are you not a car dealer, but you have free time, are more or less familiar with cars and do you want to supplement your pocket money?</p>
</h4>
<p>Then you've come to the right place.</p>
<p>We are looking for committed people who are ready to receive vehicle checks and carry them out conscientiously.</p>
<p>The orders come from people both in Germany and abroad who have searched the Internet for a dream car.</p>
<p>What should a vehicle checker be able to do?</p>
<p>Depending on how you are familiar with cars, we have grouped the questionnaire into two levels: <span class="hyperlink" data-to="infos-QuestionnaireExampleBasic">basic</span> and <span class="hyperlink" data-to="infos-QuestionnaireExampleAdvanced">advanced</span>.</p>
<p>What tasks does a vehicle checker have to do?</p>
<ul style2="list-style-type:none;">
  <li>Take an order</li>
  <br>
  <li>Get in touch with the vehicle seller, arrange a viewing appointment on behalf of the client.</li>
  <br>
  <li>Drive to the vehicle location near you and inspect the car.</li>
  <br>
  <li>Check the vehicle according to the questionnaire (here on the left) provided by deservice.net.</li>
  <br>
  <li>Stay in contact with the <span class="hyperlink" data-to="tutorials-orderer">orderer</span> live via chat during the vehicle inspection and answer any additional questions and / or forward the seller's answers (such as previous owner, accident damage, final price, etc.) to the client.</li>
  <br>
  <li>If the orderer wishes to purchase a car, ask for the seller's email address and forward it to the orderer so that they can sign it and exchange it with one another.</li>
</ul>
<br>
<p></p>
<p>So that there is no misunderstanding later, the vehicle checker remains anonymous for the orderer the entire time, only the checker's nickname is disclosed.</p>
<p>The whole process works something like this:</p>
<p>
After the vehicle seeker has selected the vehicle of their choice on an Internet platform, copies its link and places the order via deservice.net, stating the postcode of the vehicle location. The vehicle checkers who are registered with DeService leave the postcode area that they can use.
</p>
<ul style="list-style-type: decimal; list-style-position: inside">
  <li>The checkers are informed who can serve this postcode.</li>
  <li>The checkers, who are ready to accept this order at that moment, contact the client.</li>
  <li>The orderer decides on the basis of the following data about the checker: rating, price, execution level (<span class="hyperlink" data-to="infos-QuestionnaireExampleBasic">basic,</span> <span class="hyperlink" data-to="infos-QuestionnaireExampleBasic">advanced</span>) and urgency, which checker to select for the order and gives the order and the link to the vehicle.</li>
  <li>The orderer transfers the price set by the checker to the deservice.net account.</li>
  <li>The checker is informed by the deservice.net.netvice that the payment has been made and it can start.</li>
  <li>A web chat is activated between the client and the checker, through which they can constantly contact each other.</li>
  <li>The checker contacts the vehicle seller and drives to the car.</li>
  <li>If, for whatever reason, after contacting us, driving to the car does not make sense (e.g. the car has already been sold or the vehicle buyer could not be reached), the orderer will be informed and the order will be canceled with the agreement of the client (cancellation costs may not exceed 20% of the service price).</li>
  <li>Once there, he fills out the vehicle check questionnaire, communicates with the client via the messenger app, and sends additional photos or videos to the client on request. The checker remains anonymous to the client at all times, he only knows his nickname. If the client comes from abroad, all chat communication is translated live into the respective language.</li>
  <li>The questionnaire consists of required (marked with the snowflake symbol in the questionnaire) and voluntary fields. If all required fields are filled in, the order is considered fulfilled.</li>
  <li>If the client is satisfied with the vehicle, he negotiates the final price again via the checker and makes the decision to buy or not to buy the car.</li>
  <li>This has done this job for the checker and communication between them is no longer possible.</li>
  <li>In the event of a purchase request, the checker can establish email contact between the parties by asking the seller for an email address and forwarding it to the client. He sends the vehicle seller the completed and signed sales contract or preliminary contract and receives it back signed by him.</li>
  <li>The client rates this checker on deservice.net with a 5 star system and additional text.</li>
 </ul>
 <br>
</div>