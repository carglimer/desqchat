<div id="main" style="font-size: 14px;">
<h3 style="text-align: center;color:red;">
  Invoices
</h3>
<br>
<p>
  First of all, we clarify what a resell questionnaire is:
</p>
<p>
  This is the case when a customer wants to have a vehicle checked, but it turns out that this vehicle has already been checked as part of another order and the questionnaire has already been uploaded to the server.
</p>
<p>
  Receiving invoices:
</p>
<p>
  1. A customer (orderer) from the checker after the checker has executed the order or the customer canceled the order with compensation.
</p>
<p>
  2. A checker from the website after the checker has executed the order or the customer canceled the order with compensation.
</p>
<p>
  3. A customer from the website after purchasing a resell questionnaire.
</p>
<p>
  4. The website of the checker that carried out an order for the resell questionnaire, which a customer later bought.
</p>
<p>
  We explain the billing for case 1:
</p>
<p>
  In this case, the checker has carried out the order or the customer has canceled the order with compensation.
</p>
<p>
  The customer receives an invoice from the checker.
</p>
<p>
  To do this, the checker must first issue the invoice. Only after he has done that will he receive the payment from the website.
</p>
<p>
  For the checker, the entry in the order list looks like this:
</p>
<img class = "invoice">
<br/>
<br/>
<p>
  13.97 in this case is the compensation from the customer for a canceled order.
</p>
<p>
 By clicking on the button with the number 13.97, an additional window opens:
</p>
<img class = "invoice">
<br/>
<br/>
<img class = "invoice">
<br/>
<br/>
<p>
  This is just the form. The checker can edit the following fields: Name or company name, street and house number, if applicable the VAT rate. Or even make no changes at all.
</p>
<p>
  By clicking on the checkbox for declaration of consent, the button for invoicing is activated:
</p>
<img class = "invoice">
<br/>
<br/>
<p>
  And when you click on this button, you will be reminded that the invoice will be issued and sent to the customer.
</p>
<p>
  Now by clicking on the button with the number 13.97
</p>
<img class = "invoice">
<br/>
<br/>
<p>
  A window opens with invoices that have already been generated:
</p>
<img class = "invoice">
<br/>
<br/>
<p>
  The first invoice is from the checker for the customer and the second invoice from the website for the checker.
</p>
<p>
  These invoices can be downloaded by clicking the cloud buttons.
</p>
<img class = "invoice">
<br/>
<br/>
<p>
  Here is the invoice from the checker for the customer:
</p>
<img class = "invoice">
<br/>
<br/>
<p>
  And the other invoice from the website for the checker:
</p>
<img class = "invoice">
<br/>
<br/>
</div>