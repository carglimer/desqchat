<div id="main" style="font-size: 14px;">
<h3 style="text-align: center;color:red;">
  Work area of the orderer (client)
</h3>
<br>
<p>
  By clicking on the 'orderer' bar:
</p>
<img class="orderer">
<br/>
<br/>
<p>
 you get to the work area where the orders are created and managed:
</p>
<img class="orderer">
<br/>
<br/>
<p>
  One thing in advance: in general, if the questionnaire or chat icon is displayed with a light background as here for the first entry, it means that this order has already been closed. With a light background like the second entry, that it is live and has not yet been closed.
</p>
<p>
  First we create an order.
</p>
<p>
  To do this, we click on the '+ Add' button:
</p>
<img class="orderer">
<br/>
<br/>
<p>
  Now a window opens where you can enter all the data you need to create an order:
</p>
<img class="orderer">
<br/>
<br/>
<p>
  You have previously selected a car on the Internet that you would like to have checked at short notice.
</p>
<p>
  We fill out the fields step by step.
</p>
<p>
  First comes the short description. It is helpful for orientation so that you immediately know what this assignment is about.
</p>
<p>
  <span style = "color: red;">Brief description</span> (optional): this description must be kept as short as possible. For example, let's write 'golden BMW'.
</p>
<p>
  <span style = "color: red;">ZIP</span> (mandatory): 74348.
</p>
<p>
  <span style = "color: red;">City</span>: the field for city is filled in automatically after entering the zip code.
</p>
<p>
  <span style = "color: red;">FIN</span> (optional): this field is important because it can happen that someone else has already commissioned this car for testing and the test questionnaire with all its data already has is kept in our database. Based on the last 6 letters of the chassis number, our database can determine whether this is the case or not.
</p>
<p>
  <span style = "color: red;">Urgency</span> (mandatory):
</p>
<p>
  Depending on how quickly the job should be completed, you can choose between 3 different options:
</p>
<p>
  Ordinary - within 24 hours
</p>
<p>
  Quickly - within 4 hours
</p>
<p>
  Urgent - within 1 hour
</p>
<p>
  <span style = "color: red;">Level</span> (mandatory):
</p>
<p>
  You can choose between three levels:
</p>
<p>
  <span style = "color: green;">Telephone</span>: the vehicle seller's data are queried on the phone using a <span class="hyperlink" data-to="infos-QuestionnaireExamplePhone">telephone questionnaire</span>.
</p>
<p>
  In both other cases, the checker contacts the vehicle seller, arranges an inspection appointment and carries out the test on site:
</p>
<p>
  <span style = "color: green;">Basic</span>: according to <span class="hyperlink" data-to="infos-QuestionnaireExampleBasic">basic questionnaire</span>
</p>
<p>
  <span style = "color: green;">advanced</span>: after an <span class="hyperlink" data-to="infos-QuestionnaireExampleAdvanced">advanced questionnaire</span>
</p>
<p>
  <span style = "color: red;"> Vehicle link </span> (optional but highly desirable): the vehicle link helps the checker to get more detailed information about the vehicle. The customer can also provide the checker with the contact details via the chat service immediately after the order has been created.
</p>
<p>
  <span style = "color: red;"> Free text </span> (voluntary): Here the customer can write a detailed text relating to the vehicle: e.g. BMW 525i 2 hand, 100 tkm.
</p>
<img class="orderer">
<br/>
<br/>
<p>
  Clicking on the 'Close' button would close this window again after a warning message and thus terminate the creation of the order.
</p>
<p>
  Clicking on the 'Save' button also closes this creation window, but with the difference that a new entry with a human body icon appears in the list for orders:
</p>
<img class="orderer">
<br/>
<br/>
<p>
  Should this button look like this:
</p>
<img class="orderer">
<br/>
<br/>
<p>
  It tells us that no checkers have registered for vehicle testing for this zip code.
</p>
<p>
  This button:
</p>
<img class="orderer">
<br/>
<br/>
<p>
  tells us that one or more checkers have registered for this zip code for vehicle testing.
</p>
<p>
  A click on this icon opens the list of all checkers who have agreed to test vehicles in this postcode:
</p>
<img class="orderer">
<br/>
<br/>
<p>
  The click on the arrow button at the top left:
</p>
<img class="orderer">
<br/>
<br/>
<p>
  brings us back to the previous window.
</p>
<p>
  In the list you can see that two checkers have registered for this postcode.
</p>
<p>
  This new entry shows the prices and ratings.
</p>
<p>
  A click on the checker's nickname (e.g. checker28) opens the rating window of the respective checker, where you can see the previous ratings.
</p>
<img class="orderer">
<br/>
<br/>
<p>
  You can choose the checkers to whom the exam offer should be sent.
</p>
<p>
  The slider does a very important job here:
</p>
<p>
  <span style = "color: red;"> If the black circle (called the thumb) is on the left, it's your turn, you can now do something. Thumb is right - means you are waiting for the checker's answer. </span>
</p>
<img class="orderer">
<br/>
<br/>
<p>
  The click on the right side of the slider:
</p>
<img class="orderer">
<br/>
<br/>
<p>
  first opens an explanation window:
</p>
<img class="orderer">
<br/>
<br/>
<p>
  And if you click there again on the right side of the slider:
</p>
<img class="orderer">
<br/>
<br/>
<p>
  If this slider turns yellow and the thumb moves to the right.
</p>
<img class="orderer">
<br/>
<br/>
<p>
  If the checker answered, the slider looks like this:
</p>
<img class="orderer">
<br/>
<br/>
<p>
  It's green and clicked back on the left with your thumb.
</p>
<p>
  If you now click your thumb on the right again:
</p>
<img class="orderer">
<br/>
<br/>
<p>
  An explanation window appears first:
</p>
<img class="orderer">
<br/>
<br/>
<p>
  In order to finally place the order, you must also click on the right here:
</p>
<img class="orderer">
<br/>
<br/>
<p>
 Finally, an order window appears where you can view all order data again:
</p>
<img class="orderer">
<br/>
<br/>
<p>
  If you do not have sufficient credit for this order, you will be redirected to the corresponding PayPal window, where you can transfer the necessary amount to your credit account at deservice.net. At the same time you agree to pay the remuneration for the checker from this credit.
</p>
<p>
  Once you have done everything, a new entry will appear in the list:
</p>
<img class="orderer">
<br/>
<br/>
<p>
  The left button for chat takes you to the <span class="hyperlink" data-to="tutorials-chat">chat area</span> with the checker:
</p>
<img class="orderer">
<br/>
<br/>
<p>
  On the next button you can see the remaining hours until the end of the job:
</p>
<img class="orderer">
<br/>
<br/>
<p>
  A click on this button opens the following window:
</p>
<img class="orderer">
<br/>
<br/>
<p>
  It can happen that after the order has been placed, it can no longer be carried out for various reasons.
</p>
<p>
  Here are some examples:
</p>
<ol>
  <li>The vehicle buyer does not want to agree to a viewing appointment during the period of the order.</li>
  <li>Otherwise the checker was somehow prevented from getting to the vehicle location.</li>
  <li>The client changed his mind at the last moment and would like to cancel the order, etc.</li>
</ol>
<p>
  In such cases, the client may, at its own discretion, cancel this order with or without compensation. The amount of the compensation can be determined here with the help of the slider in% rates of the order price. The maximum compensation has been capped at 25% of the order price, in this case 25% of 52 euros = 13 euros.
</p>




</div>