<div id="main" style="font-size: 14px;">
<h3 style="text-align: center;color:red;">
  Work area of the checker (vehicle inspector)
</h3>
<br>
<p>
  By clicking on the 'Checker' bar:
</p>
<img class="tester">
<br/>
<br/>
<p>
 you get to the work area where the orders are managed:
</p>
<img class="tester">
<br/>
<br/>
<p>
 One thing in advance: in general, if the questionnaire or chat icon is displayed with a dark background, as here, it means that this order is live and has not yet been closed.
</p>
<p>
 Again with a light background:
</p>
<img class="tester">
<br/>
<br/>
<p>
  means that this order has already been closed.
</p>
<p>
  How to take the orders:
</p>
<p>
  If the orderer has placed an order offer for you, you will receive such a new entry in your list of orders:
</p>
<img class="tester">
<br/>
<br/>
<p>
  The slider does a very important job here:
</p>
<p>
  <span style = "color: red;"> If the yellow circle (called the thumb) is on the left, it's your turn, you can do something. Thumb on the right means that the orderer has to answer on the other side. </span>
</p>
<img class="tester">
<br/>
<br/>
<p>
  The click on the right side of the slider:
</p>
<img class="tester">
<br/>
<br/>
<p>
  first opens the explanation window:
</p>
<img class="tester">
<br/>
<br/>
<p>
  And if you click there again on the right-hand side of the slider:
</p>
<img class="tester">
<br/>
<br/>
<p>
  Agree to do this job and the thumb will slide to the right:
</p>
<img class="tester">
<br/>
<br/>
<p>
  Important: until the client has selected you as the checker after this step, you can withdraw your consent by clicking on the left side of this slider.
</p>
<img class="tester">
<br/>
<br/>
<p>
  In this case, an explanation window will open again, where you can confirm this cancellation click.
</p>
<img class="tester">
<br/>
<br/>
<p>
  It could be that the customer has sent the test offers to several checkers and if he chooses you as the contractor, then you have a new order in the order list:
</p>
<img class="tester">
<br/>
<br/>
<p>
  The left button for questionnaire leads you to the <span class="hyperlink" data-to="infos-QuestionnaireExampleAdvanced">questionnaire</span> for the vehicle inspection:
</p>
<img class="tester">
<br/>
<br/>
<p>
  The next button for chat takes you to the <span class="hyperlink" data-to="tutorials-chat">chat area</span> with the customer:
</p>
<img class="tester">
<br/>
<br/>
<p>
  On the next button you can read the remaining hours until the end of the job:
</p>
<img class="tester">
<br/>
<br/>
<p>
  Clicking on this button opens the following window:
</p>
<img class="tester">
<br/>
<br/>
<p>
  With the help of this window you can ask the client for an extension of the deadline, which of course you should have discussed beforehand with the customer in the chat. The deadline can only be extended with his consent.
</p>
<p>
  After the job has been closed, an archive icon appears in place of the chat icon:
</p>
<img class="tester">
<br/>
<br/>
<p>
  Clicking on it archives this job, i.e. moves the folder to 'Archive':
</p>
<img class="tester">
<br/>
<br/>
<p>
  Click there on the Entarchive icon:
</p>
<img class="tester">
<br/>
<br/>
<p>
  brings this entry back into the job list.
</p>
</div>