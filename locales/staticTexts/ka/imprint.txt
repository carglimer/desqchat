<div id="main">
<h2 >
  იმპრინტი
</h2>
<br>
Glimer Consulting GmbH
<br>
Kampstr. 18, 29223 Celle
<br>
<br>
წარმოდგენილი მისი დირექტორის მიერ:
<br>
<span id="imprintCeoPerson"></span>
<br>
<br>
გთხოვთ გაითვალისწინოთ, რომ თქვენი შეკითხვებისა და წინადადებების უკეთესი კლასიფიკაციისა და დოკუმენტაციისთვის, პასუხებს მხოლოდ WhatsApp-chat- ით ან ელექტრონული ფოსტის საშუალებით მიიღებთ.
<br>
<br>
მაილი: <span id="imprintEmail" ></span>
<br>
ვებგვერდი: www.deservice.net
<br>
Whatsapp: <span id="imprintTel"></span>
<br>
<br>
პასუხისმგებელი სარედაქციო შინაარსზე და მონაცემთა დაცვაზე:
<br>
<span id="imprintResponsiblePerson"></span>
<br>
<br>
საგადასახადო კოდი: <span id="imprintTax"></span>
<br>
დღგ-ს გადამხდელის კოდი: <span id="imprintVat"></span>
<br>
<br>
სამომხმარებლო დავების გადაწყვეტა:
<br>
ევროკავშირის კომისიამ შექმნა ინტერნეტ პლატფორმა ონლაინ დავების გადაწყვეტის შესახებ, რომელიც ეხება ონლაინ კონტრაქტებთან დაკავშირებულ სახელშეკრულებო ვალდებულებებს (OS პლატფორმა).
<br>
შეგიძლიათ დაუკავშირდეთ OS პლატფორმას <a id="imprintEuDisputs" target="_blank" href="">ამ ბმულზე</a>.
<br>
</div>