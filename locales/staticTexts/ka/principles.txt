<div id="main">
<h3 >
  deservice.net მომხმარებლებს შორის კომუნიკაციის პრინციპები
</h3>
<br>
<h4 >
  Chat-კომუნიკაცია
</h4>
საიტის იუზერებს შეუძლიათ გაუგზავნონ ერთმანეთს შეტყობინებები  deservice.net– ის საშუალებით. ჩვენ მხარს ვუჭერთ ჩვენს მომხმარებლებს შორის ღია კომუნიკაციას. ამასთან, ჩვენ არ დავუშვებთ,რომ ჩვენმა მომხმარებლებმა გამოიყენონ ეს საშუალებები სპამის გაგზავნის მიზნით, deservice.net– ის გარეთ ავტომობილების ყიდვისა და გაყიდვისთვის შეთავაზებებისათვის, მუქარის, შეურაცხყოფისა ან სიძულვილის ენისათვის. წევრებს ასევე არ აქვთ უფლება გაგზავნონ ელექტრონული ფოსტის მისამართები, ტელეფონის ნომრები ან სხვა საკონტაქტო ინფორმაცია, ფიზიკური მისამართები, ვებ – მისამართები ან ბმულები შეტყობინებების სისტემაში.
<br>
გთხოვთ გაითვალისწინოთ, რომ ჩვენ არ შეგვიძლია რაიმე კონტროლი განვახორციელოთ კომუნიკაციაზე, თუ ეს ხდება deservice.net– ის გარეთ. ზოგადი ინფორმაცია პირადი მონაცემების ბოროტად გამოყენების შესახებ, განსაკუთრებით ავტომობილების გაყიდვის სფეროში, შეგიძლიათ იხილოთ ინტერნეტში უსაფრთხო მანქანის შეძენის ინიციატივის <a id="safetyCarPurchase" target="_blank"> ვებგვერდზე</a>.
<br>
<br>
<h4 >
  მუქარა
</h4>
ჩვენ არ შევეგუებით ფიზიკური ძალადობის მუქარას. არ აქვს მნიშვნელობა რომელი მეთოდია გამოყენებული - ჩატის, ელექტრონული ფოსტის თუ ჩვენი საჯარო ფორუმების საშუალებით.
<br>
გთხოვთ, ამის შესახებ შეატყობინოთ შესაბამის სამართალდამცავ ორგანოებს. თქვენ შეგიძლიათ დაუკავშირდეთ თქვენს სატელეფონო კომპანიასთან, თუ თქვენ დაგემუქრნენ ტელეფონით ან სხვა წევრის ინტერნეტ მომსახურების პროვაიდერს, თუ საფრთხე მიიღეთ ელ.წერილით.
<br>
<br>
<h4 >
  შეურაცხმყოფელი, ვულგარული ან სიძულვილის ენა
</h4>
მომხმარებლებს ეკრძალებათ სხვა წევრებთან კომუნიკაციისას გამოიყენონ შეურაცხმყოფელი, ვულგარული ან სიძულვილის ენა.  
<br>
<br>
<h4 >
  შეფასებების კომენტარების წაშლა შეუსაბამო შინაარსის შემთხვევაში
</h4>
შემდეგი შეფასების კომენტარები შესაძლოა წაშლილ იქნას:
  <ol style="list-style-type:1;">
  <li>შეფასებები, რომლებიც შეიცავს ისეთ ინფორმაციას, რომლის საშუალებითაც შესაძლებელია სხვა მომხმარებლების იდენტიფიცირება, როგორც მაგალითად: მისამართი, ტელეფონი, ელექტრონული ფოსტის მისამართი ან მომხმარებლის ნამდვილი სახელი. გარდა ამისა, ჩვენ ასევე შეგვიძლია ამოვიღოთ შეფასებებები, რომელთა საშუალებითაც შესაძლებელია სხვა მომხმარებლების პირად მონაცემებზე წვდომა.</li>
  <li>შეფასებები, რომლებიც არ უკავშირდება deservice.net-ს. ეს ასევე ეხება კომენტარებს, პასუხებს ან დამატებით კომენტარებს, რომლებიც სხვა შეკვეთას უკავშირდება</li>
  <li>შეფასებები, რომლებიც შეიცავს კომენტარებს პოლიტიკურ, რელიგიურ, სოციალურ ან სხვა თემებზე და არ ეხება deservice.net-ს</li>
  <li>უარყოფითი შეფასებების კომენტარები, რომლებიც პირდაპირ ეწინააღმდეგება მიმოხილვის პოზიტიურ შეფასებას, რომლებიც შეიცავს დამამცირებელ, ვულგარულ, უცენზურო ან რასისტულ ენას, ასევე კომენტარები, რომლებიც იყენებენ მოზრდილების ენას ან იმუქრებიან ფიზიკური ძალადობით</li>
  <li>უარყოფითი შეფასებების კომენტარები, რომლებიც პირდაპირ ეწინააღმდეგება შეფასების პოზიტიურ შეფასებას</li>
  <li>შეფასებები, რომლებიც შეიცავს ბმულებს ან სარეკლამო შეტყობინებებს, რომლებიც მომხმარებლებს ხელს უწყობენ შეიძინონ საგნები deservice.net–ის გარეთ</li>
  <li>მიმოხილვები, რომლებიც შეიცავს მითითებებს სისხლის სამართლის გამოძიებაზე ან გამოძიებაზე deservice.net ან PayPal-ის მიერ</li>
</ol>
<br>
<br>
Email:  <span id="principlesEmail"></span>
<br>
<br>
<div>