<div id="main" style="font-size: 14px;">
<h3 style="text-align: center;color:red;">
  ჩექერის სამუშაო სივრცე
</h3>
<br>
<p>
  ჩექერის ველზე დაწკაპუნებით:
</p>
<img class="tester">
<br/>
<br/>
<p>
 მოხვდებით იმ სამუშაო სივრცეში, სადაც შეგიძლიათ მართოთ თქვენი შეკვეთები:
</p>
<img class="tester">
<br/>
<br/>
<p>
 ერთი რამ წინასწარ უნდა ითქვას: ზოგადად, თუ კითხვარის ან ჩატის აიკონი ნაჩვენებია მუქი ფონით, როგორც აქ არის, ეს ნიშნავს, რომ ეს შეკვეთა ღიაა და ჯერ არ არის დახურული.
</p>
<p>
 ღია ფონის შემთხვევაში კი:
</p>
<img class="tester">
<br/>
<br/>
<p>
  ნიშნავს, რომ შეკვეთა დახურულია.
</p>
<p>
  როგორ ვიღებთ შეკვეთებს:
</p>
<p>
  თუ დამკვეთმა მანქანის შემოწმების შემოთავაზება გამოგიგზავნათ, მაშინ თქვენი დაკვეთების სიაში გაჩნდება ახალი ჩანაწერი:
</p>
<img class="tester">
<br/>
<br/>
<p>
  აქ არსებულ სლაიდერს ძალიან მნიშვნელოვანი ფუნქცია აკისრია:
</p>
<p>
  <span style="color:red;">თუ სლაიდერის ყვითელი წრე(მას ცერს ეძახიან) მარცხნივ არის, მაშინ თქვენი ჯერია, თქვენგან მოქმედების განხორციელება არის საჭირო. თუ ცერი მარჯვნივ არის გადაწეული, მაშინ დამკვეთის პასუხს ელოდებით.</span>
</p>
<img class="tester">
<br/>
<br/>
<p>
  სლაიდერზე მარჯვნივ დაწკაპუნებით:
</p>
<img class="tester">
<br/>
<br/>
<p>
  გაიხსნება განმარტების ფანჯარა:
</p>
<img class="tester">
<br/>
<br/>
<p>
  და თუ თქვენ იქაც სლაიდერის მარჯვნივ დაკლიკავთ:
</p>
<img class="tester">
<br/>
<br/>
<p>
  ამით აცხადებთ მზადყოფნას, შეასრულოთ ეს დაკვეთა და ცერი გადაინაცვლებს მარჯვნივ:
</p>
<img class="tester">
<br/>
<br/>
<p>
  ყურადღება: სანამ დამკვეთმა თქვენ ჯერ კიდევ არ აგირჩიათ ჩექერად, შეგიძლიათ თქვენი თანხმობა გააუქმოთ სლაიდერის მარცხენა ნაწილზე დაწკაპუნებით.
</p>
<img class="tester">
<br/>
<br/>
<p>
  ამ შემთხვევაში გაიხსნება გამაფრთხილებელი ფანჯარა, სადაც თქვენ შეგიძლიათ დაადასტუროთ თანხმობის გაუქმება.
</p>
<img class="tester">
<br/>
<br/>
<p>
  შესაძლოა ისე მოხდეს, რომ დამკვეთმა შემოწმების შეთავაზებები ერთდროულად რამდენიმე ჩექერს გაუგზავნა და თუ ის თქვენ აგირჩევთ კონტრაქტორად, მაშინ თქვენი შეკვეთების სიაში გაჩნდება ახალი ჩანაწერი:
</p>
<img class="tester">
<br/>
<br/>
<p>
  ამ ახალ ჩანაწერში მარცხენა ღილაკი გადაგიყვანთ <span class="hyperlink" data-to="infos-questionnaireExampleAdvanced">მანქანის შემოწმების კითხვარის</span> ფანჯარაში:
</p>
<img class="tester">
<br/>
<br/>
<p>
  შემდეგი ღილაკი გადაგიყვანთ დამკვეთთან <span class="hyperlink" data-to="tutorials-chat">ჩეთის</span> ფანჯარაში:
</p>
<img class="tester">
<br/>
<br/>
<p>
  მომდევნო ღილაკზე ამოიკითხავთ დაკვეთის დასრულებამდე დარჩენილ საათებს:
</p>
<img class="tester">
<br/>
<br/>
<p>
  ამ ღილაკზე დაწკაპუნებით გაიხსნება ასეთი ფანჯარა:
</p>
<img class="tester">
<br/>
<br/>
<p>
  საჭიროების შემთხვევაში თქვენ შეგიძლიათ ამ ფანჯრის მეშვეობით დამკვეთს თხოვოთ ვადის გაგრძელება, რაზეც ბუნებრივია მანამდე ჩეთში უნდა შეთანხმდეთ. დამკვეთის თანხმობის შემთხვევაში ვადა გაგრძელდება.
</p>
<p>
  დაკვეთის დახურვის შემდეგ ჩეთის აიკონის ნაცვლად გაჩნდება არქივირების აიკონი:
</p>
<img class="tester">
<br/>
<br/>
<p>
  მასზე დაწკაპუნებით დაარქივდება აეს დაკვეთა, რაც ნიშნავს, რომ იგი გადაინაცვლებს 'არქივი'-ს საქაღალდეში:
</p>
<img class="tester">
<br/>
<br/>
<p>
  განარქივების ღილაკზე დაწკაპუნებით:
</p>
<img class="tester">
<br/>
<br/>
<p>
  ეს ჩანაწერი ისევ დაკვეთების სიაში დაბრუნდება.
</p>
</div>




</div>