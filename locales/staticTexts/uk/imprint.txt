<div id="main">
<h2>
  Вихідні дані
</h2>
<br>
Glimer Consulting GmbH
<br>
Kampstr. 18, 29223 Celle
<br>
<br>
в особі свого керуючого директора:
<br>
<span id="imprintCeoPerson"></span>
<br>
<br>
Просимо вашого розуміння в тому, що для кращої класифікації та документування ваших запитів, ми вирішили відповісти на них тільки через WhatsApp-chat або електронної пошти.
<br>
<br>
E-Mail: <span id="imprintEmail" ></span>
<br>
Веб-сайт: www.deservice.net
<br>
Whatsapp: <span id="imprintTel"></span>
<br>
<br>
Відповідальний за редакційний контент та захист даних:
<br>
<span id="imprintResponsiblePerson"></span>
<br>
<br>
Hалоговий номер: <span id="imprintTax"></span>
<br>
код ндс: <span id="imprintVat"></span>
<br>
<br>
Вирішення споживчих суперечок:
<br>
Комісія ЄС створила Інтернет-платформу для онлайн-урегулювання суперечок, пов'язаних із договірними зобов'язаннями щодо онлайн-контрактів (платформа OS).
<br>
Ви можете перейти на платформу <a id="imprintEuDisputs" target="_blank" href="">за наступним посиланням</a>.
<br>
</div>