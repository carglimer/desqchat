// import { chatColors } from '../helpers/help'
// import { getSystemDateText, isArray } from '../helpers/methods'
// import { uploadMedia } from '../helpers/axiosMethods'
import { initSocket } from '../helpers/initSocket'
// import { chatEBus } from '../helpers/datas'

const ss = require('socket.io-stream')

export const chatCore = {
  created() {
    // es sakontroloa aq tu gvchirdeba
    this.$g.store.commit('clearMessageList')
    addEventListener('beforeunload', this.$g.removeSocket())
    this.$g.chatEBus.$off(this.$enums.events.initChat)
    this.$g.chatEBus.$on(this.$enums.events.initChat, () => {
      this.initChat()
    })
    /* this.$g.chatEBus.$off(this.$enums.events.handleReceivedPendingMessage)
    this.$g.chatEBus.$on(this.$enums.events.handleReceivedPendingMessage, ({ message, meta }) => {
      this.handleReceivedMessagemeta({ message, meta })
    }) */
    this.$g.chatEBus.$off(this.$enums.events.loadReceiverAllMessages)
    this.$g.chatEBus.$on(this.$enums.events.loadReceiverAllMessages, (onlyPendingMessages) => {
      this.$nextTick(() => {
        this.loadReceiverAllMessages(onlyPendingMessages)
      })
    })
    // me mgoni aq initChat aghar aris sachiro
    // this.initChat()
  },
  mounted() {
    // camovighot murmanis bazidan user and card
    // this.fetchUser(this.userId, this.cardId) <- interfaceMixin-shia
  },
  beforeDestroy() {
    // console.log('cc beforeDestroy')
    this.$g.removeSocket()
  },
  data() {
    return {
      receiver: null
    }
  },
  watch: {
    '$g.store.getters.messageList'(messageList, oldVal) {
      this.$g.clog('cc messageList changed ')
      this.$nextTick(this._scrollDown)
    },
    '$g.store.getters.typingState'(state, oldVal) {
      this.$g.clog('cc typingState ', state)
      this.sendTypingMessage(state)
    },
    '$g.store.getters.locale'(locale, oldVal) {
      this.$g.clog('cc locale ', locale)
      if (locale) {
        this.sendTypingMessage(this.$enums.typingState.offFocus)
      }
    },
    '$g.store.getters.card'(card, oldValue) {
      this.$g.clog('cC card = ', card)
      if (card) {
        this.$g.clog('cC this.$g.socket = ', this.$g.socket)
        this.initChat()
      } else {
        // mashin aqedan gavdivart da socket-s vtishavt tu pendingMessageLists carielia
        // this.$g.removeSocket()
        const pendingMessageLists = this.$g.store.getters.pendingMessageLists
        let n = 0
        Object.keys(pendingMessageLists).forEach((cardId) => {
          const pendingMessageList = pendingMessageLists[cardId]
          if (pendingMessageList && pendingMessageList.length) {
            n++
          }
        })
        if (n === 0) {
          this.$g.removeSocket()
        }
      }
    }
  },
  methods: {
    initChat() {
      this.receiver = this.$g.getReceiver()
      this.$g.clog('cc initChat this.receiver = ', this.receiver)
      // this.$g.store.commit('clearMessageList')
      if (this.receiver) {
        this.isp(this.loadReceiverAllMessages)
        this.$g.getMedias()
      }
    },
    // aq sheizleba params mag. ase ikos -> { message, blob } an ubralod message
    isp(callback, params) {
      initSocket(this.initSocketPayload({ callback, params }))
    },
    // უინტერნეტობის დროს გაუგზავნელი message-ების დამუშავება
    setPendingMessage(message) {
      this.$g.clog('cc setPendingMessage message = ', message)
      if (message) {
        const card = this.$g.store.getters.card
        if (!card) {
          return
        }
        const pendingMessageList = this.$g.store.getters.pendingMessageLists[card.id]
        // if pendingMessageList not contains message, mashin chavcerot
        if (!pendingMessageList || pendingMessageList.findIndex((o) => o.ts === message.ts) === -1) {
          // blob-s ar vinaxavt
          if (message.data && message.data.blob) {
            delete message.data.blob
          }
          this.$g.store.commit('addToPendingMessageList', message)
          if (message.meta.ext) {
            // tu file-ia xelaxla chamovtvirtot UserIputTop-shi idbMedias da iq es FileMessage agar iqneba
            this.$g.getMedias()
          }
          // es serveridan ar chamotvirtavs, mxolod pendingMessage-ebs daamushavebs
          this.loadReceiverAllMessages(true)
        }
      }
    },
    // gavagzavnet uinternetobis dros gaugzavneli dagrovili message-ebi
    sendPendingMessages() {
      this.$g.clog('cc sendPendingMessages')
      // vagzavnit dakovnebit,radgan tu socketi avtomaturad gaagzvanis, meored ar cavides
      const pendingMessageList = this.$g.store.getters.pendingMessageLists[this.$g.store.getters.card.id] || []
      for (const message of pendingMessageList) {
        // aq ukve vicit, rom socket sheertebulia
        if (!message.meta.ext) {
          this.emitMessageEvent(message)
        } else {
          // jer camovighot blob idb-dan  id: message.idbId
          this.$g
            .readMediaById({ id: message.ts })
            .then((idbMedia) => {
              this.$g.clog('cc message = ', message)
              this.$g.clog('cc idbMedia = ', idbMedia)
              if (idbMedia) {
                // this.sendFileStream(message, idbMedia.media.blob)
                message.data = { blob: idbMedia.media.blob }
                this.sendFiles([message])
              }
            })
            .catch((err) => {
              this.$g.clog('cc sendPendingMessages err ', err)
            })
        }
      }
    },
    initSocketPayload(payload) {
      // this.getFileStream(this.$g.socket, this.downloadErrorHandler, this.downloadSuccessHandler)
      this.$g.clog('cc payload = ', payload)
      const pl = {
        connectionErrorCallback: this.loadReceiverAllMessages, // ->onlyPendingMessages = true
        sendPendingMessages: this.sendPendingMessages,
        g: this.$g,
        /* socketIoStream: {
          getFileStream: this.getFileStream,
          errorHandler: this.downloadErrorHandler,
          successHandler: this.downloadSuccessHandler
        }, */
        initSocketListeners: this.initSocketListeners
      }
      if (payload) {
        Object.keys(payload).forEach((key) => {
          if (payload[key]) {
            pl[key] = payload[key]
          }
        })
      }
      return pl
    },
    checkReceiver() {
      return !!this.receiver
    },
    loadReceiverAllMessages(onlyPendingMessages) {
      this.$g.clog('cc loadReceiverAllMessages onlyPendingMessages = ', onlyPendingMessages)
      const card = this.$g.store.getters.card
      if (!this.checkReceiver() || !card) {
        return
      }
      // this.$g.store.commit('clearMessageList')
      const message = {
        meta: { type: this.$enums.mt.TEXT, caller: this.$enums.caller.chat, collName: this.$enums.collnames.messages },
        data: {}
      }
      this.$g.adjustMessage(message)
      this.$g.clog('cc loadReceiverAllMessages message = ', message)
      // jer cavikitxot pendingMessagesList
      const pendingMessageList = this.$g.store.getters.pendingMessageLists[card.id] || []
      this.$g.clog('cc pendingMessageList ', pendingMessageList)
      if (onlyPendingMessages) {
        if (pendingMessageList.length) {
          const _messageList = [...this.sortGroups(pendingMessageList)]
          this.handleActualMessageList(_messageList)
        }
        return
      }
      this.$g.store.commit('clearMessageList')
      this.$g.clog('cc downloadUserAllMessages ')
      this.$g.socket.emit(this.$enums.events.downloadUserAllMessages, message, (messageList) => {
        // dabrunda bazidan { download: true }
        this.$g.clog('cC messageList = ', messageList)
        if (!messageList) {
          messageList = []
        }
        if (messageList.length) {
          this.acknowlidgeServerAboutReadAllUserMessages(messageList)
        }
        this.resetChatAlreadySentMediaSize()
        let alreadySentMediaSize = 0
        // bazidan mosul message-ebs
        for (const message of messageList) {
          /* text message =  
          {"_id":"617c5501b6a3e37279651a61","ts":1635538176481,"cardId":121,
          "data":{"textData":{"text":"ერთი","senderLang":"ka","receiverLang":"de","translatedText":"Einer"}},
          "meta":{"type":"text","tsList":{"notSent":1635538176481,"savedInDb":1635538177663,"read":1635538177480},"caller":"chat","collName":"messages","status":"read",
          "sender":{"id":101,"cardId":121,"orderId":113,"role":"orderer","nickname":"giaOrderer62","lang":"ka"},
          "receiver":{"id":104,"cardId":121,"orderId":113,"role":"tester","nickname":"gcarcheck763","lang":"de"},
          "msgId":"617c5501b6a3e37279651a61"},"receiverId":104,"senderId":101}
          file message -> type = image || video || audio || file
          {"_id":"617c6090752bdb0007e5f4ed","ts":1635531872663,"meta":{"tsList":{"notSent":1635531872663,"savedInDb":1635541135357,"read":1635541135729},
          "group":[1635531886595,1635531879692,1635531872663],"type":"image","ext":"jpeg","size":3145135,"status":"read",
          "sender":{"id":104,"cardId":121,"orderId":113,"role":"tester","nickname":"gcarcheck763","lang":"de"},
          "receiver":{"id":101,"cardId":121,"orderId":113,"role":"orderer","nickname":"giaOrderer62","lang":"ka"},
          "caller":"chat","collName":"messages","relPath":"/chats/1635531872663_3145135.jpeg","msgId":"617c6090752bdb0007e5f4ed"},
          "cardId":121,"senderId":104,"receiverId":101}
          */
          // this.$g.clog('cc message = ', JSON.stringify(message))
          this.addPseudoIdbMedia(message)
          if (message.meta.type !== 'text' && message.meta.size) {
            alreadySentMediaSize += message.meta.size
          }
        }
        // აქ text-ის გარდა არ აქვს მნიშვნელობა type-ს და ამიტომ ვწერთ -> type: 'image'
        this.addAlreadySentMediaSize({ meta: { type: 'image', caller: 'chat', size: alreadySentMediaSize } })
        // davalago group-ebis mixedvit
        const _messageList = [...this.sortGroups(messageList), ...this.sortGroups(pendingMessageList)]
        this.handleActualMessageList(_messageList)
      })
      // }
    },
    acknowlidgeServerAboutReadAllUserMessages(messageList) {
      // mxolod im message-ebistvis, romeltac ar aqvt status.read
      const _messageList = []
      for (const message of messageList) {
        if (!message.meta.tsList[this.$enums.status.read] && !this.$g.isMy(message)) {
          message.meta.tsList[this.$enums.status.read] = Date.now()
          _messageList.push(message)
        }
      }
      if (_messageList.length) {
        this.$g.socket.emit(this.$enums.events.acknowlidgeServerAboutReadAllUserMessages, _messageList)
        this.$g.clog('cc acknowlidgeServerAboutReadAllUserMessages _messageList = ', _messageList)
      }
    },
    // es message-ebs daalagebs group-ebis mixedvit [ [mesage1], [mesasge2], [ mesage3, mesasge4], [message5],...]
    // sadac message3 da message4 erti group-idan aris
    sortGroups(messageList) {
      const groupsArr = [] // array of arrays
      // { }
      const groupsObj = {}
      // message-ebi, romlebic group-shia
      const groupMessages = []
      // message-ebi, romlebic ar aris group-shi
      const simpleMessages = []
      for (const msg of messageList) {
        if (msg.meta.group) {
          // msg.meta.group.includes(message.ts)
          if (!groupsArr.some((group) => group.includes(msg.meta.group[0]))) {
            groupsArr.push(msg.meta.group)
          }
          groupMessages.push(msg)
        } else if (msg.meta.ext) {
          // ara group-is filemessage-ebia
          simpleMessages.push([msg])
        } else {
          // textmessage-ebia
          simpleMessages.push(msg)
        }
      }
      for (const msg of groupMessages) {
        for (const group of groupsArr) {
          if (group.includes(msg.ts)) {
            if (!groupsObj[group[0]]) {
              groupsObj[group[0]] = []
            }
            groupsObj[group[0]].push(msg)
          }
        }
      }
      const sortedArr = [...simpleMessages, ...Object.values(groupsObj)]
      this.$g.clog('cc sortedArr = ', sortedArr)
      return sortedArr
    },
    handleActualMessageList(messageList) {
      this.$g.clog('cc messageList = ', messageList)
      for (const messageOrArray of messageList) {
        if (messageOrArray && messageOrArray.meta && messageOrArray.meta.error) {
          // mashin  ragac error-ia da achvenebs snackbarText
          const errorTxt = JSON.stringify(messageOrArray.meta.error.text)
          this.$g.clog('cC meta.error = ', errorTxt)
          this.$g.showSnackbar(errorTxt, 5000, 'error')
          return
        }
      }
      let dateMillis // { meta: { tsList: { notSent: 0 } } }
      const interval = 24 * 60 * 60 * 1000 // 24 hours in milliseconds
      const obj = {}
      const locale = this.$g.store.getters.locale
      for (const messageOrArray of messageList) {
        let message
        // let groupMessages
        this.$g.clog('cC messageOrArray = ', messageOrArray)
        // to group aris, mashin message aris Array
        if (this.$g.isArray(messageOrArray)) {
          // groupMessages = messageOrArray
          message = messageOrArray[0]
        } else {
          message = messageOrArray
        }
        dateMillis = message.meta.tsList[this.$enums.status.notSent]
        // let date = new Date(dateMillis)
        const startOfDay = Math.floor(dateMillis / interval) * interval
        // const endOfDay = startOfDay + interval - 1
        const sysMillis = startOfDay
        const systemMessage = {
          ts: sysMillis,
          meta: {
            type: this.$enums.mt.SYSTEM,
            tsList: { [this.$enums.status.notSent]: sysMillis }
          },
          data: {
            date: sysMillis,
            // ase achvenebs -> 5. September 2019
            text: this.$g.getSystemDateText(locale, sysMillis)
          }
        }
        obj[dateMillis] = messageOrArray
        obj[sysMillis] = systemMessage
      }
      const fullMessageList = Object.values(obj)
      //  davalagot messagelist zrdadobis mixedvit
      // fullMessageList.sort((a, b) => (a.meta.tsList[status.notSent] > b.meta.tsList[status.notSent] ? 1 : -1))
      fullMessageList.sort((a, b) =>
        (a.meta ? a : a[0]).meta.tsList[this.$enums.status.notSent] >
        (b.meta ? b : b[0]).meta.tsList[this.$enums.status.notSent]
          ? 1
          : -1
      )
      this.$g.clog('fullMessageList = ', fullMessageList)
      for (const message of fullMessageList) {
        this.$g.store.commit('addToMessageList', message)
      }
    },
    sendTypingMessage(state) {
      // state = typing||onFocus||offFocus
      this.$g.clog('cC sendTypingMessage state ', state)
      // vagzavnit typing info-s, bazashi ar vinaxavt, radgan live-shi aqvs azri da callback-ebs ar vaketebt
      // payload -> typing, onFocus, offFocus
      const typingMessage = {
        meta: {
          type: this.$enums.mt.TYPING,
          caller: this.$enums.caller.chat,
          tokenInfo: {},
          sender: this.$g.getSender(),
          receiver: this.receiver,
          state
        }
        // meta: { sender: this.$g.store.getters.user._id, receiver: this.receiver, status: payload }
      }
      if (!typingMessage.meta.receiver) {
        this.$g.clog('error! -> receiver = null')
        return
      }
      /* &tokenoff=true -> initSocket()-shia
      if (process.env.tokenoff) {
        typingMessage.meta.tokenoff = true
      } */
      // init da callback-shi vagzavnit
      const emitTypingCb = () => {
        this.$g.socket.emit(this.$enums.events.typingToBack, typingMessage)
      }
      // initSocket(this.initSocketPayload({ callback: emitTypingCb }))
      this.isp(emitTypingCb)
      this.$g.clog('cc send typingMessage ', JSON.stringify(typingMessage))
      // this.typingMessage = typingMessage // for testing only
    },
    sendMessage(message) {
      if (!message.meta) {
        message.meta = {}
      }
      message.meta.caller = this.$enums.caller.chat
      message.meta.collName = this.$enums.collnames.messages
      this.$g.adjustMessage(message)
      this.$g.clog('cC msg ', JSON.stringify(message))
      this.setPendingMessage(message)
      this.addMessageToList(message)
      this.isp(this.emitMessageEvent, message)
    },
    emitMessageEvent(message) {
      // gavagzavne da veli migebis dadasturebas
      this.$g.socket.emit(this.$enums.events.chatToBack, message, (meta) => {
        this.handleAcknowlidgedMessagemeta({ message, meta })
      })
    },
    handleAcknowlidgedMessagemeta({ message, meta }) {
      // dabrunda bazidan meta ... status:'savedInDb'
      this.$g.clog('cC meta=', meta)
      // radgan message servers mighebuli aqvs, cavshalot pendingebidan
      this.$g.store.commit('removePendingMessageFromList', message)
      const pendingMessageList = this.$g.store.getters.pendingMessageLists[this.$g.store.getters.card.id]
      this.$g.clog('cc pendingMessageList = ', pendingMessageList)
      if (meta.error) {
        // mashin  ragac error-ia da achvenebs snackbarText
        const errorTxt = JSON.stringify(meta.error.text)
        this.$g.clog('cC meta.error = ', errorTxt)
        this.$g.showSnackbar(errorTxt, 5000, 'error')
        return
      }
      this.$g.clog('cC message ', message)
      // aq mohkveba ganaxlebuli token da davimaxsovrot
      // this.$g.store.commit('setToken', meta.token)
      delete meta.token
      this.setIdAndMeta(message, meta)
      // ukve shegvizlia vachvenot my message
      this.addMessageToList(message)
    },
    setIdAndMeta(message, meta) {
      if (!meta) {
        return
      }
      // message-s ukve _id aqvs da shemdeg notif-ebs mivabamt
      // store-is gamo pirdapir ver vanichebt -> message._id = meta.msgId
      // to update moxda mashin meta.msgId = null
      if (meta.msgId) {
        this.$g.setObjectProperty(message, '_id', meta.msgId)
        this.$g.setObjectProperty(message, 'meta', meta)
      }
      // message.meta = meta
    },
    addMessageToList(messageOrArray) {
      this.$g.clog('cc messageOrArray = ', messageOrArray)
      const messageList = this.$g.store.getters.messageList || []
      // this.$g.clog('cc addMessageToList messageList = ', messageList)
      if (!messageList.length) {
        this.$g.clog('cc addMessageToList messageList = ', messageList)
        // addToMessageList handleActualMessageList-შიც არის
        this.handleActualMessageList([messageOrArray])
      } else {
        this.$g.store.commit('addToMessageList', messageOrArray)
      }
    },
    // fielMessage -> { meta: { type: 'file', ext:'jpg' }, data: { author: 'me', text, file, handleErr, handleProgress, handleSuccess }}
    sendFiles(messages) {
      // this.addMessageToList(messages)
      // chavrtet UserInputTop-shi LoaderCircular
      const tsArray = messages.map((message) => message.ts)
      this.$g.chatEBus.$emit(this.$enums.events.chatMediaStartLoading, tsArray)
      this.$g.clog('cc this.$refs.uit.loading ', this.$refs.uit.loading)
      const uploadPromises = []
      const fileSender = this.getFileSender()
      // const callbacks = messages[0].callbacks
      // this.$g.clog('cC callbacks ', callbacks)
      for (const message of messages) {
        this.$g.adjustMessage(message)
        message.meta.caller = this.$enums.caller.chat
        message.meta.collName = this.$enums.collnames.messages
        // this.$g.adjustMessage(message)
        const blob = message.data.blob
        // aq blob caishleba
        this.setPendingMessage(message)
        this.$g.clog('cC msg ', message)
        this.$g.clog('cC blob ', blob)
        if (fileSender === this.$g.uploadMedia) {
          uploadPromises.push(this.sendFileAxios({ message, blob }))
        } else {
          // mashin socket.io-stream -it vagzavnit
          this.isp(fileSender, { message, blob })
        }
      }
      if (fileSender === this.$g.uploadMedia) {
        // vucdit kvela failis atvirtvas
        Promise.all(uploadPromises)
          .then((_messages) => {
            this.$g.clog('cC _messages = ', _messages)
            // this.addMessageToList(_messages) <- amas azri ar aqvs, radgan shemdeg mainc initChat() modis
            // radgan failebi ukve gaigzavna chamovtvirtot serveridan xelaxla
            this.initChat()
            this.handleSuccess(_messages[0])
            this.$g.chatEBus.$emit(this.$enums.events.chatMediaStopLoading)
          })
          .catch((err) => {
            this.$g.clog('cC handleErr err = ', err)
            this.handleErr(err)
          })
      }
    },
    handleSuccess(message) {
      this.$g.clog('uI uploaded message = ', message)
      // ar gvinda success-is chveneba
      // const uploadedText = this.$g.t('qchat.chat.upload.succesfully_uploaded')
      // this.$g.showSnackbar(uploadedText, 2000, 'success')
    },
    handleErr(_errorTxt) {
      // file, e
      this.$g.clog('uI failure ', _errorTxt)
      if (_errorTxt && _errorTxt.toString().includes('413')) {
        _errorTxt = this.$g.t('qchat.media.max_size_error.too_large')
      }
      const errorText = this.$g.t('qchat.chat.upload.error.text') + ': ' + _errorTxt
      this.$g.showSnackbar(errorText, 3000, 'error')
    },
    sendFileAxios({ message, blob }) {
      return new Promise((resolve, reject) => {
        this.$g
          .uploadMedia({ message, blob })
          .then((resultData) => {
            const meta = resultData.meta
            this.setIdAndMeta(message, meta)
            this.$g.clog('cC handleSuccess meta = ', meta)
            this.handleSentFileMessage(message)
            resolve(message)
          })
          .catch((err) => {
            this.$g.clog('cC message ', message)
            this.$g.clog('cC err ', err)
            this.$g.chatEBus.$emit(this.$enums.events.chatMediaStopLoading)
            reject(err)
            // callbacks.handleErr(err)
          })
      })
    },
    // chemi gagzavnili fileMessage-is idbMedia delete da add meta-s safuzvelze sheqmnili axali idbMedia
    handleSentFileMessage(message, initChat) {
      // vnaxot es gaugzavneli message-tu iko
      this.$g.store.commit('removePendingMessageFromList', message)
      this.$g
        ._removeMedia({ media: { ts: message.ts } })
        .then(() => {
          // davumatot gagzavnili failebis zomas
          // this.addAlreadySentMediaSize(message)
          // stream-is shemtxvevashi radgan ukve gaigzavna chamovtvirtot serveridan xelaxla
          if (initChat) {
            this.initChat()
          }
        })
        .catch((err) => {
          this.$g.clog('cc handleSentFileMessage err ', err)
        })
    },
    getFileSender() {
      switch (this.$g.gdata.chatMediaSendMode) {
        case this.$enums.chatMediaSendMode.socketStream:
          return this.sendFileStream
        case this.$enums.chatMediaSendMode.axios:
          return this.$g.uploadMedia
        default:
          return this.sendFileStream
      }
    },
    resetChatAlreadySentMediaSize() {
      const card = this.$g.store.getters.card
      this.$g.store.commit('setCardIndicatorParams', { card, target: 'chat', prop: 'alreadySentMediaSize', size: 0 })
    },
    addAlreadySentMediaSize(message) {
      if (message.meta.type === 'text') {
        return
      }
      const target = message.meta.caller
      let alreadySentMediaSize = 0
      const prop = 'alreadySentMediaSize'
      const card = this.$g.store.getters.card
      const indicator = card.indicator
      if (target === this.$enums.caller.chat) {
        alreadySentMediaSize = indicator.chat.alreadySentMediaSize || 0
      } else if (target === this.$enums.caller.questionnaire) {
        alreadySentMediaSize = indicator.questionnaire.alreadySentMediaSize || 0
      }
      const size = +alreadySentMediaSize + +message.meta.size
      const payload = { card, target, prop, size }
      this.$g.clog('cc addAlreadySentMediaSize payload = ', payload)
      if (target === this.$enums.caller.chat || target === this.$enums.caller.questionnaire) {
        this.$g.store.commit('setCardIndicatorParams', payload)
        // this.$g.clog('cc card.indicator.chat = ', JSON.stringify(card.indicator.chat))
      }
    },
    // es PseudoIdbMedia ar inaxeba idb-shi, ubralod idbMedia-snairi parametrebi aqvs
    addPseudoIdbMedia(message) {
      if (message.meta.ext) {
        let sent
        const isMy = this.$g.isMy(message)
        if (isMy) {
          sent = this.$enums.status.sent
        } else sent = this.$enums.status.read
        // const idbId = message.meta.relPath ? null : message.ts
        const pseudoIdbMedia = {
          cardId: message.cardId,
          sent,
          meta: message.meta,
          isMy,
          media: {
            // idbId,
            relPath: message.meta.relPath,
            // blob: message.data ? message.data.blob : null,
            ext: message.meta.ext,
            type: message.meta.type,
            size: message.meta.size,
            tsList: message.meta.tsList // [status.notSent]
          }
        }
        if (message.meta.filename) {
          pseudoIdbMedia.media.filename = message.meta.filename
        }
        this.$g.setObjectProperty(message, 'pseudoIdbMedia', pseudoIdbMedia)
      }
    },
    // data -> {name: filepath}
    sendFileStream({ message, blob }) {
      this.$g.clog('cc message = ', message)
      // to pendingMessage-ia mashin blob calke modis, radgan ise vuex atrakebs
      this.$g.clog('cC blob = ', blob)
      if (!blob) {
        return
      }
      const { callbacks, ...msg } = message
      this.$g.clog('cC callbacks ', callbacks)
      this.$g.clog('cC msg ', msg)

      const stream = ss.createStream()
      this.$g.clog('cC stream ', stream)
      ss(this.$g.socket).emit(this.$enums.events.fileStreamToBack, stream, msg, (meta) => {
        this.$g.clog('cC meta ', meta)
        if (meta.error) {
          // mashin  ragac error-ia da achvenebs snackbarText
          const errorTxt = JSON.stringify(meta.error.text)
          this.$g.clog('cC meta.error = ', errorTxt)
          // this.$g.showSnackbar(errorTxt, 7000, 'error')
          this.handleErr(errorTxt)
        } else if (meta.status === this.$enums.status.savedInDb) {
          // aq mohkveba ganaxlebuli token da davimaxsovrot
          // this.$g.store.commit('setToken', meta.token)
          this.setIdAndMeta(message, meta)
          this.$g.clog('cC handleSuccess meta = ', meta)
          this.handleSentFileMessage(message, true)
          this.handleSuccess(message)
        }
      })
      if (this.$g.socket && this.$g.socket.connected) {
        // Upload progress
        const blobStream = ss.createBlobReadStream(blob)
        let size = 0
        blobStream.on('data', (chunk) => {
          // this.$g.clog('cC file.size ', blob.size)
          // this.$g.clog('cC chunk.length ', chunk.length)
          size += chunk.length
          const progress = Math.floor((size / blob.size) * 100)
          // this.$g.clog('cC progress', progress + '%') // -> e.g. '42%'
          this.$g.chatEBus.$emit(this.$enums.events.chatMediaUploadProgress, { ts: message.ts, progress })
          // idbMedia.loadProgress = progress
          // callbacks.handleProgress(message, progress)
        })
        blobStream.on('end', () => {
          this.$g.clog('cC socket progress = 100')
          this.$g.chatEBus.$emit(this.$enums.events.chatMediaUploadProgress, { ts: message.ts, progress: 100 })
        })
        // send file-stream to server
        blobStream.pipe(stream)
        //  fs.createReadStream(filepath).pipe(stream)
      } else {
        this.$g.clog('cC socket disconnected ')
        // this.$g.showSnackbar('disconnected', 2000, 'error')
      }
    },
    initSocketListeners(socket) {
      socket.off(this.$enums.events.chatFromBack)
      this.$g.clog('cC initSocketListeners= events.chatFromBack= ' + this.$enums.events.chatFromBack)
      const that = this
      socket.on(this.$enums.events.chatFromBack, (messages, callback) => {
        this.$g.clog('cC messages=', messages)
        if (callback) {
          // aq message-s an dagrovebul message-ebs vigheb. gavagebine gamomgzavns, rom mivighe message -> status = read
          const respArr = []
          for (const message of messages) {
            that.handleReceivedMessage(message)
            respArr.push(message.meta)
          }
          callback(respArr)
          // this.initChat()
        }
      })
      socket.off(this.$enums.events.chatNotifFromBack)
      socket.on(this.$enums.events.chatNotifFromBack, (meta) => {
        // adre message gavgzavne da shevitkve mimghebis mier message-is mighebis shesaxeb, davade status:'received'
        this.$g.clog('cC meta=', meta)
        // dabrunda -> meta ...status:'received'
        // senderId-ti mimghebi daamushavebs, misi message-ia tu ara
        for (const messageOrArray of this.$g.store.getters.messageList) {
          // tu FileMessage-ia mashin array aris
          const msg = this.$g.isArray(messageOrArray) ? messageOrArray[0] : messageOrArray
          if (msg.ts === meta.tsList.notSent) {
            // this.$g.clog('cC msg2=', msg)
            // chemi gagzavnilia  msg.meta = meta
            this.$g.setObjectProperty(msg, 'meta', meta)
            if (msg.pseudoIdbMedia && msg.pseudoIdbMedia.media) {
              this.$g.setObjectProperty(msg.pseudoIdbMedia.media, 'tsList', meta.tsList)
            }
          }
        }
      })
      socket.off(this.$enums.events.tokenError)
      socket.on(this.$enums.events.tokenError, (tokenError) => {
        // error-ebia da unda achvenos snackbarText
        this.$g.clog('cC tokenError=', tokenError)
      })
      // status: typing, onFocus, offFocus
      // typing info = { meta: { type: 'typing'}, meta: { sender: senderId, receiver: receiverId, status: payload } }
      socket.off(this.$enums.events.typingFromBack)
      socket.on(this.$enums.events.typingFromBack, (typingMessage) => {
        typingMessage.data = {}
        this.$g.clog('cC typingFromBack typingMessage ', JSON.stringify(typingMessage))
        // update receiver language -> აღარ ვხმარობთ, რადგანს სერვერი აღარ არკვევს lang-ს
        // this.$g.adjustReceiverLanguage(typingMessage)
        this.$g.chatEBus.$emit(this.$enums.events.oppositeLocale, typingMessage.meta.sender.lang)
        this.$g.store.commit('setTypingMessage', typingMessage)
      })
    },
    // https://kfz-soft.com/public/transporter/tfotos//5d4efdf5b6d2f924f05d4d39/2019-8-29/1567081588114.png
    handleReceivedMessage(message) {
      message.meta.status = this.$enums.status.read
      // message.meta.tsArr.push(Date.now()) // received ts
      message.meta.tsList[this.$enums.status.read] = Date.now()
      // cavikitxot da davsvat gamomgzavnis locale -> senderLang
      if (
        message &&
        message.meta &&
        message.meta.data &&
        message.meta.data.textData &&
        message.meta.data.textData.senderLang
      ) {
        this.$g.getReceiver().lang = message.meta.data.textData.senderLang
      }
      // tu failia calcalke davumatot. dajgufeba mxolod initChat-is dros moxdeba
      if (message.meta.ext) {
        this.addPseudoIdbMedia(message)
        this.addMessageToList([message])
      } else {
        this.addMessageToList(message)
      }
      this.$g.clog('cC handleReceivedMessage message ', JSON.stringify(message))
    }
  }
}
