import axios from 'axios'
import { slugify } from '../helpers/help'
import { authenticate } from '../helpers/axiosMethods'

export const chatLogin = {
  mounted() {
    // eslint-disable-next-line prettier/prettier
    this.$g.clog('cL mounted this.$g.store.getters.isAuthenticated=', this.$g.store.getters.isAuthenticated)
    // this.$g.clog('cL mounted this.$g.store.getters.tokenExpiration=',this.$g.store.getters.tokenExpiration)
  },
  props: {
    loginTexts: {
      type: Object,
      // prettier-ignore
      default: () => { }
    }
  },
  data() {
    return {
      email: '',
      username: '',
      password: ''
    }
  },
  methods: {
    // slugify,
    initUserData(sign) {
      // let sign = 'login'  // signup
      // admin-s mxolod server anichebs
      // let admin = this.password  // admin_pattern
      // this.$g.store.commit('SET_ADMIN', admin)
      this.$g.clog('cL this.password ', this.password)
      // let admin = this.$g.store.getters.isAdmin?this.$g.store.getters.isAdmin:null

      const email = this.email // || (admin?'admin1@web.de':'test1@web.de')
      const password = this.password || slugify(email) // (admin?'admin%1':'test1webde')
      this.$g.clog('cL password ', password)
      const username = this.username || slugify(email) // (admin?'admin1':'a2')
      const name = this.name // || 'Fisher'
      const salutation = this.salutation || (this.loginTexts ? this.loginTexts.salutation : null) // 'Herr'
      const telefon = this.telefon // || '017794654678'
      const whatsapp = this.whatsapp // || '0177946522'
      const contactType = this.contactType

      const userdata = {
        baseUrl: process.env.loginUrl,
        meta: { collName: 'users', type: 'text', intent: sign, ts: Date.now() },
        data: {
          username,
          email,
          password,
          name,
          salutation,
          telefon,
          whatsapp,
          contactType
        },
        // avatarUrl, chatTitleName, suggestions
        chatData: { senderLang: this.$g.store.getters.locale }
      }
      /* if (admin) {
        userdata.data.admin = admin
      } */
      return userdata
    },
    auth(sign, userData, handleSuccess) {
      let data
      if (userData) {
        data = userData
      } else {
        data = this.initUserData(sign) // sign = 'login' or 'signup'
      }
      this.$g.clog('cL data ', JSON.stringify(data))
      if (this._validate()) {
        authenticate(data, handleSuccess, this.errorHandler)
      }
    },
    // aq response = response.data
    handleLoginSuccess(response) {
      this.$g.clog('cL response ', JSON.stringify(response))
      /*
      // dabrunda bazidan { 'user_returned': true, user, tokenInfo }
      {"result":"user_returned", an 'user_created'
      "user":{
        "_id":"5d6ae3b14f4c182674954eea",
        "meta":{"collName":"users","type":"text","intent":"signup","ts":1567286192871},
        "data":{"username":"ert","email":"gia-lomidze@web.de","name":"Fisher","salutation":"Herr","tel":"017794654678","whatsapp":"0177946522","salt":"9879504383"},
        "chatData":{"senderLang":"de","suggestions":["Guten Tag, was kann ich für Sie tun ?"],"avatarSrc":"http://localhost:4128/tfotos/5d6ae3b14f4c182674954eea/avatar/1567600555465_14285.png","chatetName":"Herr Lomidze ori"}
        "tokenInfo":{"expiresIn":3600,"token":"eyJhbGciOiJIUzI1NiIs..."}}
      },
      'serverData':{"fileSendMode":"siofu","chatUrl":"http://localhost:4129","imageSrcBase":"http://localhost:4128/tfotos/","avatarSrcDefault":"http://localhost:4128/avatar_default/avatar.png","messageTsFormat":"HH:mm","uploadFileMaxSize":2,"uploadFileChunkSize":1024000,"fotosUploadUrl":"http://localhost:4128/uploadFoto","flagsImageBase":"http://localhost:4128/flags/"}
      */

      let text = ''
      if (response.result === 'user_created' || response.result === 'user_returned') {
        this.saveUser(response) // chatLogin.js-shia ->  this.$g.store.commit('setUserAndServerData', response.data)
        this.goToChat()
        if (response.result === 'user_returned') {
          text = this.loginTexts.user_account_existed_and_logged
        } else if (response.result === 'user_created') {
          text = this.loginTexts.user_account_created_and_logged
        }
        this.$g.chatEBus.$emit(this.$enums.cbt.SNACKBAR_INFO, {
          text,
          timeout: 3000,
          color: 'success'
        })
      } else if (response.text === 'This email already exists') {
        // error-ia ->  es user ukve arsebobs  This email already exists
        text = this.loginTexts.email_already_exists_warning
        this.$g.chatEBus.$emit(this.$enums.cbt.SNACKBAR_INFO, {
          text,
          timeout: 2000,
          color: 'error'
        })
      }
    },
    saveUser(data) {
      this.$g.clog('cL data ', JSON.stringify(data))
      this.$g.store.commit('setUserAndServerData', data)
      this.$g.clog('cL this.$g.store.getters.user ', JSON.stringify(this.$g.store.getters.user))
    },
    saveUserAndGoToChat(data) {
      this.$g.clog('cL saveUserAndGoToChat')
      this.saveUser(data)
      this.$g.clog('cL imageSrcBase', this.$g.store.getters.user.imageSrcBase)
      this.$g.chatEBus.$emit(this.$enums.cbt.TO_ROOM) // chats/index.vue-ში shedis
      // chatEBus.$emit(cbt.INIT_SOCKET)
    },
    goToRoom() {
      this.$g.chatEBus.$emit(this.$enums.cbt.TO_ROOM) // chats/index.vue-ში shedis
    },
    goToChat() {
      this.$g.chatEBus.$emit(this.$enums.cbt.TO_CHAT) // chats/index.vue-ში shedis
    },
    // es signupCallback mxolod signup-is dros sheizleba ikos tu ->This email already exists
    errorHandler(error, signupCallback) {
      this.$g.clog('cL error', error)
      this.$g.clog('cL error.message', error.message)
      this.$g.clog('cL signupCallback', signupCallback)
      let errorText = ''
      if (error.response) {
        /*
         * The request was made and the server responded with a
         * status code that falls out of the range of 2xx
         */
        this.$g.clog('cL error.response.data', error.response.data)
        this.$g.clog('cL error.response.status', error.response.status)
        // step3Form.vue ikenebs signup-is dros
        // error.response.data = {name: "auth", category: "signup", text: "This email already exists", status: 400}
        if (signupCallback) {
          signupCallback(error.response.data)
        }
        // this.$g.clog('error.response.headers',error.response.headers);

        // {name: "auth", category: "signup", text: "This email already exists", status: 400}
        // {name: "auth", category: "login", text: "Email or password is incorrect", status: 400}
        errorText = 'error-response: ' + error.response.data.text
      } else if (error.request) {
        /*
         * The request was made but no response was received, `error.request`
         * is an instance of XMLHttpRequest in the browser and an instance
         * of http.ClientRequest in Node.js
         */
        this.$g.clog('cL error.request', error.request)
        // objectLooper(error.request)
        errorText = 'cL error-request: ' + error.message
      } else {
        // Something happened in setting up the request and triggered an Error
        this.$g.clog('cL error.message', error.message)
        errorText = 'cL error-message: ' + error.message
      }
      // callback-is shemtxvevashi error-is teqsts vikenebt sxva nabijistvis da am dros ar gvinda SNACKBAR_INFO
      if (!signupCallback) {
        this.$g.chatEBus.$emit(this.$enums.cbt.SNACKBAR_INFO, {
          text: errorText,
          timeout: 3000,
          color: 'error'
        })
      }
    },
    _validate() {
      if (!this.$refs.emailForm) {
        this.$g.clog('cL this.$refs.emailForm existiert nicht')
        // mashin sxva komponentidan vikenebt am metods da validate() ar gvchirdeba, amitom true
        return true
      }
      this.$g.clog('cL this.$refs.emailForm = ' + this.$refs.emailForm)
      this.$g.clog('cL this.$refs.login_snackbar = ' + this.$refs.login_snackbar)
      if (this.$refs.emailForm.validate()) {
        this.$g.clog('cL validate = true')
        return true
      } else {
        this.$g.clog('cL validate = false')
        return false
      }
    },

    login2(userData, callback) {
      this.$g.clog('cL loginUrl= ' + process.env.loginUrl)
      let data
      if (userData) {
        data = userData
      } else {
        data = this.initUserData('login')
        this.$g.clog('cL data ', JSON.stringify(data))
      }
      // this.$g.clog('data=', data)
      this.$g.clog('cL data ', JSON.stringify(data))
      if (this._validate()) {
        axios
          .post(process.env.loginUrl + '/login', data)
          .then((response) => {
            this.$g.clog('cL response.data ', JSON.stringify(response.data))
            /*
          // dabrunda bazidan { 'user_returned': true, user, tokenInfo }
          {"result":"user_returned",
          "user":{"_id":"5d6ae3b14f4c182674954eea",
          "meta":{"collName":"users","type":"text","intent":"signup","ts":1567286192871},
          "data":{"username":"ert","email":"gia-lomidze@web.de","name":"Fisher","salutation":"Herr","tel":"017794654678","whatsapp":"0177946522","salt":"9879504383"},
          "chatData":{"senderLang":"de","suggestions":["Guten Tag, was kann ich für Sie tun ?"],"avatarSrc":"http://localhost:4128/tfotos/5d6ae3b14f4c182674954eea/avatar/1567600555465_14285.png","chatetName":"Herr Lomidze ori"}},
          "tokenInfo":{"expiresIn":3600,"token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZDZhZTNiMTRmNGMxODI2NzQ5NTRlZWEiLCJpYXQiOjE1NjgxNDE1OTYsImV4cCI6MTU2ODE0NTE5Nn0.4EDcPswtDG6dlH0xQ1SMFzGOGDmO6ECfVRJX6kRvmxQ","serverData":{"fileSendMode":"siofu","chatUrl":"http://localhost:4129","imageSrcBase":"http://localhost:4128/tfotos/","avatarSrcDefault":"http://localhost:4128/avatar_default/avatar.png","messageTsFormat":"HH:mm","uploadFileMaxSize":2,"uploadFileChunkSize":1024000,"fotosUploadUrl":"http://localhost:4128/uploadFoto","flagsImageBase":"http://localhost:4128/flags/"}}}
          */
            this.saveUser(response.data)
            if (callback) {
              callback(response.data)
            } else {
              this.goToChat()
            }
          })
          .catch(this.errorHandler)
      }
    },
    signup2(userData, callback) {
      // this.$g.clog('cL this.$g.store.getters.isAdmin= ' + $store.getters.user && $store.getters.user.admin)
      // this.$g.store.commit('SET_LOGIN_URL','http://localhost:4001')
      let data
      if (userData) {
        data = userData
      } else {
        data = this.initUserData('signup')
      }
      this.$g.clog('cL userData ', JSON.stringify(data))
      this.$g.clog('cL process.env.loginUrl ', process.env.loginUrl)
      if (this._validate()) {
        axios
          .post(process.env.loginUrl + '/signup', data)
          .then((response) => {
            // this.$g.clog(response.headers);
            this.$g.clog('cL response.data', response.data)
            // dabrunda bazidan { result: 'user_created', user, tokenInfo }
            this.saveUser(response.data)
            if (callback) {
              callback(response.data)
            } else {
              this.goToChat()
            }
          })
          .catch((err) => {
            this.errorHandler(err, callback)
          })
      }
    }
  }
}
