export const idbMediasMixin = {
  props: {},
  data() {
    return {
      componentKey: 0,
      idbMedias: null,
      idbFiles: null
    }
  },
  methods: {
    handleAllIdbMedias({ allIdbMedias, downloaded }) {
      this.$g.clog('imm allIdbMedias ', allIdbMedias)
      // this.$g.clog('imm this = ', this)
      if (!allIdbMedias) {
        this.idbFiles = null
        this.idbMedias = null
        return 0
      }
      const idbFiles = []
      const idbMedias = []
      const idbMediasDownloaded = []
      for (const idbMedia of allIdbMedias) {
        idbMediasDownloaded.push(idbMedia)
        if (idbMedia.media.type === 'file') {
          idbFiles.push(idbMedia)
        } else {
          idbMedias.push(idbMedia)
        }
      }
      if (!downloaded) {
        if (idbFiles.length) {
          // this.$g.clog('imm idbFiles ', idbFiles)
          this.idbFiles = idbFiles
        } else {
          this.idbFiles = null
        }
        if (idbMedias.length) {
          this.idbMedias = idbMedias
        } else {
          this.idbMedias = null
        }
      }
      // tu mongodan aris chamotvirtuli, file-ebic image-ebshi iqneba
      else if (idbMediasDownloaded.length) {
        this.idbMedias = idbMediasDownloaded
      } else {
        this.idbFiles = null
        this.idbMedias = null
      }
      this.forceRerender()
      const filesLength = this.idbFiles ? this.idbFiles.length : 0
      const mediasLength = this.idbMedias ? this.idbMedias.length : 0
      return filesLength + mediasLength
    },
    forceRerender() {
      this.componentKey += 1
      // this.$g.clog('imm forceRerender componentKey = ', this.componentKey)
    }
  }
}
