export const indexedDBMixin = {
  data() {
    return {
      dbName: 'deservice'
      // sendIconColor: this.messageColors.messageNotReceived
    }
  },
  methods: {
    deleteDB(dbName, createDB) {
      const req = indexedDB.deleteDatabase(dbName)
      req.onsuccess = () => {
        this.$g.clog('Deleted database successfully')
        // shevqmenit xelaxla
        if (createDB) {
          createDB(dbName)
        }
      }
      req.onerror = () => {
        // this.$g.clog("Couldn't delete database")
        this.$g.showSnackbar(this.$g.t('qchat.snackbar.idb_not_deleted'), 2000, 'error')
      }
      req.onblocked = () => {
        // this.$g.clog("Couldn't delete database due to the operation being blocked")
        this.$g.showSnackbar(this.$g.t('qchat.snackbar.idb_delete_blocked'), 2000, 'error')
      }
    },
    createDB(dbName) {
      if (!('indexedDB' in window)) {
        this.$g.clog('This browser doesnt support IndexedDB')
        this.$g.showSnackbar(this.$g.t('qchat.snackbar.not_support_idb'), 2000, 'error')
        // alert('This browser doesnt support IndexedDB')
        return
      }
      this.$g.clog('createDB2 ', dbName)
      const request = indexedDB.open(dbName, 1)
      request.onupgradeneeded = (event) => {
        const db = event.target.result
        let n = 0
        // Object stores should contain objects of the same "type" (not JavaScript data type)
        // { cardId: '123', files: [], images: []}
        // { cardIdAndQuestionnaireIndex: '123_1.2', files: [], images: []}
        if (!db.objectStoreNames.contains('chatFiles')) {
          const objectStore = db.createObjectStore('chatFiles', {
            keyPath: 'id',
            autoIncrement: true
          })
          // objectStore.createIndex('indexName', 'property', options)
          objectStore.createIndex('cardId', 'cardId', { unique: false })
          n++
        }
        if (!db.objectStoreNames.contains('chatImages')) {
          const objectStore = db.createObjectStore('chatImages', {
            keyPath: 'id',
            autoIncrement: true
          })
          // objectStore.createIndex('indexName', 'property', options)
          objectStore.createIndex('cardId', 'cardId', { unique: false })
          n++
        }
        if (!db.objectStoreNames.contains('questionnaireFiles')) {
          const objectStore = db.createObjectStore('questionnaireFiles', {
            keyPath: 'id',
            autoIncrement: true
          })
          objectStore.createIndex('cardId', 'cardId', { unique: false })
          objectStore.createIndex('cardIdAndQuestionnaireIndex', 'cardIdAndQuestionnaireIndex', { unique: false })
          n++
        }
        if (!db.objectStoreNames.contains('questionnaireImages')) {
          const objectStore = db.createObjectStore('questionnaireImages', {
            keyPath: 'id',
            autoIncrement: true
          })
          objectStore.createIndex('cardId', 'cardId', { unique: false })
          objectStore.createIndex('cardIdAndQuestionnaireIndex', 'cardIdAndQuestionnaireIndex', { unique: false })
          n++
        }
        if (n === 0) {
          this.$g.clog('DB exists!')
        } else {
          this.$g.clog(`DB was created with ${n} objectStores!`)
        }
      }
      request.onsuccess = (ev) => {
        this.$g.clog('DB exists!!', ev)
      }
    }
    /* // addMedia
    addMedia(data) {
      // const data = { cardId: '123', questionnaireIndex: '1.2', file, image }
      const request = window.indexedDB.open(this.dbName, 1)
      request.onsuccess = ev => {
        const db = ev.target.result
        let transaction
        let store
        let addData
        if (!data.questionnaireIndex) {
          // mashin chatia
          if (data.file) {
            // mashin add chatFile
            transaction = db.transaction('chatFiles', 'readwrite')
            store = transaction.objectStore('chatFiles')
            addData = { cardId: data.cardId, file: data.file }
          } else if (data.image) {
            // mashin add chatImage
            transaction = db.transaction('chatImages', 'readwrite')
            store = transaction.objectStore('chatImages')
            addData = { cardId: data.cardId, image: data.image }
          }
        }
        // mashin questionnaire-ia
        else if (data.file) {
          // mashin add questionnaireFile
          transaction = db.transaction('questionnaireFiles', 'readwrite')
          store = transaction.objectStore('questionnaireFiles')
          addData = { cardIdAndQuestionnaireIndex: data.cardId + '_' + data.questionnaireIndex, file: data.file }
        } else if (data.image) {
          // mashin add questionnaireImage
          transaction = db.transaction('questionnaireImages', 'readwrite')
          store = transaction.objectStore('questionnaireImages')
          addData = {
            cardIdAndQuestionnaireIndex: data.cardId + '_' + data.questionnaireIndex,
            image: data.image
          }
        }
        // chavceret
        store.add(addData)
        // data.forEach(el => store.add(el))
        transaction.onerror = ev => {
          console.error('Ein Fehler ist aufgetreten!', ev.target.error.message)
        }
        transaction.oncomplete = ev => {
          this.$g.clog('Daten wurden erfolgreich hinzugefügt!')
        }
      }
    },
    readChatFiles(data) {
      // const data = { cardId:'135', mediaType:'image',  questionnaireIndex:'1.2' }
      const request = window.indexedDB.open(this.dbName, 1)
      request.onsuccess = ev => {
        const db = ev.target.result
        let transaction
        let store
        if (!data.questionnaireIndex) {
          // mashin chatia
          if (data.mediaType === 'file') {
            // mashin get chatFiles
            transaction = db.transaction('chatFiles', 'readonly')
            store = transaction.objectStore('chatFiles')
            const index = store.index('cardId')
            // addData = { cardId: data.cardId, file: data.file }
            const query = index.openCursor()
            query.onerror = ev => {
              console.error('Anfrage fehlgeschlagen!', ev.target.error.message)
            }
            query.onsuccess = ev => {
              const cursor = ev.target.result
              const chatFiles = []
              if (cursor) {
                this.$g.clog(cursor.key, cursor.value.cardId)
                // { cardId: '123', file: file}
                if (cursor.value.cardId === data.cardId) {
                  chatFiles.push(cursor.value.file)
                }
                cursor.continue()
              } else {
                this.$g.clog('Keine Einträge mehr vorhanden!')
                return chatFiles
              }
            }
          }
        }
      }
    },
    readMedias(data) {
      // const data = { cardId:'135', mediaType:'image',  questionnaireIndex:'1.2' }
      const request = window.indexedDB.open(this.dbName, 1)
      request.onsuccess = ev => {
        const db = ev.target.result
        let transaction
        let store
        let addData
        if (!data.questionnaireIndex) {
          // mashin chatia
          if (data.mediaType === 'file') {
            // mashin get chatFiles
            transaction = db.transaction('chatFiles', 'readonly')
            store = transaction.objectStore('chatFiles')
            const index = store.index('cardId')
            // addData = { cardId: data.cardId, file: data.file }
            const query = index.openCursor()
            query.onerror = ev => {
              console.error('Anfrage fehlgeschlagen!', ev.target.error.message)
            }
            query.onsuccess = ev => {
              const cursor = ev.target.result
              const chatFiles = []
              if (cursor) {
                this.$g.clog(cursor.key, cursor.value.cardId)
                // { cardId: '123', file: file}
                if (cursor.value.cardId === data.cardId) {
                  chatFiles.push(cursor.value.file)
                }
                cursor.continue()
              } else {
                this.$g.clog('Keine Einträge mehr vorhanden!')
                return chatFiles
              }
            }
          } else if (data.mediaType === 'image') {
            // mashin get chatImages
            transaction = db.transaction('chatImages', 'readonly')
            store = transaction.objectStore('chatImages')
            addData = { cardId: data.cardId, image: data.image }
          }
        }
        // mashin questionnaire-ia
        else if (data.file) {
          // mashin add questionnaireFile
          transaction = db.transaction('questionnaireFiles', 'readonly')
          store = transaction.objectStore('questionnaireFiles')
          addData = { cardIdAndQuestionnaireIndex: data.cardId + '_' + data.questionnaireIndex, file: data.file }
        } else if (data.image) {
          // mashin add questionnaireImage
          transaction = db.transaction('questionnaireImages', 'readonly')
          store = transaction.objectStore('questionnaireImages')
          addData = {
            cardIdAndQuestionnaireIndex: data.cardId + '_' + data.questionnaireIndex,
            image: data.image
          }
        }
        // chavceret
        store.add(addData)
        // data.forEach(el => store.add(el))
        transaction.onerror = ev => {
          console.error('Ein Fehler ist aufgetreten!', ev.target.error.message)
        }
        transaction.oncomplete = ev => {
          this.$g.clog('Daten wurden erfolgreich hinzugefügt!')
        }
      }
    },
    readMedias2(data) {
      const request = window.indexedDB.open(this.dbName, 1)
      // const data = { cardId:'135', mediaType:'image',  questionnaireIndex:'1.2' }
      request.onsuccess = ev => {
        const db = ev.target.result
        const store = db.transaction('user', 'readonly').objectStore('user')
        // const query = store.get(1); // Einzel-Query
        const query = store.openCursor()
        query.onerror = ev => {
          console.error('Anfrage fehlgeschlagen!', ev.target.error.message)
        }
        query.onsuccess = ev => {
          const cursor = ev.target.result
          if (cursor) {
            this.$g.clog(cursor.key, cursor.value.chat, cursor.value.questionnaire)
            cursor.continue()
          } else {
            this.$g.clog('Keine Einträge mehr vorhanden!')
          }
        }
      }
    },
    deleteData(dbName) {
      const request = window.indexedDB.open(dbName, 1)
      request.onsuccess = ev => {
        const db = ev.target.result
        const store = db.transaction(['Nutzer'], 'readwrite').objectStore('Nutzer')
        store.delete(1)
        store.onsuccess = event => {
          this.$g.clog('Eintrag erfolgreich gelöscht!')
        }
      }
    } */
  }
}
