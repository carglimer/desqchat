import { getDataById } from '../helpers/axiosMethods'
// murmanis project-s da chems project-s shoris interface-ia es
export default {
  created() {
    this.$g.updateReaderLocale(this.readerLocale)
  },
  mounted() {
    // console.log('im mounted')
    // aucileblad mounted()-idan camovighot murmanis bazidan user and card
    this.fetchUser(this.userId, this.cardId)
  },
  props: {
    userId: {
      type: Number,
      default: null
    },
    cardId: {
      type: Number,
      default: null
    },
    readerLocale: {
      type: String,
      default: 'de'
    }
    /* ,
    სალამი გია,• შენი კომპონენტების desChat და desQuestionnaire-ს გამოძახებისას აღარ გადმოგცემ ცვლად locale;
    locale: {
      type: String,
      default: process.env.DEFAULT_LANG
    } */
  },
  data() {
    return {
      // globalImages: []
    }
  },
  computed: {},
  watch: {
    readerLocale(locale, oldVal) {
      // this.$g.clog('im locale ', locale)
      if (locale) {
        this.$g.updateReaderLocale(locale)
      }
    },
    userId(userId, oldValue) {
      this.$g.clog('im userId = ', userId)
      if (userId) {
        this.fetchUser(userId)
      }
    },
    cardId(cardId, oldValue) {
      this.$g.clog('im cardId = ', cardId)
      if (cardId) {
        this.fetchCard(cardId)
      }
    }
  },
  methods: {
    fetchUser(userId = this.userId, cardId = this.cardId) {
      this.$g.clog('im fetchUser userId = ', userId)
      this.$g.clog('im fetchUser cardId = ', cardId)
      // this.$g.clog('im qed = ', this.$g.store.getters.questionnaireExampleData)
      if (!userId) {
        return
      }
      const user = this.$g.store.getters.user
      this.$g.clog('im fetchUser user = ', user)
      if (this.$g.isExampleQuestionnairePage(cardId)) {
        this.$g.clog('im isExampleQuestionnairePage = true')
        // mashin exampleQuestionnaire-is page-ia
        let level = this.$enums.levels.basic
        if (this.$g.store.getters.questionnaireExampleData.advanced.cardId === cardId) {
          level = this.$enums.levels.advanced
        } else if (this.$g.store.getters.questionnaireExampleData.phone.cardId === cardId) {
          level = this.$enums.levels.phone
        }
        if (!user) {
          this.$g.store.commit('setUser', this.$g.adjustUserMMS({ id: userId }))
        }
        this.$g.store.commit('setCard', this.$g.adjustCardIndicator({ id: cardId, level }))
        // this.$g.chatEBus.$emit(events.loadQuestionnaire, cardId)
        return
      }
      if (this.$g.isExampleQuestionnaireCreating()) {
        this.$g.clog('im isExampleQuestionnaireCreating = true')
        // mashin exampleQuestionnaire-ebis sheqmna gvinda
        let level = this.$enums.levels.basic
        const qed = this.$g.store.getters.questionnaireExampleData
        cardId = qed.basic.cardId
        if (qed.exampleBasic) {
          level = this.$enums.levels.basic
          cardId = qed.basic.cardId
        } else if (qed.exampleAdvanced) {
          level = this.$enums.levels.advanced
          cardId = qed.advanced.cardId
        } else if (qed.examplePhone) {
          level = this.$enums.levels.phone
          cardId = qed.phone.cardId
        }
        if (!user) {
          this.$g.store.commit('setUser', this.$g.adjustUserMMS({ id: userId }))
        }
        const card = this.$g.store.getters.card
        this.$g.clog('im isExampleQuestionnaireCreating card = ', card)
        if (card && card.id === cardId) {
          this.$g.chatEBus.$emit(this.$enums.events.loadQuestionnaire, cardId)
        } else this.$g.store.commit('setCard', this.$g.adjustCardIndicator({ id: cardId, level }))
        return
      }

      this.$g.clog('im user = ', user)
      // user.id < 0 mashin examples aris
      if (userId < 0 || (user && user.id === userId)) {
        this.$g.clog('im cardId = ', cardId)
        // mashin user ukve gvaqvs gamozaxebuli
        if (cardId) {
          this.fetchCard(cardId)
        } else this.informQuestionnaire(true)
        return
      }
      // develop-is dros mxolod testData-s vxmarobt
      if (process.env.G_TESTDATA && JSON.parse(process.env.G_TESTDATA)) {
        const user = this.$g.store.getters.testUsers.find((user) => user.recId === userId)
        // console.log('im user ', user)
        if (user) {
          this.$g.store.commit('setUser', this.$g.adjustUser(user))
          this.$g.clog('im this.$g.store.getters.user', this.$g.store.getters.user)
        }
        if (cardId) {
          this.fetchCard(cardId)
        } else this.informQuestionnaire(true)
        return
      }
      // jer user-s movitxovt, shemdeg card-s
      const requestObj = { userId }
      // vekitxebi chems servers, romelic ekitxeba murmanis servers
      getDataById(requestObj)
        .then((data) => {
          this.$g.clog('im fetchuser success data = ', data)
          this.informQuestionnaire(false)
          if (data && data.length) {
            const user = data[0]
            this.$g.clog('im data user = ', user)
            this.$g.store.commit('setUser', this.$g.adjustUser(user))
            this.$g.clog('im data cardId = ', cardId)
            if (cardId) {
              this.fetchCard(cardId)
            } else this.informQuestionnaire(true)
            // es ava-s test-s chirdeba
            Promise.resolve(user)
          } else {
            // err: "Error: Request failed with status code 404"
            this.handleErr('user = ', 'data error')
          }
        })
        .catch((e) => {
          this.handleErr('user', e)
        })
    },
    fetchCard(cardId = this.cardId) {
      // jer user-s movitxovt, shemdeg card-s
      this.$g.clog('im cardId = ', cardId)
      const card = this.$g.store.getters.card
      this.$g.clog('im card = ', card)
      if (card && card.id === cardId) {
        // mashin card ukve gvaqvs gamozaxebuli
        // locale უნდა adjust, რადგან შეიძლება de აირჩიოს და card-ში როგორც testers ka ჰქონდეს არჩეული
        this.$g.adjustLocale()
        if (this.questionnaire) {
          // this.questionnaire-is arsebobit ubralod vxvdebit chat-is fanjaraa tu questionnaire-is
          this.$g.chatEBus.$emit(this.$enums.events.loadQuestionnaire)
        } else {
          this.$g.chatEBus.$emit(this.$enums.events.initChat)
        }
        return
      }
      // test-is dros mxolod testData-s vxmarobt
      if (process.env.G_TESTDATA && JSON.parse(process.env.G_TESTDATA)) {
        const card = this.$g.store.getters.testCards.find((card) => card.recId === cardId)
        this.$g.clog('im 174 card = ', card)
        if (card) {
          this.$g.clog('im 176 card = ', card)
          this.$g.store.commit('setCard', this.$g.adjustCard(card))
        }
        return
      }
      this.$g.store.commit('clearMessageList')
      const requestObj = { cardId }
      // vekitxebi chems servers, romelic ekitxeba murmanis servers
      getDataById(requestObj)
        .then((data) => {
          this.$g.clog('im fetchCard success data = ', data)
          if (data) {
            this.informQuestionnaire(false)
            if (data.length) {
              const card = data[0]
              // tu card sheicvala card-is watcher daqoqavs this.initChat() da this.loadQuestionnaire()
              this.$g.store.commit('setCard', this.$g.adjustCard(card))
              // locale უნდა adjust, რადგან შეიძლება de აირჩიოს და card-ში როგორც testers ka ჰქონდეს არჩეული
              this.$g.adjustLocale()
              // if (initChat) { this.initChat() }
            } else {
              // mashin bazidan ar movida card
              const text = this.$enums.etexts.cardWasNotFound + ' cardId = ' + cardId
              this.$g.showSnackbar(text, 10000, 'red', this.getCaller())
            }
          }
        })
        .catch((e) => {
          this.handleErr('card', e)
        })
    },
    getCaller() {
      return this.questionnaire ? this.$enums.caller.questionnaire : this.$enums.caller.chat
    },
    // gavagebinot questionnaire.vue-s aris networkError tu ara
    informQuestionnaire(networkError) {
      if (this.getCaller() === this.$enums.caller.questionnaire) {
        this.$g.chatEBus.$emit(this.$enums.events.networkError, networkError)
      }
    },
    handleErr(subject, e) {
      this.$g.clog('im handleErr = ' + subject + ':', e)
      this.$g.showSnackbar(subject + ':' + e, 3000, 'error', this.getCaller())
      this.informQuestionnaire(true)
    }
  }
}
