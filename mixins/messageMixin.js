import Autolinker from 'autolinker'
// import moment from 'moment'
import { htmlEscape } from '../helpers/escape'
const fmt = require('msgdown')

export const messageMixin = {
  props: {
    message: {
      type: Object,
      default: null
    },
    messageStyling: {
      type: Boolean,
      default: true
    }
  },
  data() {
    return {
      messageIsMy: false
      // statusIcon: 'clock-outline',
      // sendIconColor: 'message_not_received'
    }
  },
  computed: {
    justifyRow() {
      return this.messageIsMy ? 'end' : 'start'
    },
    messageStyle() {
      let bg
      let txt
      if (this.$g.isMy(this.message)) {
        bg = 'message_bg_me'
        txt = 'message_txt_me'
        this.messageIsMy = true
      } else {
        bg = 'message_bg_he'
        txt = 'message_txt_he'
        this.messageIsMy = false
      }
      // return { backgroundColor: bg, color: txt }
      return { [bg]: true, ['text--' + txt]: true }
    },
    messageHasTs() {
      this.$g.clog('mm this.message ', JSON.stringify(this.message))
      // eslint-disable-next-line prettier/prettier
      return (
        this.message && this.message.meta && this.message.meta.tsList && Object.keys(this.message.meta.tsList).length
      )
    },
    translated() {
      let translated = false
      const textData = this.message.data.textData
      if (textData && textData.translatedText && !this.$g.isMy(this.message)) {
        translated = true // textData.senderLang !== textData.receiverLang
      }
      return translated
    },
    messageText() {
      // textData = {senderLang:'de', receiverLang: 'ka', text: 'Haus Tür', translatedText:'სახლი კარი' }
      const textData = this.message.data.textData
      this.$g.clog('mm this.message.data.textData ', textData)
      if (!textData) {
        return {}
      }
      this.$g.clog('mm textData.text ', textData.text)
      // davamushaot tu link-ebi aris
      textData.text = this.escapeAndLinkedText(textData.text)
      if (textData.translatedText) {
        textData.translatedText = this.escapeAndLinkedText(textData.translatedText)
      }
      return textData
    }
  },
  methods: {
    escapeAndLinkedText(_text) {
      // const escaped = escapeGoat.escape(this.data.text) ასე იყო
      this.$g.clog('mm text', _text)
      const escaped = htmlEscape(_text)
      return Autolinker.link(this.messageStyling ? fmt(escaped) : escaped, {
        className: 'chatLink',
        truncate: { length: 50, location: 'smart' }
      })
    }
  }
}
