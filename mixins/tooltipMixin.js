export default {
  props: {},
  data() {
    return {
      show: false
    }
  },
  methods: {
    showTooltip() {
      this.show = true
      setTimeout(() => {
        this.show = false
      }, 3000)
    }
  }
}
