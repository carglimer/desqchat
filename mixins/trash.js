let ss, events, collnames, intents
const SocketIOFileUpload = require('socketio-file-upload')
export function downloadFile(idbMedia) {
  const message = { meta: { ...idbMedia.meta, caller: 'chat', collName: collnames.messages } }
  this.$g.adjustMessage(message)
  message.ts = idbMedia.media.tsList[status.notSent]
  message.meta.intent = intents.DOWNLOAD // 'download'
  this.$g.clog('cC downloadFile message = ', JSON.stringify(message))
  this.isp(this.emitDownloadFileEvent, message)
}
export function emitDownloadFileEvent(message) {
  this.$g.downloadFileFromServer(message.meta.relPath)
  /* // gavagzavne da veli migebis dadasturebas
    this.$g.socket.emit(events.downloadFileFromBack, message, message => {
      // dabrunda bazidan {download:true}
      this.$g.clog('cC message=', message)
      if (message.meta.error) {
        // mashin  ragac error-ia da achvenebs snackbarText
        const errorTxt = JSON.stringify(message.meta.error)
        this.$g.clog('cC meta.error = ', errorTxt)
        this.$g.showSnackbar(errorTxt, 7000, 'error')
      }
      //  meta.intent = intents.DOWNLOADING
      // es mxolod ertxel unda moxdes -> this.getFileStream <- initSocket()-shia
      // this.getFileStream(this.$g.socket, this.downloadErrorHandler, this.downloadSuccessHandler)
    }) */
}
export function downloadErrorHandler(err) {
  const errorTxt = err // an sxva rame
  this.$g.showSnackbar(errorTxt, 5000, 'error')
}
export function downloadSuccessHandler(message, binaryString) {
  const filename = message.meta.relPath.replace(/^.*[\\/]/, '')
  // const filename = path.posix.basename(message.meta.relPath)
  this.$g.clog('cC filename ', filename)
  this.$g.clog('cC sucess binaryString.length= ', binaryString.length)
  this.createBlobFromBinaryString(message, binaryString)
}
export function getFileStream(socket, err, success) {
  ss(socket).on(events.downloadFileFromBack, (stream, message, callback) => {
    this.$g.clog('cC received message', message)
    this.$g.clog('cC success', success)
    let binaryString = ''
    let progress = 0
    stream.on('data', (chunk) => {
      this.$g.clog('chunk.length = ', chunk.length)
      this.$g.clog('message.meta.size = ', (chunk.length / message.meta.size) * 100)
      progress += (chunk.length / message.meta.size) * 100
      this.$g.clog('cc progress = ', progress)
      this.$g.chatEBus.$emit(events.fileDownloadProgress, { id: message.idbTs, progress: Math.floor(progress) })
      for (let i = 0; i < chunk.length; i++) {
        binaryString += String.fromCharCode(chunk[i])
      }
    })
    stream.on('error', (error) => {
      this.$g.clog('cC Error found:', error)
      err(error)
    })
    stream.on('close', () => {
      this.$g.clog('cC close')
    })
    stream.on('drain', (e) => {
      this.$g.clog('cC drain')
    })
    stream.on('end', (data) => {
      this.$g.clog('cC end')
      this.$g.clog('cC success', success)
      this.$g.chatEBus.$emit(events.fileDownloadProgress, { id: message.idbTs, progress: 100 })
      if (success) {
        success(message, binaryString)
      }
      // document.getElementById('img').setAttribute("src", "data:image/png;base64," + window.btoa(binaryString))
      // $("#img").attr("src","data:image/png;base64,"+window.btoa(binaryString));
      binaryString = ''
    })
    if (callback) {
      // acknolidgment
      this.$g.clog('cC callback', callback)
    }
  })
}
export function createBlobFromBinaryString(message, binaryString) {
  const meta = message.meta
  const l = binaryString.length
  const array = new Uint8Array(l)
  for (let i = 0; i < l; i++) {
    array[i] = binaryString.charCodeAt(i)
  }
  const blob = new Blob([array], { type: message.meta.type + '/' + message.meta.ext })
  // var b = new Blob([array], {type: 'application/octet-stream'})
  const filename = meta.filename ? meta.filename : 'download' + '_' + Date.now() + '.' + meta.ext
  this.saveBlob(blob, filename)
}
export function saveBlob(blob, fileName) {
  this.$g.clog('cC saveBlob blob ', blob)
  const a = document.createElement('a')
  document.body.appendChild(a)
  a.style = 'display: none'
  const url = window.URL.createObjectURL(blob)
  a.href = url
  a.download = fileName
  a.click()
  window.URL.revokeObjectURL(url)
}
export function sendFileSiofu(message) {
  const files = [message.data.file]
  message.data.file = null
  const callbacks = { ...message.callbacks }
  this.$g.clog('cC callbacks ', callbacks)
  this.$g.clog('cC files ', files)
  message.callbacks = null
  this.$g.clog('cC this.$g.socket ', this.$g.socket)
  const uploader = new SocketIOFileUpload(this.$g.socket)
  // uploader.chunkSize = this.$g.store.getters.uploadFileChunkSize
  // uploader.maxFileSize = this.$g.store.getters.uploadFileMaxSize * 1000000
  this.$g.clog('cC uploader ', uploader)
  uploader.addEventListener('start', (event) => {
    this.$g.clog('cC start uploader')
    this.$g.socket.emit(events.fileSiofuToBack, message, (meta) => {
      this.$g.clog('cC meta ', JSON.stringify(meta))
      if (meta.error) {
        // mashin  ragac error-ia da achvenebs snackbarText
        let errorTxt = JSON.stringify(meta.error)
        if (meta.error.text) {
          errorTxt = JSON.stringify(meta.error.text)
        }
        this.$g.clog('cC meta.error = ', errorTxt)
        // this.$g.showSnackbar(errorTxt, 7000, 'error')
        callbacks.handleErr(errorTxt) // file, errorTxt
      } else if (meta.status === status.savedInDb) {
        // aq mohkveba ganaxlebuli token da davimaxsovrot
        this.$g.store.commit('setToken', meta.token)
        this.$g.clog('cC handleSuccess ', JSON.stringify(message))
        this.setIdAndMeta(message, meta)
        callbacks.handleSuccess(message) // file, meta.status
      }
    })
  })
  uploader.addEventListener('progress', (event) => {
    const percent = (event.bytesLoaded / event.file.size) * 100
    this.$g.clog('cC File is', percent.toFixed(2), 'percent loaded')
    callbacks.handleProgress(percent)
  })
  uploader.addEventListener('complete', (event) => {
    this.$g.clog(event.success)
    this.$g.clog(event.file)
  })
  uploader.addEventListener('error', (event) => {
    if (event.code === 1) {
      this.$g.showSnackbar(this.$g.t('qchat.snackbar.no_big_file'), 2000, 'error')
      // alert("Don't upload such a big file")
    }
  })
  if (this.$g.socket && this.$g.socket.connected) {
    this.$g.clog('cC socket file ', files)
    uploader.submitFiles(files)
  } else {
    this.$g.clog('cC socket disconnected ')
    // this.$g.showSnackbar('disconnected', 2000, 'error')
  }
}
/*
function addImageFromPc(payload) {
  return new Promise((resolve, reject) => {
    // payload = { file, questionnaireIndex, maxSize }
    // ctx.clog('i addImageFromPc payload.file = ' + payload.file)
    ctx.clog('idb addImageFromPc payload ', payload)
    const file = payload.file
    // const questionnaireIndex = payload.questionnaireIndex
    // const cb = payload.cb
    const maxSize = payload.maxSize
    // maxSize = 500000
    // if (!file.type.match('image')) return
    if (!file.type.match('image')) {
      ctx.clog('idb file.type = ', file.type)
    }
    const media = {}
    const typeAndExt = splitTypeAndExt(file.type)
    if (typeAndExt) {
      media.type = typeAndExt.type
      media.ext = typeAndExt.ext
    } else {
      media.type = 'image'
      media.ext = ctx.getExtensionFromFilename(file.name)
    }
    // gadavamocmot failis sidide
    // dasalagebelia 3024x4032 achvena 2,99mb
    if (maxSize && file.size > maxSize) {
      const reader = new FileReader()
      reader.onloadend = (e) => {
        const img = new Image()
        img.src = reader.result
        img.onload = () => {
          let width = img.naturalWidth || img.width
          let height = img.naturalHeight || img.height
          ctx.clog('idb img.{ width, height } = ', { width, height })
          const origSize = (img.src.length * 3) / 4

          const koeff = +Math.sqrt(origSize / maxSize).toFixed(2) // koeff >= 1
          // koeff = 1
          ctx.clog('idb koeff ', koeff)
          width = width / koeff
          height = height / koeff
          const canvas = document.createElement('canvas')
          canvas.width = width
          canvas.height = height
          canvas.getContext('2d').drawImage(img, 0, 0, width, height)
          canvas.toBlob((blob) => {
            media.blob = blob
            media.width = width
            media.height = height
            media.size = (img.src.length * 3) / 4
            ctx.clog('idb media = ', JSON.stringify(media))
            createPayloadAndResolve(media, payload, resolve)
          })
        }
        // media.src = img.src
      }
      reader.readAsDataURL(file)
    } else {
      media.blob = file
      media.size = file.size
      createPayloadAndResolve(media, payload, resolve)
    }
  })
}
function addVideoFromPc(payload) {
  return new Promise((resolve, reject) => {
    // payload = { file, questionnaireIndex, maxSize }
    ctx.clog('idb addVideoFromPc payload.file = ' + payload.file)
    const file = payload.file
    // const maxSize = payload.maxSize
    // if (!file.type.match('video')) return
    if (!file.type.match('video')) {
      ctx.clog('idb file.type = ', file.type)
    }
    const media = {}
    const typeAnExt = splitTypeAndExt(file.type)
    if (typeAnExt) {
      media.type = typeAnExt.type
      media.ext = typeAnExt.ext
    } else {
      media.type = 'video'
      media.ext = ctx.getExtensionFromFilename(file.name)
    }
    media.size = file.size
    media.blob = file
    createPayloadAndResolve(media, payload, resolve)
  })
}
function addAudioFromPc(payload) {
  return new Promise((resolve, reject) => {
    // payload = { file, questionnaireIndex, maxSize }
    ctx.clog('idb addAudioFromPc payload.file = ' + payload.file)
    const file = payload.file
    if (!file.type.match('audio')) {
      ctx.clog('idb file.type = ', file.type)
    }
    const media = {}
    const typeAnExt = splitTypeAndExt(file.type)
    if (typeAnExt) {
      media.type = typeAnExt.type
      media.ext = typeAnExt.ext
    } else {
      media.type = 'audio'
      media.ext = ctx.getExtensionFromFilename(file.name)
    }
    media.size = file.size
    media.blob = file
    createPayloadAndResolve(media, payload, resolve)
  })
}
aq vcdilobdit, rom failis zomebi sachiromde dagvekvana
if (maxSize && file.size > maxSize) {
      const reader = new FileReader()
      reader.onloadend = (e) => {
        const img = new Image()
        img.src = reader.result
        img.onload = () => {
          let width = img.naturalWidth || img.width
          let height = img.naturalHeight || img.height
          ctx.clog('idb img.{ width, height } = ', { width, height })
          const origSize = (img.src.length * 3) / 4

          const koeff = +Math.sqrt(origSize / maxSize).toFixed(2) // koeff >= 1
          // koeff = 1
          ctx.clog('idb koeff ', koeff)
          width = width / koeff
          height = height / koeff
          const canvas = document.createElement('canvas')
          canvas.width = width
          canvas.height = height
          canvas.getContext('2d').drawImage(img, 0, 0, width, height)
          canvas.toBlob((blob) => {
            media.blob = blob
            media.width = width
            media.height = height
            media.size = (img.src.length * 3) / 4
            ctx.clog('idb media = ', JSON.stringify(media))
            createPayloadAndResolve(media, payload, resolve)
          })
        }
        // media.src = img.src
      }
      reader.readAsDataURL(file)
    } else {
      media.blob = file
      media.size = file.size
      createPayloadAndResolve(media, payload, resolve)
    }


// vadarebt ertmanets mongoshi atvirtul media-failebs da idb-shi arsebulebs
function compareQMedias2(mongoQuestionnaireValues, idbMedias) {
  let mongoValues = {}
  if (mongoQuestionnaireValues) {
    mongoValues = mongoQuestionnaireValues.values
  }
  const resp = {
    toUpload: [],
    toRemove: []
  }
  const allMongoRelPathes = []
  const allMongoFilenames = []
  const allIdbFilenames = []
  Object.keys(mongoValues).forEach(key => {
    const itemValue = mongoValues[key]
    if (itemValue.attachments) {
      if (itemValue.attachments.medias) {
        allMongoRelPathes.push(...itemValue.attachments.medias)
      }
      if (itemValue.attachments.files) {
        allMongoRelPathes.push(...itemValue.attachments.files)
      }
    }
  })
  for (const relPath of allMongoRelPathes) {
    allMongoFilenames.push(ctx.getFilenamefromPath(relPath))
  }
  for (const idbMedia of idbMedias || []) {
    const filename = createMongoFilenameForQuestionnaireIdbMedia(idbMedia)
    allIdbFilenames.push(filename)
  }
  // რომელიც არ არის ატვირთული და არის ასატვირთებში, ატვირთავს
  // ანუ არის allIdbFilenames-ში და არ არის allMongoFilenames-ში
  const toUpload = allIdbFilenames.filter(x => !allMongoFilenames.includes(x))

  // რომელიც ატვირთულია და აღარ არის ასატვირთებში, სერვერზე წასაშლელების სიაში ჩადებს
  // ანუ არის allMongoFilenames-ში და არ არის allIdbFilenames-ში
  const toRemove = allMongoRelPathes.filter(x => !allIdbFilenames.includes(ctx.getFilenamefromPath(x)))
  resp.toUpload.push(...toUpload)
  resp.toRemove.push(...toRemove)
  return resp
}

function createTsArrayFromIdbMedias2(uplRemMedias, idbMedias) {
  const tsArray = []
  if (idbMedias) {
    for (const idbMedia of idbMedias) {
      const filename = createMongoFilenameForQuestionnaireIdbMedia(idbMedia)
      if (uplRemMedias.toUpload.includes(filename)) {
        tsArray.push(idbMedia.media.tsList.notSent)
      }
    }
  }
  ctx.chatEBus.$emit(ctx.enums.events.questionnaireMediaStartLoading, tsArray)
}

ანკეტის ატცირთვის წინ ჯერ მონგოდან გამოიძახებს არსებულ QValues, შეადარებს ასატვირთს და გააკეთებს შედარებას: 
ფაილებს, რომელიც არ არის ატვირთული და არის ასატვირთებში, ატვირთავს
რომელიც ატვირთულია და აღარ არის ასატვირთებში, სერვერზე წასაშლელების სიაში ჩადებს


 export function uploadQuestionnaireToServer2() {
  const card = ctx.store.getters.card
  // getQuestionnaireValuesByCardId-is dros targmani aq ar gvWirdeba da amitom specialurad ar vazlevt locales, rom gadatargmna ar moucios servers
  const requestObj = { cardId: card.id }
  ctx
    .getQuestionnaireValuesByCardId(requestObj)
    .then(mongoQuestionnaireValues => {
      validateQuestionnaire().then(validateErrorGroupsArr => {
        // im group-ebis sia, sadac error-ebia
        ctx.clog('iq validateErrorGroupsArr = ', validateErrorGroupsArr)
        const payload = { cardId: card.id, questionnaireIndex: 'all' }
        ctx.clog('iq uploadQuestionnaireToServer mongoQuestionnaireValues = ', mongoQuestionnaireValues)
        ctx.clog('iq uploadQuestionnaireToServer payload', payload)

        ctx.readMedias(payload).then(idbMedias => {
          ctx.clog('iq getAllMediasForQuestionnaire idbMedias = ', idbMedias)
          // gamovtvalot asatvirti da casashleli failebis sia
          const uploadOrRemoveMedias = compareQMedias2(mongoQuestionnaireValues, idbMedias)
          createTsArrayFromIdbMedias2(uploadOrRemoveMedias, idbMedias)
          if (!uploadOrRemoveMedias.toUpload.length) {
            // mashin asatvirti failebi ar aris, questionnaire aris medias gareshe
            const questionnaireValues = getCardIdQuestionnaireValues()
            questionnaireValues.toRemove = uploadOrRemoveMedias.toRemove
            adjustQuestionnaireValues(questionnaireValues)
            ctx
              .uploadQuestionnaire(questionnaireValues)
              .then(questionnaireId => {
                handleUploadQuestionnaireSuccess(questionnaireId)
              })
              .catch(err => {
                handleUploadQuestionnaireError(err)
              })
            return
          }
          // davtvalot failebis saerto size
          let attachSize = 0
          // mediasAmount = 0
          for (const idbMedia of idbMedias || []) {
            attachSize += idbMedia.media.size
          }
          const user = ctx.store.getters.user
          // console.log('i user = ', user)
          // davamrgvalot qvevitken
          const attachSizeRounded = Math.floor(attachSize / 1000000)
          const mediaMaxSize = user.mediaMaxSize.tester
          // shevamocmot failebis saerto zomis gadameteba
          if (attachSizeRounded > mediaMaxSize) {
            ctx.chatEBus.$emit(ctx.enums.events.mediaMaxSizeExeeded, { attachSizeRounded, mediaMaxSize })
            return
          }
          ctx.store.commit('setQuestionnaireAttachSize', { cardId: ctx.store.getters.card.id, attachSize })
          // chavcert rigrigobit questionnaireValues.values['1.1',...].attachments-shi
          for (const idbMedia of idbMedias) {
            const filename = createMongoFilenameForQuestionnaireIdbMedia(idbMedia)
            // tu asatvirtebis siashi ar aris continue
            if (!uploadOrRemoveMedias.toUpload.includes(filename)) {
              continue
            }
            // for (const idbMedia of idbMedias) {
            ctx.clog('iq uploadQuestionnaireToServer idbMedia ', idbMedia)
            // const questionnaireIndex = idbMedia.cardIdAndQuestionnaireIndex.split('_')[1]
            const questionnaireIndex = idbMedia.questionnaireIndex
            // const lastBlob = i === idbMedias.length - 1
            const media = idbMedia.media
            const message = {
              // cardId: idbMedia.cardId,
              data: { blob: idbMedia.media.blob },
              meta: {
                // idbId: idbMedia.ts,
                mediasAmount: uploadOrRemoveMedias.toUpload.length,
                toRemove: uploadOrRemoveMedias.toRemove,
                size: media.size,
                intent: ctx.enums.intents.uploadQuestionnaire,
                collName: ctx.enums.collnames.questionnaires,
                caller: ctx.enums.caller.questionnaire,
                questionnaireIndex,
                ext: media.ext,
                type: media.type,
                tsList: { [ctx.enums.status.notSent]: media.tsList[ctx.enums.status.notSent] }
              }
            }
            ctx.adjustMessage(message)
            ctx
              .uploadMedia({ message })
              .then(resultData => {
                handleUploadQMediaSuccess(resultData)
              })
              .catch(err => {
                handleUploadMediaError(err)
              })
          }
        })
      })
    })
    .catch(err => {
      // console.log('am catch e: ', err)
      // shecdoma amas exeba -> getQuestionnaireValuesByCardId
      ctx.showSnackbar(err, 2000, 'red', ctx.enums.caller.questionnaire)
    })
} */

/* if (
      this.qIndicatorDefaultData &&
      this.qIndicatorDefaultData.basic.allFields > 0 &&
      this.qIndicatorDefaultData.advanced.allFields > 0
    ) {
      return this.qIndicatorDefaultData
    } */
/* const questionnaireBasic = this.store.getters.questionnaire({ locale: 'de', card: { level: 'basic' } })
    const questionnaireAdvanced = this.store.getters.questionnaire({ locale: 'de', card: { level: 'advanced' } })
    const data = { basic: {}, advanced: {} }

    // console.log('qs questionnaire = ', questionnaire)
    if (!questionnaireBasic || !questionnaireBasic.items) {
      data.basic.mustFilledFields = 0
      data.basic.allFields = 0
    } else {
      let mustFilledFields = 0
      let allFields = 0
      const items = questionnaireBasic.items
      // console.log('questionnaireBasic = ', questionnaireBasic)
      for (const item of items) {
        for (const it of item.children) {
          allFields++
          if (it.mandatory) {
            mustFilledFields++
          }
        }
      }
      data.basic.mustFilledFields = mustFilledFields
      data.basic.allFields = allFields
    }
    if (!questionnaireAdvanced || !questionnaireAdvanced.items) {
      data.advanced.mustFilledFields = 0
      data.advanced.allFields = 0
    } else {
      let mustFilledFields = 0
      let allFields = 0
      const items = questionnaireAdvanced.items
      for (const item of items) {
        for (const it of item.children) {
          allFields++
          if (it.mandatory) {
            mustFilledFields++
          }
        }
      }
      data.advanced.mustFilledFields = mustFilledFields
      data.advanced.allFields = allFields
    } */
/* removeSentPendingMessage(message) {
      // vnaxot es gaugzavneli message-tu iko
      const pendingMessageList = this.$g.store.getters.pendingMessageLists[this.$g.store.getters.card.id]
      for (const pendingMessage of pendingMessageList) {
        if (pendingMessage.ts === message.ts) {
          this.$g.store.commit('removePendingMessageFromList', message)
        }
      }
    }, */
/* socket.off("fileFromBack")
      // ss(socket).on("fileFromBack", (stream, message, callback) => {
      socket.on("fileFromBack", (message, callback) => {
        // message asetia: relpath chaceris shemdeg daemata
        // {"meta":{"type":"file","collName":"messages","tsFormat":"DD.MM.YY HH:mm"},
        //  "data":{"author":"me","text":"ert", "senderLang":"de","username":"misha","chatetName":'Herr Fisher', avatarSrc:'http..some.png'},
        //  "meta":{relpath: '/senderId/dateDay/date.jpg', "sender":"5d4b41f38b7a432c541ab3b4","msgId":"afgadfgadfg","receiver":"admin","status":"notSent","tsArr":[1565264823436]},
        //  "admin":false}
        
        this.handleReceivedMessage(message)
        if (callback) {
          callback(message.meta)
        }        
      }) */
/* 
   adjustMessage(message) {
      this.$g.clog('message ', JSON.stringify(message))
      // this.$g.clog('this.$g.socket=', this.$g.socket)
      const user = this.$g.store.getters.user
      this.$g.clog('user ', JSON.stringify(user))
      if (!this.checkReceiver()) {
        return
      }
      message.meta.collName = 'messages'
      message.meta.caller = caller.chat
      message.meta.status = status.notSent
      // message.meta.token = user.tokenInfo.token,
      message.meta.sender = {
        id: user.id,
        lang: user.lang,
        nickname: user.nickname
      }
      message.meta.receiver = this.receiver
      // textData = {senderLang:'de', receiverLang: 'ka', text: 'Haus Tür', translatedText:'სახლი კარი' }
      if (message.data.textData && message.data.textData.text) {
        message.data.textData.senderLang = user.lang
        message.data.textData.receiverLang = this.receiver.lang
      }
      message.cardId = this.$g.store.getters.card.id
      message.senderId = user.id
      message.receiverId = this.receiver.id
      if (process.env.tokenoff) {
        message.meta.tokenoff = true
        this.$g.clog('message.meta ', message.meta)
      }
    }, 

createBlobFromBinaryString(binaryString){
      var i, l, array;
        // d = this.result;
        l = binaryString.length;
        array = new Uint8Array(l);
        for (var i = 0; i < l; i++){
            array[i] = binaryString.charCodeAt(i);
        }
        var b = new Blob([array], {type: 'application/octet-stream'})
        return b
    }, 

es ar cavshalo, damchirdeba roca file download gaxdeba sachiro
// mogvaqvs file to downloads daklikavs
        var binaryString = ''
        stream.on('data', data => {
          this.$g.clog('data')
          for (var i = 0; i < data.length; i++) {
            binaryString += String.fromCharCode(data[i])
          }
        })
        stream.on('error', error => {
          this.$g.clog('Error found:', error)
        })
        stream.on('close', () => {
          this.$g.clog('close')
        })
        stream.on('drain', (e) => {
          this.$g.clog('drain')
        })
        stream.on('end', data => {
          this.$g.clog('end')
          // document.getElementById('img').setAttribute("src", "data:image/png;base64," + window.btoa(binaryString))
          // $("#img").attr("src","data:image/png;base64,"+window.btoa(binaryString))
          binaryString = ''
          // vugzavnit dadasturebas rom chamoitvirta file
          if (callback) {
            callback(message.meta)
          }
        }) */
/*
function setPendingMediaSize2(payload) {
  // payload = { size: media.size, add: false, cardId, isChat: true }
  // const ctx = payload.ctx
  const card = ctx.store.getters.card
  const add = payload.add
  let size = payload.size / 1000000
  size = Number.parseFloat(size).toFixed(2)
  ctx.clog('i size ', size)
  if (payload.isChat) {
    const indicator = { ...card.indicator.chat }
    ctx.store.commit('setCardIndicatorChat', countNewSize({ indicator, add, card, size })) // pendingMediaSize}
  } else {
    const indicator = { ...card.indicator.questionnaire }
    ctx.store.commit('setCardIndicatorQuestionnaire', countNewSize({ indicator, add, card, size })) // pendingMediaSize}
  }
  // ctx.store.commit('setPendingMediaSize', newSize) // pendingMediaSize}
}
function countNewSize({ indicator, add, card, size }) {
  const pendingSize = indicator.pendingMediaSize
  let newSize = add ? +pendingSize + +size : +pendingSize - +size
  newSize = Math.min(card.mediaMaxSize, newSize)
  if (newSize < 0) {
    newSize = 0
  }
  indicator.pendingMediaSize = newSize
  ctx.clog('i indicator = ', indicator)
  return indicator
}

handleSuccessPendingMessageAxios(message, meta) {
    chatEBus.$emit(events.handleReceivedPendingMessage, { message, meta })
  }
  handleErrorPendingMessageAxios() {}
  setIntervalPendingMessagesTimer() {
    this.pendingIntervalTimer = setInterval(this.sendPendingMessages(), this.pendingMessagesInterval)
  }
  sendPendingMessages() {
    const pendingMessageLists = this.store.getters.pendingMessageLists
    const pendingMessagesArr = []
    Object.keys(pendingMessageLists).forEach(key => {
      const value = pendingMessageLists[key]
      pendingMessagesArr.push(...value)
    })
    for (const pendingMessage of pendingMessagesArr) {
      sendPendingMessage(pendingMessage, this.handleSuccessPendingMessageAxios, this.handleErrorPendingMessageAxios)
    }
  }
  clearIntervalPendingMessagesTimer() {
    clearInterval(this.pendingIntervalTimer)
  }

  


*/
/*
// quiestionnaire-is titoeuli mediafile-is atvirtvisas
// ar vxmarobt
 function handleUploadedFile(qValues, message) {
  Object.keys(qValues).forEach(key => {
    // indexValue = {answer: '0', comment: 'tratra', attachments:[...]}
    const indexValue = qValues[key]
    indexValue.attachments = []
    // tu question aris da ara group, mashin key certils sheicavs, mag 1.1
    if (key.includes('.')) {
      // key='1.2'
      // cardIdAndQuestionnaireIndex -> 123_1.1
      const questionnaireIndex = message.questionnaireIndex
      // questionnaireIndex = '1.2'
      if (questionnaireIndex === key) {
        indexValue.attachments.push(message.relPath)
      }
    }
  })
}


// ar vxmarobt
export function getQValuesArrFromMongoByCardIdArr2(cardIdArr, callback) {
  ctx.clog('iq data cardIdArr = ', cardIdArr)
  // vekitxebi chems servers, targmanebi ar gvchirdeba
  const requestObj = { cardIdArr }
  ctx
    .getQuestionnaireValuesByCardId(requestObj)
    .then(questionnaireValuesArr => {
      ctx.clog('iq data success questionnaireValuesArr = ', questionnaireValuesArr)
      if (questionnaireValuesArr) {
        const stands = {}
        for (const questionnaireValues of questionnaireValuesArr) {
          const cardId = questionnaireValues.cardId
          stands[cardId] = getQuestionnaireValidateStands(null, questionnaireValues)
        }
        if (callback) {
          callback(stands)
        }
      }
    })
    .catch(err => {
      ctx.showSnackbar(err, 2000, 'red', ctx.enums.caller.questionnaire)
      if (callback) {
        callback(null, err)
      }
    })
}

// ar vxmarobt
  validateQuestionnaire2() {
    return new Promise((resolve, reject) => {
      this.store.commit('setValidateQuestionnaire', false)
      this.store.commit('setValidateQuestionnaire', true)
      setTimeout(() => {
        const locale = this.store.getters.locale
        const card = this.store.getters.card
        const questionnaire = this.store.getters.questionnaire({ locale, card })
        for (const groupItem of questionnaire.items) {
          for (const item of groupItem.children) {
            // this.clog('t', JSON.stringify(t))
            // this.clog('i item ' + JSON.stringify(item))
            if (item.validateError) {
              resolve(false)
            }
          }
        }
        resolve(true)
      }, 100)
    })
  }
  // ar vxmarobt
  openOrCloseAllGroups(open) {
    const locale = this.store.getters.locale
    const card = this.store.getters.card
    const questionnaire = this.store.getters.questionnaire({ locale, card })
    const allGroupIndexesArray = []
    for (const groupItem of questionnaire.items) {
      allGroupIndexesArray.push(groupItem.index)
    }
    const payload = {
      cardId: card.id,
      open,
      allGroupIndexesArray,
      locale
    }
    this.store.commit('openOrCloseAllGroups', payload)
  }
  // ar vxmarobt
  openOrCloseGroupItem(open, index) {
    const locale = this.store.getters.locale
    const card = this.store.getters.card
    const questionnaire = this.store.getters.questionnaire({ locale, card })
    let groupItemExists = false
    for (const groupItem of questionnaire.items) {
      if (groupItem.index === index) {
        groupItemExists = true
      }
    }
    if (groupItemExists) {
      const payload = {
        cardId: card.id,
        open,
        index,
        locale
      }
      this.store.commit('setQuestionnaireGroupOpen', payload)
    }
  } */
