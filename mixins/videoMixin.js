export const videoMixin = {
  /* video-> romlitac vickebt, recorder -> romelshic vcert
    canvas -> screenshot-istvis
    captureSize -> screenshot-is zomebistvis
    audioOptions -> audio mockobilobebi
    videoOptions -> video mockobilobebi
    media -> am obieqtshi vdebt kvelafers: image, video, audio, download src-eebs
    mediaStream -> koveli axali sheqmnili mediaStream
    mediaStreams -> aq vinaxavt mediaStream-ebs, rom bolos mati stop shevzlot
    streamStarted -> play-s sakontrolod

    constraints-> { video: true, audio: false } an ufro rtulic
    mag. video-stvis
  {
    width: {min: 640, ideal: 1280},
    height: {min: 480, ideal: 720},
    advanced: [
      {width: 1920, height: 1280},
      {aspectRatio: 1.333}
    ]
  }
  */
  data() {
    return {
      video: null,
      recorder: null,
      canvas: null,
      voice: JSON.parse(JSON.stringify(this.$g.store.getters.constraints)).audio,
      frontCamera: false,
      // constraints: null,
      captureSize: null,
      media: null,
      audioSelectEl: null,
      videoSelectEl: null,
      audioDevices: [],
      videoDevices: [],
      selectedAudio: null,
      selectedVideo: null,
      // streamStarted: null,
      mediaStreams: [],
      mediaStream: null
    }
  },
  methods: {
    wait(delayInMS) {
      return new Promise((resolve) => setTimeout(resolve, delayInMS))
    },
    stopStream() {
      for (const mediaStream of this.mediaStreams) {
        mediaStream.getTracks().forEach((track) => {
          track.stop()
        })
      }
      this.video.srcObject = null
      this.mediaStream = null
      // this.streamStarted = false
    },
    startVideo(caller) {
      // this.constraints = constraints
      this.$g.clog('vm navigator ', navigator)
      if (!navigator.mediaDevices) {
        // window.alert('No media devices was found!')
        let text = this.$g.t('qchat.snackbar.no_media_devices')
        if (this.$g.gdata.isLocalhost) {
          text = this.$g.t('qchat.snackbar.no_media_devices_on_localhost')
        }
        this.$g.showSnackbar(text, 4000, 'error', caller)
        // this.$g.clog('vm No media devices was found! ')
        this.cancel() // <-videoDialog-shia
        return
      }
      this.$g.clog('vm startVideo ')
      this.audioSelectEl.onchange = () => {
        const constraints = JSON.parse(JSON.stringify(this.$g.store.getters.constraints))
        // const constraints = this.$g.store.getters.constraints
        constraints.audio = {
          deviceId: {
            exact: this.selectedAudio.deviceId
          }
        }
        this.$g.store.commit('setConstraints', constraints)
        this.startStream(true)
      }
      this.videoSelectEl.onchange = () => {
        const constraints = JSON.parse(JSON.stringify(this.$g.store.getters.constraints))
        // const constraints = this.$g.store.getters.constraints
        constraints.video = {
          deviceId: {
            exact: this.selectedVideo.deviceId
          }
        }
        this.$g.store.commit('setConstraints', constraints)
        this.startStream(true)
      }
      navigator.mediaDevices
        .enumerateDevices()
        .then(this.addDevices)
        .then(() => {
          this.$g.clog('vm this.constraints = ', JSON.stringify(this.$g.store.getters.constraints))
          if (this.hasBackVideoDevice()) {
            const constraints = JSON.parse(JSON.stringify(this.$g.store.getters.constraints))
            constraints.video = { facingMode: { exact: 'environment' } }
            this.$g.store.commit('setConstraints', constraints)
          }
        })
        .then(this.startStream)
        // .then(() => ctx.dialog = true)
        .catch(this.handleError)
    },
    addDevices(deviceInfos) {
      this.$g.clog('vm addDevices')
      this.audioDevices.length = 0
      this.videoDevices.length = 0
      for (let i = 0; i !== deviceInfos.length; ++i) {
        const deviceInfo = deviceInfos[i]
        // deviceInfo.deviceId = i
        /* 
        {deviceId: "default", groupId: "0457eda5a0ec7fe4cf2e7dca72c16fa3b7c9ea7a4f6b561bec47aa08f98c3844",
         kind: "audiooutput", label: "Standardeinstellung - AMD HDMI Output (AMD High Definition Audio Device)"}
         deviceId: "256452490facb5ab3a585189b823bf328ed9fb4351d1dac99f19eb05325874a4", 
         groupId: "09cc563ba7f244aaf3467b8521b084777d6bf06fea09ed35f0679ea071767fdd", 
         kind: "videoinput", label: "MEDION MD86511 (04f2:a14c)"}
        */
        /* const option = document.createElement('option')
        option.value = deviceInfo.deviceId */
        if (this.audioSelectEl && deviceInfo.kind === 'audioinput') {
          this.$g.clog('vm deviceInfo audioinput = ', deviceInfo)
          // option.text = deviceInfo.label || 'microphone ' + (this.audioSelectEl.length + 1)
          // this.audioSelectEl.appendChild(option)
          this.audioDevices.push(deviceInfo)
        } else if (this.videoSelectEl && deviceInfo.kind === 'videoinput') {
          this.$g.clog('vm deviceInfo videoinput = ', deviceInfo)
          // option.text = deviceInfo.label || 'camera ' + (this.videoSelectEl.length + 1)
          // this.videoSelectEl.appendChild(option)
          this.videoDevices.push(deviceInfo)
        } else {
          this.$g.clog('vm Found another kind of device: ', deviceInfo)
        }
      }
    },
    startStream(play) {
      this.$g.clog('vm startStream play = ', play)
      if ('mediaDevices' in navigator && navigator.mediaDevices.getUserMedia) {
        navigator.mediaDevices
          .getUserMedia(this.$g.store.getters.constraints)
          .then((stream) => {
            this.$g.clog('vm startStream then ')
            this.video.srcObject = stream
            this.video.captureStream = this.video.captureStream || this.video.mozCaptureStream
            this.mediaStream = stream
            this.mediaStreams.push(stream)
          })
          .then(() => play && this.play())
          .catch(this.handleError)
      } else if (this.$g.gdata.isLocalhost) {
        // console.log('vm this.$g.gdata.isLocalhost = ', this.$g.gdata.isLocalhost)
        this.handleError(this.$g.t('qchat.snackbar.no_media_devices_on_localhost'))
      } else {
        // console.log('vm2 this.$g.gdata.isLocalhost = ', this.$g.gdata.isLocalhost)
        this.handleError(this.$g.t('qchat.snackbar.no_media_devices'))
      }
    },
    handleError(err) {
      this.$g.clog('vm err: ', err)
    },
    pauseStream() {
      this.video.pause()
    },
    async startRecordingProcess(stream, lengthInMS) {
      this.$g.clog('vm stream = ', stream)
      let timeout
      this.recorder = new MediaRecorder(stream)
      const data = []
      this.recorder.ondataavailable = (event) => {
        // event.data -> es erti didi blob obieqtia
        if (event.data) {
          data.push(event.data)
          this.$g.clog('vm data.push ', event.data)
          // cavshalot maqsimaluri drois brzaneba rom gvaqvs micemuli
          if (timeout) {
            this.$g.clog('vm clearTimeout ')
            clearTimeout(timeout)
            timeout = null
          }
        }
      }
      this.recorder.start()
      this.$g.clog(this.recorder.state + ' for ' + lengthInMS / 1000 + ' seconds...')
      const stopped = new Promise((resolve, reject) => {
        this.recorder.onstop = resolve
        this.recorder.onerror = (event) => reject(event.name)
      })
      const recorded = this.wait(lengthInMS)
        .then(() => {
          this.stopRecorder()
          return Promise.resolve(recorded).then(() => data)
        })
        .catch((err) => {
          this.$g.clog('vm startRecordingProcess err ', err)
        })
      // Promise.race nishnavs romelic moascrebs
      await Promise.resolve(stopped)
      return data
    },
    stopRecorder() {
      // this.$g.clog('this.recorder.state ', this.recorder.state)
      // recorder.stop() fires recorder.ondataavailable -> data -> callback addImage
      this.recorder && this.recorder.state === 'recording' && this.recorder.stop()
    },
    startRecording(recordingData) {
      const addMedia = recordingData.addMedia
      const media = this.media
      // const downloadButton = recordingData.downloadButton
      const videoRecordingMaxTimeMS = recordingData.videoRecordingMaxTimeMS
      this.startRecordingProcess(this.video.captureStream(), videoRecordingMaxTimeMS)
        .then((recordedChunks) => {
          this.$g.clog('vm recordedChunks ', recordedChunks)
          const recordedBlob = new Blob(recordedChunks, { type: 'video/webm' })
          if (!recordedBlob) {
            throw new Error('Recording error')
          }
          this.$g.clog('vm recordedBlob.size ', recordedBlob.size)
          // media.src = URL.createObjectURL(recordedBlob)
          media.blob = recordedBlob
          media.isVideo = true
          /* media.tsList[status.notSent] = Date.now()
          media.size = recordedBlob.size
          media.type = recordedBlob.type */
          // downloadButton.href = media.src
          // downloadButton.download = 'RecordedVideo.webm'
          addMedia(media)
          this.$g.clog('vm Successfully recorded ' + recordedBlob.size + ' bytes of ' + recordedBlob.type + ' media.')
        })
        .catch((err) => {
          this.$g.clog('vm err: ', err)
        })
    },
    snapshot() {
      return new Promise((resolve, reject) => {
        this.canvas.width = this.video.videoWidth
        this.canvas.height = this.video.videoHeight
        this.canvas.getContext('2d').drawImage(this.video, 0, 0)
        this.canvas.toBlob((blob) => {
          this.$g.clog('vm blob', blob)
          const media = {
            blob,
            isPhoto: true
          }
          this.stopStream()
          resolve(media)
        })
      })
    },
    play() {
      this.$g.clog('vm this.mediaStream ', this.mediaStream)
      if (this.mediaStream) {
        this.video.play()
      } else {
        this.startStream()
      }
    },
    hasBackVideoDevice() {
      for (const deviceInfo of this.videoDevices) {
        const label = deviceInfo.label
        if (label.includes('back')) {
          this.$g.clog('vm label ', label)
          return true
        }
      }
      this.$g.clog('vm hasBackVideoDevice ', false)
      return false
    },
    // video-s gadartva front <- -> back
    switchVideoTrack() {
      this.$g.clog('vm this.$g.store.getters.constraints ', JSON.stringify(this.$g.store.getters.constraints))
      let videoMode
      if (this.frontCamera) {
        // videoMode = { facingMode: 'user' }
        // videoMode = { facingMode: { exact: 'user' } }
        videoMode = true
      } else {
        // back camera
        videoMode = { facingMode: { exact: 'environment' } }
      }
      const constraints = JSON.parse(JSON.stringify(this.$g.store.getters.constraints))
      constraints.video = videoMode
      this.$g.store.commit('setConstraints', constraints)
      this.restartStream()
    },
    setVideoTrack(deviceInfo) {
      this.$g.clog('vm deviceInfo ', JSON.stringify(deviceInfo))
      // this.$g.store.getters.constraints.video = { deviceId: deviceInfo.deviceId }
      // const constraints = this.$g.store.getters.constraints
      const constraints = JSON.parse(JSON.stringify(this.$g.store.getters.constraints))
      this.$g.clog('vm constraints ', JSON.stringify(constraints))
      if (!deviceInfo.deviceId) {
        return
      }
      constraints.video = { deviceId: { exact: deviceInfo.deviceId } }
      this.$g.store.commit('setConstraints', constraints)
      if (!deviceInfo.label) {
        this.frontCamera = !deviceInfo.label.includes('back')
      }
      this.$g.clog('vm this.constraints ', JSON.stringify(this.$g.store.getters.constraints))
      this.restartStream()
    },
    restartStream() {
      this.video.srcObject = null
      // this.streamStarted = false
      this.mediaStream = null
      // this.video.captureStream = null
      this.stopStream()
      this.play()
    }
  }
}
