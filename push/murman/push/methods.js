import ka from '@/locales/ka.json'
import de from '@/locales/de.json'
import en from '@/locales/en.json'
import ru from '@/locales/ru.json'
import { getRoute, setFingerPrint } from '~/helpers/methods'

const locales = { ka: ka.push, de: de.push, en: en.push, ru: ru.push }

const pushServerPublicKey = 'BJyem4fs7_L-GfzSUAOYs8IjStk-D8valCWNv_N2XgOxmJriqWADaI11t3L9qVCyKmMugc7E1jCsspgkTtkxaOg'

let client = null

const createClient = function (store, serviceWorker) {
  serviceWorker.addEventListener('message', (event) => {
    const locale = store.state.user.root.locale
    const texts = locales[locale]
    const route = getRoute(event.data)
    const viewUrl = route.path + '?' + Object.keys(route.query)[0] + '=' + Object.values(route.query)[0]
    client.postMessage({ locale, viewUrl, texts })
  })
  serviceWorker.ready
    .then((registration) => {
      const locale = store.state.user.root.locale
      const texts = locales[locale]
      if (registration.active) {
        client = registration.active
        client.postMessage({ locale, texts })
      }
    })
    .catch((err) => {
      console.log('sw serviceWorker.ready err ', err)
    })
}

export function isPushNotificationSupported() {
  return 'serviceWorker' in navigator && 'PushManager' in window
}

export function initializePushNotifications() {
  return Notification.requestPermission(function (result) {
    return result
  })
}

export async function unregisterServiceWorker() {
  await navigator.serviceWorker
    .getRegistrations()
    .then(async function (registrations) {
      for (const registration of registrations) {
        await registration.unregister()
        console.log('>->-> - unregistered!')
      }
    })
    .catch((err) => {
      console.log('sw unregisterServiceWorker err ', err)
    })
}

export async function registerServiceWorker(store) {
  const swRegistration = await navigator.serviceWorker.register('/sw2.js')
  swRegistration.addEventListener('updatefound', () => {
    // If updatefound is fired, it means that there's a new service worker being installed.
    const installingWorker = swRegistration.installing
    console.log('pn A new service worker is being installed:', installingWorker)
    // You can listen for changes to the installing service worker's
    // state via installingWorker.onstatechange
  })
  createClient(store, navigator.serviceWorker)
}

export function createNotificationSubscription() {
  return navigator.serviceWorker.ready.then(function (serviceWorker) {
    return serviceWorker.pushManager
      .subscribe({
        userVisibleOnly: true,
        applicationServerKey: pushServerPublicKey
      })
      .then(function (subscription) {
        return subscription
      })
      .catch((err) => {
        console.log('sw createNotificationSubscription err ', err)
      })
  })
}

export async function sendNotificationSubscription(axios, subscription, userId) {
  const params = { subscription, userId, fingerPrint: await setFingerPrint() }
  return await axios.post('/api/push/subscription', params)
}

export async function fullServiceWorker(store, axios, userId) {
  await registerServiceWorker(store)
  const subscription = await createNotificationSubscription()
  sendNotificationSubscription(axios, subscription, userId)
}
