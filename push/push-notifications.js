import axios from 'axios'
const pushServerPublicKey = 'BJyem4fs7_L-GfzSUAOYs8IjStk-D8valCWNv_N2XgOxmJriqWADaI11t3L9qVCyKmMugc7E1jCsspgkTtkxaOg'
// const pushUrl = 'http://192.168.178.20:4129'   http://localhost:3010/
const pushUrl = 'http://localhost:4129'

let ctx // ctx = $g da ara this
export function initContext(_ctx) {
  // console.log('PushNotifications initContext _ctx = ', _ctx)
  ctx = _ctx
}
const messageListener = (event) => {
  console.log('pn Got reply from service worker: ', event.data)
  // აქ არის პასუხი service worker-იდან და ეს პასუხი მიდის store-ში
  ctx.push(event.data)
}
/**
 * checks if Push notification and service workers are supported by your browser
 */
function isPushNotificationSupported() {
  const supported = 'serviceWorker' in navigator && 'PushManager' in window
  // console.log('pn serviceWorker in navigator = ', 'serviceWorker' in navigator)
  // console.log('pn PushManager in window = ', 'PushManager' in window)
  if (supported) {
    navigator.serviceWorker.removeEventListener('message', messageListener)
    navigator.serviceWorker.addEventListener('message', messageListener)
  } else {
    window.alert('Push not suppported!')
  }
  return supported
}

/**
 * asks user consent to receive push notifications and returns the response of the user, one of granted, default, denied
 */
function initializePushNotifications() {
  // request user grant to show notification
  return Notification.requestPermission((result) => {
    return result
  })
}

function updateUserConsent(userConsent) {
  if (userConsent !== 'granted') {
    window.alert(
      'You have denied push news! The application cannot be switched properly in this way. Please correct your selection.'
    )
    return false
  }
  return true
}

// using the registered service worker creates a push notification subscription and returns it
function createNotificationSubscription() {
  // wait for service worker installation to be ready, and then
  return navigator.serviceWorker.ready.then((serviceWorker) => {
    // subscribe and return the subscription
    return serviceWorker.pushManager
      .subscribe({
        userVisibleOnly: true,
        applicationServerKey: pushServerPublicKey
      })
      .then((subscription) => {
        console.log('User is subscribed.', subscription)
        return subscription
      })
  })
}
// returns the subscription if present or nothing
function getUserSubscription() {
  // wait for service worker installation to be ready, and then
  return navigator.serviceWorker.ready
    .then((serviceWorker) => {
      return serviceWorker.pushManager.getSubscription()
    })
    .then((pushSubscription) => {
      return pushSubscription
    })
}
// es satestod aris mxolod http://192.168.178.38:4130
function sendNotification(subscriptionId) {
  axios.get(pushUrl + `/subscription/${subscriptionId}`)
}
function postMessageToSW(message) {
  navigator.serviceWorker.ready.then((registration) => {
    // registration.active -> ServiceWorker   {scriptURL: "http://localhost:3010/sw2.js", state: "activated", onstatechange: null, onerror: null}
    if (registration.active) {
      // console.log('pn registration.active ', registration.active)
      registration.active.postMessage(message)
    }
  })
}

function initSW(subscriptionIdOld, userId, cb) {
  initializePushNotifications()
    .then((result) => {
      return updateUserConsent(result)
    })
    .then(() => {
      // register ServiceWorker
      // return navigator.serviceWorker.register('sw2.js', { scope: './' })
      return navigator.serviceWorker.register('sw2.js')
    })
    .catch((err) => {
      console.log('pn serviceWorker.register failed! ', err)
    })
    .then((swRegistration) => {
      console.log('pn swRegistration ', swRegistration)
      swRegistration.addEventListener('updatefound', () => {
        // If updatefound is fired, it means that there's a new service worker being installed.
        const installingWorker = swRegistration.installing
        console.log('pn A new service worker is being installed:', installingWorker)
        // You can listen for changes to the installing service worker's
        // state via installingWorker.onstatechange
      })
      navigator.serviceWorker.addEventListener('message', (event) => {
        // event is a MessageEvent object
        console.log('pn The service worker sent me a message event = ', event)
        // console.log(`pn The service worker sent me a message2: ${event.data}`)
        postMessageToSW({ data: event.data, url: 'test@web.de' })
        // navigator.serviceWorker.postMessage({ data: event.data, url: 'test@web.de' })
      })
      // ამით იგებს service worker sw2 ფაილი client-ის მონაცემებს და თან ვუგზავნთ push-ის სათარგმნ key-ებს
      /* 
      {
        "notif_from_orderer": "თქვენ გაქვთ შეტყობინება დამკვეთიდან - XXnickXX",
        "notif_from_tester": "თქვენ გაქვთ შეტყობინება ჩექერიდან - XXnickXX",
        "notif_time": "შეტყობინების მიღების დრო: "
      } */
      const translateKeys = {
        _you_have_note_from: 'თქვენ გაქვთ შეტყობინება დამკვეთიდან - XXnickXX',
        // notif_from_tester: 'თქვენ გაქვთ შეტყობინება ჩექერიდან - XXnickXX',
        _time_of_recieve_note: 'შეტყობინების მიღების დრო: '
      }
      // სინამდვილეში ეს უნდა იყოს -> translateKeys = this.$t('push')
      postMessageToSW({ translateKeys })
    })
    .then(() => {
      getUserSubscription()
        .then((subscription) => {
          if (subscription && subscriptionIdOld) {
            // tu ukve arsebobs subscription
            console.log('pn subscription2 ', subscription)
            console.log('pn subscriptionIdOld2 ', subscriptionIdOld)
            return subscription
          } else {
            return createNotificationSubscription()
          }
        })
        .then((subscription) => {
          return axios.post(pushUrl + '/subscription', { subscription, userId })
        })
        .then((response) => {
          const subscriptionId = response.data.id
          console.log('pn this.subscriptionId ', subscriptionId)
          console.log('pn response ', response)
          if (cb) {
            cb(subscriptionId)
          }
        })
    })
    .catch((err) => {
      console.log('pn something failed! ', err)
    })
}
/* // amit vqoqavt ushualod proeqtshi
function startSW() {
  const userId = 15
  const supported = isPushNotificationSupported(this)
  const subscriptionIdOld = this.$g.store.getters.subscriptionId
  if (supported) {
    initSW(subscriptionIdOld, userId, subscriptionId => {
      if (!subscriptionIdOld || subscriptionIdOld !== subscriptionId) {
        this.$g.store.commit('setSubscriptionId', subscriptionId)
      }      
    })
  }
} */
export { isPushNotificationSupported, sendNotification, initSW }
