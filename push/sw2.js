let client = null
let show = true
const timeout = 1000
function receivePushNotification(event) {
  show = true
  console.log('SW2 event:', event) // event -> PushEvent
  const data = event.data.json()
  data.title += ' ' + Date.now()
  console.log('SW2 by data:', data.title, JSON.stringify(data))
  setTimeout(() => {
    if (show) {
      console.log('SW2 show = true')
      event.waitUntil(showNotification(data))
    }
  }, timeout)
  if (client) {
    client.postMessage(data)
  }
  // event.waitUntil(showNotification(data))
}

function showNotification(data) {
  self.registration
    .getNotifications()
    .then((notifications) => {
      console.log('SW2 .then after getNotifications:', notifications)
      notifications.forEach((notification) => notification.close())
    })
    .then(() => {
      console.log('SW2 .then after showNotification:')
      self.registration.showNotification(data.title, data)
    })
}
function openPushNotification(event) {
  console.log('[Service Worker] Notification click Received.', event.notification.data)
  event.notification.close()
  event.waitUntil(self.clients.openWindow(event.notification.data))
}
self.addEventListener('push', receivePushNotification)
self.addEventListener('notificationclick', openPushNotification)
self.addEventListener('message', (event) => {
  // messages from client event ->  ExtendableMessageEvent -> {isTrusted: true, data: {…}, origin: "http://localhost:3010", lastEventId: "", source: WindowClient,…}
  console.log('sw event = ', event)
  // service worker-მა დაიმახსოვრა ეს client და აწი შეუძლია მას message-ები პირდაპირ გაუგზავნოს receivePushNotification()-იდან
  client = event.source
  /* if (!myClients.find((cl) => cl.id === client.id)) {
    myClients.push(client)
  } */
  if (event.data.url) {
    show = false
    showNotification(event.data.data)
  }
  // client.postMessage('Hi client')
})
// saitze gadasvlas rom daklikavs push-is icon-ze mashin fired 'fetch' event
/* self.addEventListener('fetch', (event) => {
  console.log('48 event ', event)
  event.waitUntil(
    (async function () {
      console.log('50 event ', event)
      // Exit early if we don't have access to the client.
      // Eg, if it's cross-origin.
      if (!event.clientId) return

      // Get the client.
      const client = await self.clients.get(event.clientId)
      // Exit early if we don't get the client.
      // Eg, if it closed.
      if (!client) return

      // Send a message to the client.
      client.postMessage({
        msg: 'Hey I just got a fetch from you!',
        url: event.request.url
      })
    })()
  )
}) */
