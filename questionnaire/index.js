let ctx // ctx = $g da ara this
// vitvlit asatvirti media-failebis raodenobas, rom davadginot bolo failis atvirtva
// let mediasAmount = 0
export function initContext(_ctx) {
  ctx = _ctx
}
// am dros teqsti tan unda gadaitargmnos kidec
export function getQuestionnaireValuesFromMongoByCardId(card, initQuestionnaire, readerLocale) {
  // aq card shemocmebulia da null an undefined ver iqnebaoo
  const requestObj = { cardId: card.id }
  // const isTester = ctx.userIsTester()
  // const isCreating = ctx.isExampleQuestionnaireCreating()
  const isExample = isExampleQuestionnairePage(card.id)
  const isReader = !!readerLocale || isExample // orderer||buyer  ctx.userIsOrderer(card)
  // let initQ = false
  if (isExample) {
    // tu locales gadavcemt, mashin sachiroebis shemtxvevashi natargmni mova
    requestObj.locale = ctx.store.getters.locale
  }
  if (readerLocale) {
    // mashin 'orderer'|'buyer'
    requestObj.locale = readerLocale
  }
  ctx.clog('iq requestObj = ', requestObj)
  ctx
    .getQuestionnaireValuesByCardId(requestObj)
    .then((questionnaireValues) => {
      ctx.clog('iq data success questionnaireValues = ', questionnaireValues)
      if (questionnaireValues) {
        let mongo = false
        // avgnishnot rom mongo-dan chamotvirtulia
        questionnaireValues.downloaded = true
        ctx.clog('iq questionnaireValues cardId = ', requestObj.cardId)
        const qvaluesLocal = getCardIdQuestionnaireValues()
        // readerLocale-ს დროს role='orderer'|'buyer'
        if (isReader) {
          mongo = true
          // initQ = true // da vicit questionnaireValues arsebobs
        } else if (qvaluesLocal && qvaluesLocal.ts >= questionnaireValues.ts) {
          //  if (isCreating || isTester) // initQ = true
          // tu localStorage-shi arsebuli questionnaireValues ufro axalia vidre chamotvirtuli, mashin mas vikenebt
          // _id gadmovitanot
          qvaluesLocal._id = questionnaireValues._id
          mongo = false
        } else {
          mongo = true
        }
        ctx.clog('iq uses questionnaireValues mongo = ', mongo)
        ctx.store.commit('setCardIndicatorParams', {
          card: ctx.store.getters.card,
          target: 'questionnaire',
          prop: 'alreadySentMediaSize',
          size: questionnaireValues.attachSize
        })
        ctx.store.commit('initQuestionnaireValues', {
          cardId: requestObj.cardId,
          data: mongo ? questionnaireValues : qvaluesLocal
        })
      } else {
        // null mashin questionnaireValues === etexts.noQuestionnaireFound
        ctx.clog('iq questionnaire.error.not_found = ', ctx.t('qchat.questionnaire.download.error.not_found'))
        // ctx.clog('iq isReader = ', isReader)
        // initQ = isCreating || isTester
        // ctx.showSnackbar( ctx.t('qchat.questionnaire.download.error.not_found'), 2000, 'red', ctx.enums.caller.questionnaire )
      }
      if (isReader && !questionnaireValues) {
        // mashin ratomghac bazashi ar aris orderer-istvis questionnaireValues
        ctx.clog('iq ratomghac bazashi ar aris orderer-istvis questionnaireValues ')
        initQuestionnaire(true)
      } else {
        initQuestionnaire()
      }
    })
    .catch((err) => {
      // ctx.clog('iq networkError error = ', err)
      // initQuestionnaire(true)
      ctx.chatEBus.$emit(ctx.enums.events.networkError, err)
      ctx.showSnackbar(err, 2000, 'red', ctx.enums.caller.questionnaire)
    })
}
// murman -> არსებული ანკეტების გაყიდვის მოდულისათვის შენგან დამჭირდება ასეთი ასინქრონული ფუნქცია: გადმოგეცემა vinLasts (ექვსი სიმბოლო), შენ მიბრუნებ მასივს შემდეგი ობიექტებისას:
// {cardId, vin (სრული), filled: {musts, mustFills, all, fills}}
export function getQStandsArrForCarlinkOrVin6(requestObj) {
  return new Promise((resolve, reject) => {
    // const requestObj = { vin6 } || { link }
    ctx
      .getQStandsArrForCarlinkOrVin6(requestObj)
      .then((qValuesArrForCarlinkOrVin6) => {
        const resArray = []
        ctx.clog('iq qValuesArrForCarlinkOrVin6 = ', qValuesArrForCarlinkOrVin6)
        if (
          qValuesArrForCarlinkOrVin6 &&
          ctx.isArray(qValuesArrForCarlinkOrVin6) &&
          qValuesArrForCarlinkOrVin6.length
        ) {
          qValuesArrForCarlinkOrVin6.forEach((qValues) => {
            const res = {
              cardId: qValues.cardId,
              vin: qValues.vin,
              locale: qValues.lang,
              stands: getQuestionnaireValidateStands(qValues)
            }
            // მხოლოდ სრულად შევსებულებს გადავცემთ
            if (qValues.filled) {
              resArray.push(res)
            }
          })
        }
        resolve(resArray)
      })
      .catch((e) => {
        reject(e)
      })
  })
}
export function getQStandsArrForVin6(vin6) {
  return getQStandsArrForCarlinkOrVin6({ vin6 })
}
export function getQStandsArrForCarlink(link) {
  return getQStandsArrForCarlinkOrVin6({ link })
}
// murmanistvis is QStands monacemebi, romlebic mongoshia atvirtuli
export function getCardIdArrQStands(cardIdArr, callback) {
  ctx.clog('iq data cardIdArr = ', cardIdArr)
  // vekitxebi chems servers, romelic ekitxeba murmanis servers
  // locales ar gadavcemt, radgan targmani ar gvchirdeba
  const requestObj = { cardId: cardIdArr }
  // tu cardIdArr carielia
  if (cardIdArr && !cardIdArr.length) {
    if (callback) {
      const stands = {}
      callback(stands)
    }
    return
  }
  ctx
    .getDataById(requestObj)
    .then((cardArr) => {
      ctx.clog('iq cardArr = ', cardArr)
      if (cardArr && cardArr.error) {
        ctx.showSnackbar(cardArr, 2000, 'red', ctx.enums.caller.questionnaire)
        ctx.clog('iq cardArr includes error = ', cardArr)
        return
      }
      // axla mongodan mogvaqvs questionnaireValuesArr
      ctx.getQuestionnaireValuesByCardId(requestObj).then((questionnaireValuesArr) => {
        // ctx.clog('i data success cardArr = ', cardArr)
        // ctx.clog('i data questionnaireValuesArr = ', questionnaireValuesArr)
        if (!questionnaireValuesArr || !ctx.isArray(questionnaireValuesArr)) {
          questionnaireValuesArr = []
        }
        const cardIdArrFromQvalues = questionnaireValuesArr.map((x) => x.cardId)
        if (cardArr) {
          // jer davtvalot cardId-ebi, romlebic aris cardIdArr-shi da ar aris cardIdArrFromQvalues-shi
          const difference = cardIdArr.filter((x) => !cardIdArrFromQvalues.includes(x))
          // ctx.clog('i difference = ', difference)
          const stands = {}
          const getAsyncStandsPromises = []
          for (const _card of cardArr) {
            if (!_card || !_card.recId || !difference.includes(_card.recId)) {
              continue
            }
            // eseni mongoshi ar aris da carielebi unda mivacodo
            const card = ctx.adjustCard(_card)
            ctx.clog('iq data card = ', card)
            getAsyncStandsPromises.push(getAsyncQuestionnaireValidateStands(card))
          }
          if (getAsyncStandsPromises.length) {
            Promise.all(getAsyncStandsPromises).then((standsArray) => {
              // standsArray -> [{ cardId, stands }, { cardId, stands }, ..]
              ctx.clog('iq standsArray = ', standsArray)
              if (standsArray && standsArray.length) {
                standsArray.forEach((standsObj) => {
                  stands[standsObj.cardId] = standsObj.stands
                })
              }
              // rac mongoshi aris chavcerot questionnaireValuesArr-ebidan
              for (const questionnaireValues of questionnaireValuesArr) {
                const cardId = questionnaireValues.cardId
                stands[cardId] = getQuestionnaireValidateStands(questionnaireValues)
              }
              if (callback) {
                callback(stands)
              }
            })
          } else {
            // rac mongoshi aris chavcerot questionnaireValuesArr-ebidan
            for (const questionnaireValues of questionnaireValuesArr) {
              const cardId = questionnaireValues.cardId
              stands[cardId] = getQuestionnaireValidateStands(questionnaireValues)
            }
            if (callback) {
              callback(stands)
            }
          }
        }
      })
    })
    .catch((err) => {
      ctx.showSnackbar(err, 2000, 'red', ctx.enums.caller.questionnaire)
      if (callback) {
        callback(err)
      }
    })
}
// atvirtuli media-ebis attachment-ebshi daamatebs relPath-ebs
function handleUploadQMediaSuccess(resultDataArray) {
  // res.data: {result: "done", meta: { ts: 1600609099979,  caller: 'questionnaire', collName: 'questionnaires', receiver:.., sender:..,status:'notSent',size: 58761, ext: "jpeg", type: "image", tsList: {…},
  // questionnaireIndex: "1.1", relPath: "/docs/1600609099979_58761.jpeg"}}
  ctx.clog('i handleUploadMediaSuccess resultDataArray = ', resultDataArray)
  // const meta = resultData.meta
  const metaArray = resultDataArray.map((resultData) => resultData.meta)
  const questionnaireValues = getCardIdQuestionnaireValues()
  const values = questionnaireValues.values
  // davamatot value.index.attachment-shi es relPath
  Object.keys(values).forEach((key) => {
    // ctx.clog('iq key = ', key)
    const itemValue = values[key]
    for (const meta of metaArray) {
      if (key === meta.questionnaireIndex) {
        // ctx.clog('iq itemValue = ', itemValue)
        // console.log('i itemValue', JSON.stringify(itemValue))
        if (!itemValue.attachments) {
          // itemValue.attachments = {}
          ctx.setObjectProperty(itemValue, 'attachments', {})
        }
        if (meta.type === 'file') {
          if (!itemValue.attachments.files) {
            // itemValue.attachments.files = []
            ctx.setObjectProperty(itemValue.attachments, 'files', [])
          }
          // itemValue.attachments.files.push(meta.relPath)
          ctx.pushToArray(itemValue.attachments.files, meta.relPath)
        } else {
          if (!itemValue.attachments.medias) {
            // itemValue.attachments.medias = []
            ctx.setObjectProperty(itemValue.attachments, 'medias', [])
          }
          // itemValue.attachments.medias.push(meta.relPath)
          ctx.pushToArray(itemValue.attachments.medias, meta.relPath)
        }
      }
    }
  })
}
function handleUploadMediaError(message, err) {
  ctx.clog('iq handleUploadMediaError message = ', message)
  ctx.clog('iq handleUploadMediaError err = ', err)
  ctx.chatEBus.$emit(ctx.enums.events.mediaUploadError, err)
  ctx.showSnackbar(ctx.t('qchat.media.upload_error'), 2000, 'red', ctx.enums.caller.questionnaire)
}
function handleUploadQuestionnaireSuccess(questionnaireId, tsArray, allMediasUploaded) {
  // და წავშლით idb-ში ყველა ფაილს tsArray-ის მიხედვით
  if (tsArray && tsArray.length) {
    const promises = []
    for (const ts of tsArray) {
      // aq ar aqvs mnishvneloba questionnaireIndex ras udris, mtavaria ikos. cashla ts-it xdeba
      promises.push(ctx._removeMedia({ questionnaireIndex: 1, media: { ts } }))
    }
    Promise.all(promises)
      .then((removeResults) => {
        ctx.clog('iq removeResults = ', removeResults) // -> [deleteId1, deleteId2,..]
        if (allMediasUploaded) {
          informUploadedStatus(questionnaireId)
        }
      })
      .catch((err) => {
        ctx.clog('iq handleUploadQuestionnaireSuccess = ', err)
      })
  } else if (allMediasUploaded) {
    informUploadedStatus(questionnaireId)
  }
}
function informUploadedStatus(questionnaireId) {
  ctx.chatEBus.$emit(ctx.enums.events.questionnaireUploaded, questionnaireId) // midis Questionnaire.vue-shi
  ctx.chatEBus.$emit(ctx.enums.events.questionnaireUploadedForUploader) // midis QuestionnaireUploader-shi
}
function handleUploadQuestionnaireError(err) {
  // ctx.clog('iq handleUploadQuestionnaireError questionnaireValues = ', questionnaireValues)
  ctx.clog('iq handleUploadQuestionnaireError err = ', err)
  ctx.chatEBus.$emit(ctx.enums.events.questionnaireUploadError, err)
  ctx.chatEBus.$emit(ctx.enums.events.questionnaireUploadErrorForUploader, err)
  // error-istvis infoDialog fanjara amodis questionnaire.vue-shi
  // ctx.showSnackbar(ctx.t('qchat.snackbar.questionnaire_upload_error'), 2000, 'red', caller.questionnaire)
}
// gaxsnis daxuravs kvela groups rom caikitxos sad aris validateError-ebi da sadac sachiroa gaacitlos
function openAndCloseAllGroups() {
  const locale = ctx.store.getters.locale
  const card = ctx.store.getters.card
  const questionnaire = ctx.store.getters.questionnaire({ locale, card })
  for (const groupItem of questionnaire.items) {
    const payload = {
      cardId: card.id,
      index: groupItem.index,
      locale
    }
    payload.open = true
    // ctx.clog('iq openAndCloseAllGroups payload = ', payload)
    ctx.store.commit('setQuestionnaireGroupOpen', payload)
    payload.open = false
    ctx.store.commit('setQuestionnaireGroupOpen', payload)
  }
}
export function valide(itemValue) {
  return !itemValue.mandatory || (itemValue.answer && itemValue.answer !== '-1')
}
function valideNotMandatory(itemValue) {
  return itemValue.disabled || (itemValue.answer && itemValue.answer !== '-1')
}
export function setQuestionnaireValidateStands() {
  ctx.clog('iq setQuestionnaireValidateStands')
  // card.questionnaireStands = questionnaireStands
  ctx.store.commit('setCardQuestionnaireStands', getQuestionnaireValidateStands())
}
export function getQuestionnaireValidateStands(questionnaireValues) {
  ctx.clog('iq getQuestionnaireValidateStands questionnaireValues1 = ', questionnaireValues)
  if (!questionnaireValues) {
    questionnaireValues = getCardIdQuestionnaireValues()
  }
  ctx.clog('iq getQuestionnaireValidateStands questionnaireValues2 = ', questionnaireValues)
  return countQValuesStands(questionnaireValues)
}
export async function getAsyncQuestionnaireValidateStands(card) {
  ctx.clog('iq getAsyncQuestionnaireValidateStands card = ', card)
  const questionnaireValues = await getAsyncCardIdQuestionnaireValues(card)
  return { cardId: card.id, stands: countQValuesStands(questionnaireValues) }
}
export function countQValuesStands(questionnaireValues) {
  // ctx.clog('iq questionnaireValues = ', questionnaireValues)
  let actualFilledFields = 0 // sul ramdenia shevsebuli
  let actualMustFilledFields = 0 // sul ramdenia shevsebuli savaldebuloebidan
  let mustFilledFields = 0 // sul savaldebulo velebi
  let allFields = 0 // sul kvela veli
  if (questionnaireValues && questionnaireValues.values) {
    const values = questionnaireValues.values
    const level = questionnaireValues.level
    // jer daemateba vin-is textfield, romelic garet aris
    allFields++
    if (level !== 'phone') {
      mustFilledFields++
    }
    ctx.clog('iq getQuestionnaireValidateStands level = ', level)
    // phone-is dros bolo 6 asoa da tan ar aris savaldebulo vin-is shevseba
    if (level !== 'phone' && ctx.valideVin(questionnaireValues.vin)) {
      actualMustFilledFields++
      actualFilledFields++
    }
    // phone-ze gvkofnis bolo 6 asoc
    if (level === 'phone' && ctx.valideVin6(questionnaireValues.vin)) {
      actualFilledFields++
    }
    Object.keys(values).forEach((key) => {
      const itemValue = values[key]
      if (key.includes('.')) {
        // only questionItem-s
        allFields++
        if (valideNotMandatory(itemValue)) {
          actualFilledFields++
        }
        if (itemValue.mandatory) {
          mustFilledFields++
          if (valide(itemValue)) {
            actualMustFilledFields++
          }
        }
      }
    })
  }
  return {
    actualFilledFields,
    actualMustFilledFields,
    mustFilledFields,
    allFields
  }
}
// gamotvlis im group-ebs, sadac aris ara valide questionItem-ebi
// questionItem-ic xmarobs
export function getValidateErrorGroupsArr() {
  const questionnaireValues = getCardIdQuestionnaireValues()
  const values = questionnaireValues.values
  const allGroupsArr = []
  const validateErrorGroupsArr = []
  ctx.clog('iq getValidateErrorGroupsArr = ')
  Object.keys(values).forEach((key) => {
    const itemValue = values[key]
    if (!key.includes('.')) {
      // isGroup groupIndex = key
      if (!allGroupsArr.includes(key)) {
        allGroupsArr.push(key)
      }
    } else if (!valide(itemValue)) {
      // questionItem-ia
      // ctx.clog('iq key = ', key)
      // ctx.clog('iq itemValue = ', itemValue)
      const groupIndex = key.split('.')[0]
      if (!validateErrorGroupsArr.includes(groupIndex)) {
        validateErrorGroupsArr.push(groupIndex)
      }
    }
    for (const groupIndex of allGroupsArr) {
      const valide = !validateErrorGroupsArr.includes(groupIndex)
      ctx.chatEBus.$emit(ctx.enums.events.valide + groupIndex, valide)
    }
  })
  ctx.store.commit('setValidateErrorGroupsArr', validateErrorGroupsArr)
  return validateErrorGroupsArr
}
export function validateQuestionnaire() {
  return new Promise((resolve, reject) => {
    openAndCloseAllGroups()
    ctx.store.commit('setValidateQuestionnaire', true)
    setTimeout(() => {
      const validateErrorGroupsArr = getValidateErrorGroupsArr()
      resolve(validateErrorGroupsArr)
    }, 0)
  })
}
function createMongoFilenameForQuestionnaireIdbMedia(idbMedia) {
  // /questionnaires/1610934797600_16506.webp
  if (!idbMedia || !idbMedia.media || !idbMedia.media.tsList) {
    return ''
  }
  return idbMedia.media.tsList.notSent + '_' + idbMedia.media.size + '.' + idbMedia.media.ext
}
function fillAllRelPathes(values, allRelPathes, allFilenames) {
  Object.keys(values).forEach((key) => {
    const itemValue = values[key]
    if (itemValue.attachments) {
      if (itemValue.attachments.medias) {
        allRelPathes.push(...itemValue.attachments.medias)
      }
      if (itemValue.attachments.files) {
        allRelPathes.push(...itemValue.attachments.files)
      }
    }
  })
  for (const relPath of allRelPathes) {
    allFilenames.push(ctx.getFilenamefromPath(relPath))
  }
}

// vadarebt ertmanets mongoshi atvirtul media-failebs da idb-shi arsebulebs
function compareQMedias(mongoQuestionnaireValues, questionnaireValues, idbMedias) {
  let mongoValues = {}
  let localValues = {}
  if (mongoQuestionnaireValues) {
    mongoValues = mongoQuestionnaireValues.values
  }
  if (questionnaireValues) {
    localValues = questionnaireValues.values
  }
  const allMongoRelPathes = []
  const allMongoFilenames = []
  const allLocalRelPathes = [] // store-ში შენახული questionnaireValues-ში არსებული RelPathes
  const allLocalFilenames = []

  const allIdbFilenames = []
  fillAllRelPathes(mongoValues, allMongoRelPathes, allMongoFilenames)
  fillAllRelPathes(localValues, allLocalRelPathes, allLocalFilenames)
  for (const idbMedia of idbMedias || []) {
    const filename = createMongoFilenameForQuestionnaireIdbMedia(idbMedia)
    allIdbFilenames.push(filename)
  }
  const resp = {}
  // idbMedias-ში რაც არის ატვირთავს
  resp.toUpload = allIdbFilenames
  // რომელიც ატვირთულია და არის mongoQuestionnaireValues-ში და questionnaireValues-ში წინ delete_ აქვს, სერვერზე წასაშლელების სიაში ჩადებს
  resp.toRemove = allMongoRelPathes.filter((x) => allLocalRelPathes.includes(ctx.gdata.deletePrefix + x))
  // /questionnaires/1620049523724_3244508.jpeg
  // davitvalot casashlelebis zoma
  let removeSize = 0
  for (const relPath of resp.toRemove) {
    removeSize += +ctx.getTsAndSizeFromFilename(relPath)[1]
  }
  resp.removeSize = removeSize
  ctx.clog('iq removeSize = ', removeSize)
  return resp
}
function createTsArrayFromIdbMedias(idbMedias) {
  let tsArray = []
  if (idbMedias && idbMedias.length) {
    tsArray = idbMedias.map((x) => x.media.tsList.notSent)
  }
  ctx.chatEBus.$emit(ctx.enums.events.questionnaireMediaStartLoading, tsArray)
  return tsArray
}
function adjustQuestionnaireValues(card, questionnaireValues, validateErrorGroupsArr) {
  ctx.clog('iq card = ', card)
  delete questionnaireValues.downloaded
  questionnaireValues.filled = !validateErrorGroupsArr.length && ctx.valideVin(questionnaireValues.vin)
  questionnaireValues.testerNickname = card.tester ? card.tester.nickname : ctx.gdata.adminNicknames[0]
  questionnaireValues.lang = ctx.store.getters.locale
}
// თუ mongo-ზე ასატვირთი და mongoQuestionnaireValues ერთიდაიგივეა, მაშინ update არ სჭირდება-თქო
export function checkUptodate() {
  const cardId = ctx.store.getters.card.id
  const requestObj = { cardId }
  try {
    return new Promise((resolve, reject) => {
      ctx
        .getQuestionnaireValuesByCardId(requestObj)
        .then((mongoQuestionnaireValues) => {
          const questionnaireValuesOrig = getCardIdQuestionnaireValues()
          const questionnaireValues = ctx.cloneObj(questionnaireValuesOrig)
          if (!questionnaireValues || !mongoQuestionnaireValues) {
            resolve(false)
          }
          const payload = { cardId, questionnaireIndex: 'all' }
          ctx.readMedias(payload).then((idbMedias) => {
            // gamovtvalot asatvirti da casashleli failebis sia
            const uploadOrRemoveMedias = compareQMedias(mongoQuestionnaireValues, questionnaireValues, idbMedias)
            if (
              JSON.stringify(questionnaireValues.values) === JSON.stringify(mongoQuestionnaireValues.values) &&
              questionnaireValues.vin === mongoQuestionnaireValues.vin &&
              !uploadOrRemoveMedias.toUpload.length
            ) {
              ctx.clog('iq questionnaire.upload.is_already_uptodate = ')
              informUploadedStatus()
              ctx.showSnackbar(
                ctx.t('qchat.questionnaire.upload.is_already_uptodate'),
                2000,
                'green',
                ctx.enums.caller.questionnaire
              )
              resolve(true)
            } else {
              resolve(false)
            }
          })
        })
        .catch((err) => {
          reject(err)
        })
    })
  } catch (err) {
    ctx.showSnackbar(ctx.t('qchat.questionnaire.upload.error.text') + err, 2000, 'red', ctx.enums.caller.questionnaire)
  }
}
/*
  ანკეტის ატვირთვისას რაც idb-შია ისინი აიტვირთება, სხვა url-იან ფაილებიდან, რომელსაც წინ delete_ უწერიათ, წაიშლება. 
  ატვირთვის შემდეგ ისევ წამოვიღებთ Qvalues, ვნახავთ,რომ უკვე სერვერზეა და ამის შემდეგ ამ ფაილებს ts-ის მიხედვით წავშლით idb-ში.
*/
export function uploadQuestionnaireToServer() {
  const card = ctx.store.getters.card
  const cardId = card.id
  // getQuestionnaireValuesByCardId-is dros targmani aq ar gvchirdeba da amitom specialurad ar vazlevt locales, rom gadatargmna ar moucios servers
  const requestObj = { cardId }
  ctx
    .getQuestionnaireValuesByCardId(requestObj)
    .then((mongoQuestionnaireValues) => {
      validateQuestionnaire().then((validateErrorGroupsArr) => {
        // im group-ebis sia, sadac error-ebia
        ctx.clog('iq validateErrorGroupsArr = ', validateErrorGroupsArr)
        const payload = { cardId, questionnaireIndex: 'all' }
        ctx.clog('iq uploadQuestionnaireToServer mongoQuestionnaireValues = ', mongoQuestionnaireValues)
        ctx.clog('iq uploadQuestionnaireToServer payload', payload)
        const ts = Date.now()
        // shenaxul da gagzavnil qValuebs ertnairi ts eqnebat
        ctx.store.commit('setQvTopProperty', { cardId, prop: 'ts', value: ts })
        const questionnaireValuesOrig = getCardIdQuestionnaireValues()
        const questionnaireValues = ctx.cloneObj(questionnaireValuesOrig)
        // თუ mongo-ზე ასატვირთი და mongoQuestionnaireValues ერთიდაიგივეა, მაშინ update არ სჭირდება-თქო
        /* if (checkUptodate(questionnaireValues, mongoQuestionnaireValues)) {
          return
        } */
        adjustQuestionnaireValues(card, questionnaireValues, validateErrorGroupsArr)
        ctx.clog('iq filled = ', questionnaireValues.filled)
        ctx.clog('iq questionnaireValues = ', questionnaireValues)
        ctx
          .readMedias(payload)
          .then((idbMedias) => {
            ctx.clog('iq getAllMediasForQuestionnaire idbMedias = ', idbMedias)
            const mongoAttachSize = (mongoQuestionnaireValues || {}).attachSize || 0
            // gamovtvalot asatvirti da casashleli failebis sia
            const uploadOrRemoveMedias = compareQMedias(mongoQuestionnaireValues, questionnaireValues, idbMedias)
            // rac mongoze atvirtulia imas vaklebt ris cashlasac vapirebt
            questionnaireValues.attachSize = mongoAttachSize - uploadOrRemoveMedias.removeSize
            ctx.clog('iq uploadOrRemoveMedias = ', uploadOrRemoveMedias)
            const tsArray = createTsArrayFromIdbMedias(idbMedias)
            ctx.clog('iq tsArray = ', tsArray)
            /* if (uploadOrRemoveMedias.toRemove.length) {
              // itemValue.attachments-დან ამოიღებს წასაშლელ ფაილებს, რომლებსაც წინ delete_ უწერიათ
              cleanQValuesDeleteMedia(questionnaireValues)
            } */
            // questionnaireValues.toRemove = uploadOrRemoveMedias.toRemove
            if (!uploadOrRemoveMedias.toUpload.length) {
              // mashin asatvirti failebi ar aris, questionnaire aris medias gareshe
              // questionnaireValuesOrig-დან ამოიღებს წასაშლელ ფაილებს, რომლებსაც წინ delete_ უწერიათ
              // removeAttachmentFilesFromQValues(questionnaireValuesOrig, questionnaireValues.toRemove)
              ctx
                .uploadQuestionnaire(questionnaireValues)
                .then((questionnaireId) => {
                  handleUploadQuestionnaireSuccess(questionnaireId, null, true)
                })
                .catch((err) => {
                  handleUploadQuestionnaireError(err)
                })
              return
            }
            // davtvalot failebis saerto size
            for (const idbMedia of idbMedias || []) {
              questionnaireValues.attachSize += idbMedia.media.size
            }
            const user = ctx.store.getters.user
            // console.log('i user = ', user)
            // davamrgvalot qvevitken
            const attachSizeRounded = Math.floor(questionnaireValues.attachSize / 1000000)
            const mediaMaxSize = user.mediaMaxSize.tester
            // shevamocmot failebis saerto zomis gadameteba
            if (attachSizeRounded > mediaMaxSize) {
              ctx.chatEBus.$emit(ctx.enums.events.mediaMaxSizeExeeded, { attachSizeRounded, mediaMaxSize })
              return
            }
            ctx.store.commit('setQvTopProperty', {
              cardId,
              prop: 'attachSize',
              value: questionnaireValues.attachSize
            })
            // chavcert rigrigobit questionnaireValues.values['1.1',...].attachments-shi
            // vajgufebt suratebs index-ebis mixedvit da calcalke avtvirtavt titoeuli index-ebis jgufs
            const uploadmediaPromises = {}
            for (const idbMedia of idbMedias) {
              const filename = createMongoFilenameForQuestionnaireIdbMedia(idbMedia)
              // tu asatvirtebis siashi ar aris continue
              if (!uploadOrRemoveMedias.toUpload.includes(filename)) {
                continue
              }
              /* for (const idbMedia of idbMedias) { */
              ctx.clog('iq uploadQuestionnaireToServer idbMedia ', idbMedia)
              // const questionnaireIndex = idbMedia.cardIdAndQuestionnaireIndex.split('_')[1]
              const questionnaireIndex = idbMedia.questionnaireIndex
              // const lastBlob = i === idbMedias.length - 1
              const media = idbMedia.media
              const caller = cardId < 0 ? ctx.enums.caller.examplemedias : ctx.enums.caller.questionnaire
              const message = {
                // cardId: idbMedia.cardId,
                data: { blob: idbMedia.media.blob },
                meta: {
                  ts: idbMedia.ts,
                  size: media.size,
                  intent: ctx.enums.intents.uploadQuestionnaire,
                  collName: ctx.enums.collnames.questionnaires,
                  caller,
                  questionnaireIndex,
                  ext: media.ext,
                  type: media.type,
                  tsList: { [ctx.enums.status.notSent]: media.tsList[ctx.enums.status.notSent] }
                }
              }
              ctx.adjustMessage(message)
              // uploadmediaPromises -> { '1.1': [promise1, promise2..],'3.2': [promise1, promise2..],.. }
              if (uploadmediaPromises[questionnaireIndex]) {
                uploadmediaPromises[questionnaireIndex].push(ctx.uploadMedia({ message }))
              } else {
                uploadmediaPromises[questionnaireIndex] = [ctx.uploadMedia({ message })]
              }
            }
            ctx.clog('iq uploadmediaPromises = ', uploadmediaPromises)
            if (Object.keys(uploadmediaPromises).length) {
              // es function recursive mushaobs
              uploadMediasFromOneQIndex(uploadmediaPromises, 0, { card, validateErrorGroupsArr })
            }
          })
          .catch((err) => {
            ctx.clog('iq uploadQuestionnaireToServer = ', err)
          })
      })
    })
    .catch((err) => {
      // console.log('am catch e: ', err)
      // shecdoma amas exeba -> getQuestionnaireValuesByCardId
      ctx.showSnackbar(err, 2000, 'red', ctx.enums.caller.questionnaire)
      handleUploadQuestionnaireError(err)
    })
}
// es titoeul suratebis jgufs qIndex-ebidan calcalke atvirtavs
function uploadMediasFromOneQIndex(uploadmediaPromises, i, { card, validateErrorGroupsArr }) {
  const keys = Object.keys(uploadmediaPromises)
  const length = keys.length
  // sheizleba qIndex-ebi areuli ikos da amitom sort()-> 1.1, 1.2, ...
  keys.sort()
  if (i < length) {
    const uploadmediaQIndexPromises = uploadmediaPromises[keys[i]]
    ctx.clog('iq keys[i] = ', keys[i])
    Promise.all(uploadmediaQIndexPromises)
      .then((resultDataArray) => {
        // amati array-ia -> res.data: {result: "done", meta: { ts: 1600609099979,  caller: 'questionnaire', collName: 'questionnaires', receiver:.., sender:..,status:'notSent',size: 58761, ext: "jpeg", type: "image", tsList: {…}, questionnaireIndex: "1.1", relPath: "/docs/1600609099979_58761.jpeg"}}
        ctx.clog('iq resultDataArray = ', resultDataArray)
        const uploadedTsArray = resultDataArray.map((x) => x.meta.ts)
        // atvirtuli media-ebis attachment-ebshi daamatebs relPath-ebs
        handleUploadQMediaSuccess(resultDataArray)
        // aq xelaxla vizaxebt,radgan attachments-ebshi handleUploadQMediaSuccess-is dros cvlilebebi moxda
        const questionnaireValues = ctx.cloneObj(getCardIdQuestionnaireValues())
        // delete questionnaireValues.download, ...
        adjustQuestionnaireValues(card, questionnaireValues, validateErrorGroupsArr)
        // if (i === length - 1) {
        ctx.clog('iq bolos avtvirtot tvit Questionnaire questionnaireValues = ', questionnaireValues)
        // avtvirtot tvit Questionnaire. აქ ნაწილ-ნაწილ აიტვირთება questionnaireIndex-ების მიხედვით, რომ უინტერნეტობის
        // შემთხვევაში ანკეტის ერთი ჯგუფიდან ატვირთული სურათების მეორედ ატვირთვა არ მოგვიწიოს
        ctx
          .uploadQuestionnaire(questionnaireValues)
          .then((questionnaireId) => {
            // თან წავშლით idb-ში ყველა ფაილს tsArray-ის მიხედვით
            handleUploadQuestionnaireSuccess(questionnaireId, uploadedTsArray, i === length - 1)
          })
          .catch((err) => {
            handleUploadQuestionnaireError(err)
          })
        // }
      })
      .then((res) => {
        ctx.clog('iq res = ', res)
        uploadMediasFromOneQIndex(uploadmediaPromises, i + 1, { card, validateErrorGroupsArr })
      })
      .catch((err) => {
        handleUploadMediaError(err)
      })
  }

  // uploadmediaQIndexPromises.then(uploadMediasFromOneQIndex(i + 1, length))
}
export function initItemValue(questionnaireIndex, itemValue) {
  const questionnaireValues = getCardIdQuestionnaireValues()
  ctx.clog('iq questionnaireValues = ', questionnaireValues)
  if (!questionnaireValues) {
    return {}
  }
  const stringifiedItemValue = JSON.stringify(questionnaireValues.values[questionnaireIndex])
  // tu itemValue-s mnishvneloba namdvilad sheicvala, mashin davakopirot
  if (stringifiedItemValue !== JSON.stringify(itemValue)) {
    ctx.clog('iq stringifiedItemValue = ', stringifiedItemValue)
    return stringifiedItemValue ? JSON.parse(stringifiedItemValue) : itemValue
  } else return itemValue
}

export function getCardIdQuestionnaireValues() {
  if (!ctx.store.getters.card) {
    return null
  }
  const cardId = ctx.store.getters.card.id
  // return qValues
  return ctx.store.getters.questionnaireValues[cardId]
}
export async function getAsyncCardIdQuestionnaireValues(card) {
  let cardId
  if (!card) {
    if (!ctx.store.getters.card) {
      return null
    }
    cardId = ctx.store.getters.card.id
  }
  if (card) {
    cardId = card.id
  }
  let qValues = ctx.store.getters.questionnaireValues[cardId]
  ctx.clog('iq qValues = ', qValues)
  if (card && (!qValues || !qValues.values)) {
    // getQuestionnaireValidateStands()- ma gamoizaxa da jer ar sheqmnila quiestionnaireValues da shevqmnat
    ctx.clog('iq getQuestionnaireValidateStands()- ma gamoizaxa da jer ar sheqmnila qValues da shevqmnat carieli')
    const vt = getQuestionnaireVersionAndType(card)
    const questionnaire = await initQuestionnaireItems(card.level, vt.version, vt.type)
    // initQuestionnaireItems(card.level, vt.version, vt.type).then((questionnaire) => {
    const values = initQuestionnaireValueItems(questionnaire)
    qValues = {
      cardId: card.id,
      version: vt.version,
      level: card.level,
      values
    }
    ctx.clog('iq qValues2= ', qValues)
    return qValues
    // })
  }
  return qValues
}

export function getQuestionnaireVersionAndType(card) {
  if (!card || !card.level) {
    return { version: null, type: null }
  }
  let version
  let type
  ctx.clog('iq card.level = ', card.level)
  if (card.level === ctx.enums.levels.phone) {
    version = ctx.gdata.versions.questionnaire.phone
    type = ctx.enums.questionnaireTypes.phone
  } else {
    version = ctx.gdata.versions.questionnaire.deep
    type = ctx.enums.questionnaireTypes.deep
  }
  return { version, type }
}
export function initQuestionnaireValueItems(questionnaire) {
  if (!questionnaire) {
    return {}
  }
  ctx.clog('iq questionnaire = ', questionnaire)
  const values = {}
  for (const group of questionnaire.items) {
    values[group.index] = {}
    for (const item of group.children) {
      // itemValue-mac unda icodes mandatory-ia tu ara
      values[item.index] = { mandatory: item.mandatory }
    }
  }
  return values
}
async function readQuestionnaireRemotefile(questionnaireVersion, questionnaireType, filetype, notCached) {
  // 'questionnaires/' + type + '/', version
  // თუ relPath-ს ბოლოში დავამატებთ ?q=Date.now()-ს მაშინ cache-ს აღარ გამოიყენებს და სულ საიტიდან წაიკითხავს
  const relPath =
    'questionnaires/' +
    questionnaireType +
    '/' +
    questionnaireVersion +
    '.' +
    filetype + // json || js
    (notCached ? '?q=' + Date.now() : '')
  return await ctx.readActualVersionsRemoteFile(relPath)
}
// aq locale aris chat-is da questionnaire-is shignit rom vxmarobt is locale, romelzec dakveta iqna gacemuli
export async function initQuestionnaireItems(
  level,
  questionnaireVersion,
  questionnaireType,
  locale = ctx.store.getters.locale
) {
  // let locale = ctx.store.getters.locale
  const questionnaire = {}
  ctx.clog('iq questionnaireVersion ', questionnaireVersion)
  // მოდის javascript object
  const jsFile = await readQuestionnaireRemotefile(questionnaireVersion, questionnaireType, 'js')
  // eslint-disable-next-line no-eval
  const jsObj = eval(jsFile)
  ctx.clog('iq jsObj ', jsObj)
  const questionGroups = jsObj.questionGroups
  questionnaire.version = jsObj.version
  questionnaire.indicator = 0
  const langs = jsObj.langs
  if (!langs.includes(locale)) {
    locale = ctx.gdata.defaultLanguage
  }
  questionnaire.lang = locale
  questionnaire.yesConnections = jsObj.yesConnections
  // const title = advanced ? json.advanced.title[locale] : json.basic.title[locale]
  const title = jsObj[level].title[locale]
  ctx.clog('iq json.advanced.headerParagraphs = ', jsObj[level].headerParagraphs)
  // const headerParagraphs = advanced ? json.advanced.headerParagraphs[locale] : json.basic.headerParagraphs[locale]
  const headerParagraphs = jsObj[level].headerParagraphs[locale]
  questionnaire.title = title
  // questionnaire.cardId = ctx.store.getters.card.id
  questionnaire.subtitle = ''
  for (const el of headerParagraphs) {
    questionnaire.subtitle += el + '</br>'
  }
  questionnaire.items = []
  for (let i = 0; i < questionGroups.length; i++) {
    const it = questionGroups[i]
    // const it = obj[i + 1]
    const group = {}
    group.isGroup = true
    // group.index = i + 1
    group.index = it.index
    // გამყიდველი წინ ინდექსს აღარ ვუწერთ
    // group.title = it.index + '. ' + it.title[locale]
    group.title = it.title[locale]
    // ctx.clog('group.name ', group.name)
    group.note = it.note ? it.note[locale] : null
    // group.advanced = it.advanced
    // s.show = false
    group.children = []
    questionnaire.items.push(group)
    for (let j = 0; j < it.questions.length; j++) {
      // qi-> questionItem, qel-> questionElement
      const qel = it.questions[j]
      const qi = {}
      // el.index = i + 1 + '.' + (j + 1)
      qi.index = qel.index
      qi.isQuestion = true
      qi.title = qel.question[locale]
      qi.note = qel.note ? qel.note[locale] : null
      // el.advanced = l.advanced
      qi.mandatory = qel.mandatory
      qi.evaluateColor = qel.evaluateColor
      qi.commentHeader = jsObj.commentHeader[locale]
      // aq chkvianurad ceria da 'basic' ase scoria
      if (level !== ctx.enums.levels.basic || !qel.advanced) {
        group.children.push(qi)
      }
    }
  }
  // radgan async aris, es dabrundeba rogorc promise
  return questionnaire
}
export function isExampleQuestionnairePage(cardId) {
  const qed = ctx.store.getters.questionnaireExampleData || {}
  return cardId < 0 && !qed.exampleBasic && !qed.exampleAdvanced && !qed.examplePhone
}
export function isExampleQuestionnaireCreating() {
  const qed = ctx.store.getters.questionnaireExampleData || {}
  ctx.clog('iq qed ', JSON.stringify(qed))
  return qed.exampleBasic || qed.exampleAdvanced || qed.examplePhone
}
export function isAdminNickname(user, adminNicknames) {
  // const user = this.$user // murmanidan momaqvs
  if (!user || !adminNicknames) {
    return false
  }
  let nickName
  if (user.profile && user.profile.nickName && user.profile.nickName.nickName) {
    nickName = user.profile.nickName.nickName
  }
  if (nickName && adminNicknames.find((nickname) => nickname === nickName)) {
    return true
  }
  return false
}
export function resetQed() {
  const qed = ctx.cloneObj(ctx.store.getters.questionnaireExampleData)
  qed.exampleBasic = false
  qed.exampleAdvanced = false
  qed.examplePhone = false
  ctx.store.commit('setQuestionnaireExampleData', qed)
}
export function questionnaireIsDisabled() {
  const card = ctx.store.getters.card
  // tu cheker-ია ანკეტის შექმნის პროცესში ან გვინდა ExampleQuestionnaire შევქმნათ
  if (isExampleQuestionnaireCreating() || (card && !card.closed && card.id > 0)) {
    return false
  }
  return true
}
