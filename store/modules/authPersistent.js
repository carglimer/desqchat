// import { slugify } from '@/helpers/help'
export default {
  state: () => {
    return {
      // user: null, // { userId, serverData: {mediaMaxSize: 200, ip: ...}, profile : {} }
      // isAuthenticated: true,
      token: null,
      loggedIn: false,
      fileSendMode: 'siofu', // 'siofu' an 'stream'
      chatUrl: null, // login mitteilt  chatAuth-shia kvelaferi
      imageSrcBase: null, // login supplies
      fotosUploadUrl: null, // login supplies
      avatarSrcDefault: null, // login supplies
      uploadFileMaxSize: 2, // login suppplies
      uploadFileChunkSize: 1024 * 1000, // login supplies
      flagsImageBase: null, // login supplies
      messageTsFormat: 'DD.MM.YY HH:mm' // login supplies
    }
  },
  getters: {
    /* messageTsFormat(state) {
      return state.messageTsFormat
    }, */
    /* isAuthenticated(state) {
      // return state.user && state.user.tokenInfo && state.user.tokenInfo.tokenExpiration - Date.now() > 0
      return true
    },
    user(state) {
      return state.user
    }, */
    token(state) {
      return state.token
    },
    loggedIn(state) {
      return state.loggedIn
    },
    fileSendMode(state) {
      return state.fileSendMode
    },
    chatUrl(state) {
      return state.chatUrl
    },
    imageSrcBase(state) {
      return state.imageSrcBase
    },
    flagsImageBase(state) {
      return state.flagsImageBase
    },
    fotosUploadUrl(state) {
      return state.fotosUploadUrl
    },
    avatarSrcDefault(state) {
      return state.avatarSrcDefault
    },
    uploadFileMaxSize(state) {
      return state.uploadFileMaxSize
    },
    uploadFileChunkSize(state) {
      return state.uploadFileChunkSize
    }
  },
  mutations: {
    /* setUser(state, user) {
      state.user = user
    }, */
    setUserParticipantId(state, participantId) {
      state.user.participantId = participantId
    },
    deleteUserParticipantId(state) {
      state.user && delete state.user.participantId
    },
    setUserAndServerData(state, data) {
      if (!data) {
        return
      }
      try {
        // state.isAdmin = data.user.data.admin
        state.user = data.user
        if (state.user && state.user.tokenInfo) {
          state.user.tokenInfo.tokenExpiration = new Date().getTime() + state.user.tokenInfo.expiresIn * 1000
        }
        if (data.serverData) {
          state.chatUrl = data.serverData.chatUrl
          state.fileSendMode = data.serverData.fileSendMode
          state.imageSrcBase = data.serverData.imageSrcBase
          state.fotosUploadUrl = data.serverData.fotosUploadUrl
          state.flagsImageBase = data.serverData.flagsImageBase
          state.avatarSrcDefault = data.serverData.avatarSrcDefault
          state.messageTsFormat = data.serverData.messageTsFormat
          state.uploadFileMaxSize = data.serverData.uploadFileMaxSize
          if (data.serverData.uploadFileChunkSize) {
            state.uploadFileChunkSize = data.serverData.uploadFileChunkSize
          }
        }
      } catch (error) {
        // console.log('error setUserAndServerData: ', error)
      }
    },
    setMessageTsFormat(state, messageTsFormat) {
      state.messageTsFormat = messageTsFormat
    },
    setFileSendMode(state, fileSendMode) {
      state.fileSendMode = fileSendMode
    },
    setChatUrl(state, chatUrl) {
      state.chatUrl = chatUrl
    },
    setAvatarSrcDefault(state, avatarSrcDefault) {
      state.avatarSrcDefault = avatarSrcDefault
    },
    setImageSrcBase(state, imageSrcBase) {
      state.imageSrcBase = imageSrcBase
    },
    setFlagsImageBase(state, flagsImageBase) {
      state.flagsImageBase = flagsImageBase
    },
    setFotosUploadUrl(state, fotosUploadUrl) {
      state.fotosUploadUrl = fotosUploadUrl
    },
    setUploadFileMaxSize(state, uploadFileMaxSize) {
      state.uploadFileMaxSize = uploadFileMaxSize
    },
    setUploadFileChunkSize(state, uploadFileChunkSize) {
      state.uploadFileChunkSize = uploadFileChunkSize
    },
    setToken(state, token) {
      if (state.user && state.user.tockenInfo) {
        state.user.tockenInfo.token = token
      }
    },
    setTokenExpiration(state, expiresIn) {
      if (state.user && state.user.tokenInfo) {
        state.user.tokenInfo.tokenExpiration = new Date().getTime() + state.user.tokenInfo.expiresIn * 1000
      }
      /* setTimeout(() => {
        state.token = null
      }, state.tokenExpiration)    */
    },
    setLoggedIn(state, user) {
      state.loggedin = true
      state.user = user
    },
    setLoggedOut(state) {
      state.loggedin = false
      // state.user = null // droebit satestod aris gamortuli
      state.token = null
      state.tokenExpiration = 0
    }
  }
}
