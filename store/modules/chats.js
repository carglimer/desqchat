export default {
  state: () => {
    return {
      languages: ['de', 'en', 'ka', 'ru'],
      testUser: null,
      testUsers: [],
      testCards: [],
      showSnackbar: {}, // showSnackbar = { ts: Date.now(), payload: { text, timeout, color } }
      socketArrowColor: 'red',
      typingState: 'offFocus', // typing||onFocus||offFocus. roca igzavneba,  amas usmens chatCore
      typingMessage: null, // roca vighebt, amas usmens TypingMessage.vue
      consolexDialog: false,
      consoleText: ''
    }
  },
  getters: {
    languages(state) {
      return state.languages
    },
    consoleText(state) {
      return state.consoleText
    },
    consolexDialog(state) {
      return state.consolexDialog
    },
    typingMessage(state) {
      return state.typingMessage
    },
    typingState(state) {
      return state.typingState
    },
    socketArrowColor(state) {
      // console.log('state.socketArrowColor = ', state.socketArrowColor)
      return state.socketArrowColor
    },
    showSnackbar(state) {
      return state.showSnackbar
    },
    testUsers(state) {
      return state.testUsers
    },
    testUser(state) {
      return state.testUser
    },
    testCards(state) {
      return state.testCards
    }
  },
  mutations: {
    pushToArray(state, payload) {
      // payload -> { arr: message, value: meta.msgId }
      // console.log('c payload = ', payload)
      if (!payload.arr.includes(payload.value)) {
        payload.arr.push(payload.value)
      }
    },
    setConsoleText(state, consoleText) {
      state.consoleText = consoleText
    },
    setConsolexDialog(state, open) {
      state.consolexDialog = open
    },
    setObjectProperty(state, payload) {
      // payload -> { obj: message, key: '_id', value: meta.msgId }
      const obj = payload.obj
      obj[payload.key] = payload.value
    },
    setObjectProperties(state, payload) {
      // payload -> { obj: message, arr: [ { key: '_id', value: meta.msgId }, { key: '_id2', value: meta2 },..  ]  }
      const obj = payload.obj
      const arr = payload.arr
      for (const item of arr) {
        obj[item.key] = item.value
      }
    },
    setTypingMessage(state, typingMessage) {
      // console.log('c typingMessage = ', typingMessage)
      state.typingMessage = typingMessage
    },
    setTypingState(state, typingState) {
      state.typingState = typingState
    },
    setSocketArrowColor(state, socketArrowColor) {
      state.socketArrowColor = socketArrowColor
    },
    showSnackbar(state, payload) {
      // aq ts imitom ceria, Snackbar.vue-shi watcher mixvdes cvlilebas
      state.showSnackbar = { ts: Date.now(), payload }
    },
    setTestUser(state, testUser) {
      state.testUser = testUser
    },
    setTestUsers(state, testUsers) {
      state.testUsers = testUsers
    },
    setTestCards(state, testCards) {
      state.testCards = testCards
    }
  },
  actions: {}
}
