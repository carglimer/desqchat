export default {
  state: () => {
    return {
      stepNr: 1,
      messageList: [],
      pendingMessageLists: {}, // gaugzavneli message-ebistvis, sanam interneti ar aqvs { cardId1:[ message1, message2,..], cardId2:[..],.. }
      socket: null,
      fontWasWarned: {}
    }
  },
  getters: {
    fontWasWarned(state) {
      return state.fontWasWarned
    },
    socket(state) {
      return state.socket
    },
    messageList(state) {
      return state.messageList
    },
    pendingMessageLists(state) {
      return state.pendingMessageLists
    },
    stepNr(state) {
      return state.stepNr
    }
  },
  mutations: {
    setFontWasWarned(state, { userId, fontWasWarned }) {
      if (userId) {
        state.fontWasWarned[userId] = fontWasWarned
      }
    },
    clearMessageList(state) {
      state.messageList = []
    },
    clearPendingMessageLists(state, cardId) {
      state.pendingMessageLists[cardId] = []
    },
    addToPendingMessageList(state, message) {
      const cardId = message.cardId
      if (!cardId) {
        return
      }
      if (!state.pendingMessageLists[cardId]) {
        state.pendingMessageLists[cardId] = []
      }
      const cardPendings = state.pendingMessageLists[cardId]
      // if not contains, mashin davamatot
      const i = cardPendings.findIndex((o) => o.meta.tsList.notSent === message.meta.tsList.notSent)
      if (i < 0) {
        cardPendings.push(message)
      } /* else {
        cardPendings[i] = message
      } */
    },
    removePendingMessageFromList(state, message) {
      const cardId = message.cardId
      if (!cardId) {
        return
      }
      if (!state.pendingMessageLists[cardId]) {
        state.pendingMessageLists[cardId] = []
      }
      const cardPendings = state.pendingMessageLists[cardId]
      const i = cardPendings.findIndex((o) => o.meta.tsList.notSent === message.meta.tsList.notSent)
      if (cardPendings[i]) {
        cardPendings.splice(i, 1)
      }
    },
    removePendingMessageByIdbMediaFromList(state, idbMedia) {
      const cardId = idbMedia.cardId
      if (!cardId) {
        return
      }
      if (!state.pendingMessageLists[cardId]) {
        state.pendingMessageLists[cardId] = []
      }
      const cardPendings = state.pendingMessageLists[cardId]
      const i = cardPendings.findIndex((o) => o.ts === idbMedia.ts)
      if (cardPendings[i]) {
        cardPendings.splice(i, 1)
      }
    },
    addToMessageList(state, messageOrArray) {
      // jer shevamocmot tu aris es message siashi
      // to contains, mashin shevcvalot axlit
      // const i = state.messageList.findIndex(o => o.meta.tsList.notSent === message.meta.tsList.notSent)
      const i = state.messageList.findIndex(
        (o) => (o.meta ? o : o[0]).ts === (messageOrArray.meta ? messageOrArray : messageOrArray[0]).ts
      )
      if (i > -1) {
        state.messageList[i] = messageOrArray
      } else {
        state.messageList.unshift(messageOrArray)
      }
      // console.log('cP state.messageList = ', state.messageList)
    },

    setSocket(state, socket) {
      state.socket = socket
    },
    setMessageList(state, messageList) {
      state.messageList = messageList
    },
    setMessageList2(state, messageList) {
      // const _messageList = []
      const newMessageList = []
      // console.log('cP messageList = ', messageList)
      for (const message of messageList) {
        if (message.meta.ext && message.meta.group) {
          let firstInGroupExists = false
          for (const msg of state.messageList) {
            if (msg.meta.group && msg.meta.group.contains(message.meta.tsList.notSent)) {
              // mashin pirveli am group-idan ukve arsebobs
              msg.groupMessages.push(message)
              firstInGroupExists = true
            }
          }
          // tu es message pirvelia am group-idan
          if (!firstInGroupExists) {
            // shevqmnat axali groupMessages
            message.groupMessages = []
            newMessageList.push(message)
          }
        } else {
          // type = text || system
          newMessageList.push(message)
        }
      }
      // console.log('cP newMessageList = ', newMessageList)
      state.messageList = newMessageList
    },
    setStepNr(state, stepNr) {
      state.stepNr = stepNr
    }
  },
  actions: {}
}
