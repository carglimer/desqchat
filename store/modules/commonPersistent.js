export default {
  // namespaced: true,
  state: () => {
    return {
      locale: 'de',
      // es aris aqtualuri amorcheuli card
      card: {},
      user: null, // { userId, serverData: {mediaMaxSize: 200, ip: ...}, profile : {} }
      loadingParams: { uploading: false, tsToProgress: {}, caller: null }, // LoaderCircular-istvis
      // tu packageVersion-shi sheicvala meore ricxvi, mashin store-shi sheicvala raime da klientis localStorage-s reset unda gavuketot
      // ra aqtualuri versiebia: tviton qchat da questionnaire
      qchatVersions: null,
      subscriptionId: null,
      // invoices -> ინვოისის ედიტირების შედეგები     { cardId: { editableData:{..}}, .. }
      invoices: {},
      constraints: {
        // video: { facingMode: { exact: 'environment' } },
        video: true,
        audio: false,
        videoData: { agree: true, width: 555, height: 550 }
      },
      debug: false
    }
  },
  getters: {
    debug(state) {
      return state.debug
    },
    subscriptionId(state) {
      return state.subscriptionId
    },
    locale(state) {
      return state.locale
    },
    invoices(state) {
      return state.invoices
    },
    constraints(state) {
      return state.constraints
    },
    loadingParams(state) {
      return state.loadingParams
    },
    qchatVersions(state) {
      return state.qchatVersions
    },
    user(state) {
      return state.user
    },
    card(state) {
      return state.card
    }
  },
  mutations: {
    setDebug(state, debug) {
      state.debug = debug
    },
    setSubscriptionId(state, subscriptionId) {
      state.subscriptionId = subscriptionId
    },
    setEditableData(state, { cardId, editableData }) {
      // console.log('state.invoices = ', state.invoices)
      state.invoices[cardId] = editableData
    },
    setEditableDataKey(state, { cardId, key, value }) {
      if (!state.invoices[cardId]) {
        return
      }
      state.invoices[cardId][key] = value
    },
    setConstraints(state, constraints) {
      state.constraints = constraints
    },
    setLocale(state, locale) {
      state.locale = locale
    },
    setKeyToLoadingParams(state, { key, value }) {
      state.loadingParams[key] = value
    },
    setLoadingParams(state, loadingParams) {
      state.loadingParams = loadingParams
    },
    setQchatVersions(state, qchatVersions) {
      state.qchatVersions = qchatVersions
    },
    setUser(state, user) {
      state.user = user
    },
    setCard(state, card) {
      state.card = card
    },
    setCardIndicatorParams(state, { card, target, prop, size }) {
      // console.log('cp card = ', card)
      card.indicator[target][prop] = size
    },
    setCardQuestionnaireStands(state, questionnaireStands) {
      if (state.card && state.card.indicator) {
        state.card.indicator.questionnaireStands = questionnaireStands
      }
    },
    setCardIndicatorChat(state, indicator) {
      state.card.indicator.chat = indicator
    },
    setCardIndicatorQuestionnaire(state, indicator) {
      state.card.indicator.questionnaire = indicator
    },
    addCard(state, card) {
      state.cards.push(card)
    },
    removeCard(state, card) {
      const index = state.cards.indexOf(card)
      if (index !== -1) state.cards.splice(index, 1)
    }
  },
  actions: {}
}
