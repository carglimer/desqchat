export default {
  // namespaced: true,
  state: () => {
    return {
      /* languages: ['de', 'en', 'ka'],
      locale: 'de', */
      localeOther: 'de',
      windowWidth: 0,
      breakpoint: ''
    }
  },
  getters: {
    /* locale(state) {
      return state.locale
    },    
    languages(state) {
      return state.languages
    }, */
    localeOther(state) {
      return state.localeOther
    }
  },
  mutations: {
    /* setLocale(state, locale) {
      // console.log('7 locale= ' + locale)
      state.locale = locale
    },
    setLanguages(state, locale) {
      // console.log('7 locale= ' + locale)
      state.locale = locale
    }, */
    setLocaleOther(state, localeOther) {
      state.localeOther = localeOther
    },
    SET_WIDTH(state, windowWidth) {
      // console.log('11 windowWidth= '+windowWidth)
      state.windowWidth = windowWidth
      let width = 'xs'
      if (state.windowWidth < 576) {
        width = 'xs'
      } else if (state.windowWidth < 768) {
        width = 'sm'
      } else if (state.windowWidth < 992) {
        width = 'md'
      } else if (state.windowWidth < 1200) {
        width = 'lg'
      } else if (state.windowWidth >= 1200) {
        width = 'xl'
      }
      state.breakpoint = width
    }
  },
  actions: {}
}
