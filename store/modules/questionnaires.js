export default {
  state: () => {
    return {
      validateQuestionnaire: false
      // openAll: false // treeview open all groups
    }
  },
  getters: {
    validateQuestionnaire(state) {
      return state.validateQuestionnaire
    } /* ,
    openAll(state) {
      return state.openAll
    } */
  },
  mutations: {
    setValidateQuestionnaire(state, validateQuestionnaire) {
      state.validateQuestionnaire = validateQuestionnaire
    } /* ,
    setOpenAll(state, openAll) {
      state.openAll = openAll
    } */
  },
  actions: {}
}
