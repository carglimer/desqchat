export default {
  state: () => {
    return {
      // questionnaires-> არის ცარიელი მთლიანი კითხვარები ყველა ენაზე და ორივე ტიპის
      // გადმოიწერება questionnaire.json ფაილებიდან. თითოეულ ენას ცალკე ფაილი აქვს, რომელშიც
      // სამივე ტიპი advanced, basic, phone -ია მოთავსებული
      // { advanced: { de: {}, en: {},..}, basic: { de: {}, en: {},..} }

      questionnaires: { advanced: {}, basic: {}, phone: {} },

      /* qestionnaireValues -> ანკეტის შევსების შედეგები        
        { cardId1: { რაც ქვევით წერია}, cardId2: {...} }
        id,         ბაზაში მიენიჭება
        cardId,       ბარათის მონაცემები        
        version,    ანკეტის რომელი ვერსიით შეივსო
        lang,       რა ენაში შეივსო
        testerId,   ვინ შეავსო
        size,  მაგ. 117345, სულ რა ზომის არის
        ts,  როდის შეივსო. ატვირთვის დროს დაედება ეს ts
        values:
          {
            '1.1': {
              answer: true||false||undefined, <- checkbox-ების შევსება
              comment: 'tratratratra..'
              attachments: {
                files: [file1, file2,..],
                medias: [media1, media2,..]
              }
            }, ...
          }        
      */
      questionnaireValues: {},

      /* ordererQuestionnaireValues -> ნაყიდი ან შევსებული ანკეტის შედეგები        
        { questionnaireId1: { რაც სულ ზევით წერია}, questionnaireId2: {...} }
        
      */

      openArr: [],
      validateErrorGroupsArr: [],
      // example questionnairebisatvis
      questionnaireExampleData: {
        userId: -2,
        exampleBasic: false,
        exampleAdvanced: false,
        examplePhone: false,
        basic: {
          cardId: -2
        },
        advanced: {
          cardId: -3
        },
        phone: {
          cardId: -4
        }
      }
    }
  },
  getters: {
    getVin: (state) => (cardId) => {
      const questionnaireValues = state.questionnaireValues[cardId]
      if (questionnaireValues) {
        return questionnaireValues.vin
      }
      return null
    },
    questionnaireExampleData(state) {
      return state.questionnaireExampleData
    },
    validateErrorGroupsArr(state) {
      return state.validateErrorGroupsArr
    },
    openArr(state) {
      return state.openArr
    },
    questionnaire:
      (state) =>
      ({ locale, card }) => {
        // console.log('qs t state.questionnaires = ', state.questionnaires)
        let questionnaire = state.questionnaires[card.level][locale]
        // console.log('qs questionnaire = ', questionnaire)
        if (!questionnaire) {
          questionnaire = {}
        }
        return questionnaire
      },
    questionnaires: (state) => (card) => {
      // let questionnaires = state.questionnaires[card.level]
      /* if (card.level === 'advanced') {
        questionnaires = state.questionnaires.advanced
      } else questionnaires = state.questionnaires.basic */
      return state.questionnaires[card.level]
    },
    questionnaireValues(state) {
      return state.questionnaireValues
    },
    // Non Cached Getter ase icereba
    cardIdQuestionnaireValues: (state) => (card) => {
      // console.log('qs card ', card)
      //  console.log('qs state.questionnaireValues ', state.questionnaireValues)
      return state.questionnaireValues[card.id]
    }
  },
  mutations: {
    // არ ვხმარობთ
    setOpenArr(state, openArr) {
      state.openArr = openArr
    },
    setQuestionnaireExampleData(state, questionnaireExampleData) {
      state.questionnaireExampleData = questionnaireExampleData
    },
    // im group-ebis sia, romlebshic aris error-iani anu sheuvsebeli questionItem-ebi
    setValidateErrorGroupsArr(state, validateErrorGroupsArr) {
      state.validateErrorGroupsArr = validateErrorGroupsArr
    },
    setQuestionnaires(state, questionnaires) {
      state.questionnaires = questionnaires
    },
    setQuestionnaireValues(state, questionnaireValues) {
      state.questionnaireValues = questionnaireValues
    },
    // payload = { cardId: '123', prop: 'vin|attachSize|uploadId', value}  }
    setQvTopProperty(state, payload) {
      // payload = { cardId: '123', vin: '1wdb9066331s3215487'}  }
      const questionnaireValues = state.questionnaireValues[payload.cardId]
      if (questionnaireValues) {
        questionnaireValues[payload.prop] = payload.value
      }
    },
    deleteQuestionnaires(state) {
      state.questionnaires = { advanced: {}, basic: {}, phone: {} }
      state.questionnaireValues = {}
    },
    addQuestionnaire(state, { questionnaire, card }) {
      // console.log('qs addQuestionnaire')
      // console.log('qs t state.questionnaires = ', state.questionnaires)
      const questionnaires = state.questionnaires[card.level]
      /* if (card.level === 'advanced') {
        questionnaires = state.questionnaires.advanced
      } else {
        questionnaires = state.questionnaires.basic
      } */
      questionnaires[questionnaire.lang] = questionnaire
    },
    setQuestionnaireItemValues(state, payload) {
      // console.log('qs setQuestionnaireItemValues payload = ', payload)
      // payload = { cardId: '123', index: '1.1', data: { key1: value1, key2: value2, ..}  }
      /* values:
          {
            '1.1': {
              show: false,
              answer: true||false||undefined, <- checkbox-ების შევსება
              comment: 'tratratratra..'

              attachments idb-shia
              attachments: { 
                files: [file1, file2,..],
                medias: [media1, media2,..]
              }
            }, ...
          }   */
      const questionnaireValues = state.questionnaireValues[payload.cardId]
      // console.log('qs questionnaireValues = ', questionnaireValues)
      if (questionnaireValues) {
        const values = questionnaireValues.values
        const data = payload.data
        // const indexValue = { ...values[payload.index] }
        const indexValue = values[payload.index]
        // console.log('qs indexValue = ', indexValue)
        if (indexValue) {
          Object.keys(data).forEach((key) => {
            const value = data[key]
            indexValue[key] = value
          })
        }
        // console.log('qs indexValue = ', indexValue)
        // mivanichot axali obieqti
        // values[payload.index] = { ...indexValue }
      }
    },
    setQValuesAttachSize(state, { cardId, attachSize }) {
      const questionnaireValues = state.questionnaireValues[cardId]
      // console.log('qs questionnaireValues = ', questionnaireValues)
      if (questionnaireValues) {
        questionnaireValues.attachSize = attachSize
      }
    },
    initQuestionnaireValues(state, payload) {
      // payload = { cardId: '123', data: questionnaireValues  }
      // questionnaireValues initializebas an actualizebas vaxdent
      const questionnaireValues = state.questionnaireValues[payload.cardId]
      if (!questionnaireValues) {
        // initialize
        state.questionnaireValues[payload.cardId] = payload.data
      } else {
        // actualize
        for (const [key, value] of Object.entries(payload.data)) {
          questionnaireValues[key] = value
        }
      }
      // state.questionnaireValues[payload.cardId] = payload.data
      // console.log('qs state.questionnaireValues = ', state.questionnaireValues)
    },
    /* 
      "yesnoConnections": {
         "1.1": {
           "yes": "1.3"
         }
      }
    */
    checkYesnoConnections(state, payload) {
      // console.log('qs payload = ', payload)
      const questionnaire = state.questionnaires[payload.card.level][payload.locale]
      /* if (payload.card.level === 'advanced') {
        questionnaire = state.questionnaires.advanced[payload.locale]
      } else questionnaire = state.questionnaires.basic[payload.locale] */
      // console.log('qs questionnaire = ', questionnaire)
      const yesConnections = questionnaire.yesConnections
      // console.log('qs yesConnections = ', yesConnections)
      // const noConnections = questionnaire.noConnections
      if (!yesConnections) {
        return
      }
      for (const key in yesConnections) {
        if (key === payload.index) {
          const targetIndex = yesConnections[key]
          const questionnaireValues = state.questionnaireValues[payload.card.id]
          const targetItem = questionnaireValues.values[targetIndex]
          targetItem.disabled = payload.answer === '1'
          targetItem.answer = '-1'
        }
      }
    },
    openOrCloseAllGroups(state, payload) {
      // payload = { cardId: '123', open: true, allGroupIndexesArray: ['1','2',..], locale: 'de' }
      const questionnaireValues = state.questionnaireValues[payload.cardId]
      // questionnaireValues.openArr = payload.allGroupIndexesArray
      if (payload.open) {
        questionnaireValues.openArr = payload.allGroupIndexesArray
      } else {
        questionnaireValues.openArr = []
      }
      state.openArr = questionnaireValues.openArr
      // console.log('qs state.openArr = ', state.openArr)
    },
    setQuestionnaireGroupOpen(state, payload) {
      // payload = { cardId: '123', open: true, index: 3, locale: 'de' }
      // console.log('qs payload = ', payload)
      const questionnaireValues = state.questionnaireValues[payload.cardId]
      if (payload.isMobile) {
        // mashin mxolod 1 tabs vxsnit
        questionnaireValues.openArr = []
        if (payload.open) {
          questionnaireValues.openArr = [payload.index]
        }
      } else {
        if (!questionnaireValues.openArr) {
          questionnaireValues.openArr = []
        }
        const arr = questionnaireValues.openArr
        if (payload.open) {
          if (!arr.includes(payload.index)) {
            arr.push(payload.index)
          }
        } else {
          // console.log('qs payload.close ', arr)
          const index = arr.indexOf(payload.index)
          if (index !== -1) arr.splice(index, 1)
        }
      }
      state.openArr = questionnaireValues.openArr
      // questionnaire.openArr = [...arr]
    },
    closeGroupsOtherQuestionItems(state, payload) {
      // payload = { cardId: '123', index: 3.1, locale: 'de' }
      const questionnaireValues = state.questionnaireValues[payload.cardId]
      if (questionnaireValues && questionnaireValues.values && payload.index && payload.index.includes('.')) {
        // console.log('qp payload ', payload)
        const groupIndex = payload.index.split('.')[0]
        Object.keys(questionnaireValues.values).forEach((key) => {
          const itemValue = questionnaireValues.values[key]
          if (key.includes('.') && key.split('.')[0] === groupIndex && key !== payload.index && itemValue.show) {
            // console.log('qp itemValue ', itemValue)
            itemValue.show = false
            payload.ctx.$g.chatEBus.$emit(payload.ctx.$enums.events.hideQuestionItem + key)
          }
        })
      }
    }
  },
  actions: {}
}
