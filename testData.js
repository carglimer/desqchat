import { isMobileBrowser } from './helpers/methodsCtx'
export default class TestData {
  constructor(store, gdata) {
    this.store = store
    this.gdata = gdata
  }

  init() {
    const users = []
    const pcuser = this.user1()
    const mobuser1 = this.user2()
    const mobuser2 = this.user3()
    users.push(pcuser)
    users.push(mobuser1)
    users.push(mobuser2)
    const cards = []
    cards.push(this.card1())
    cards.push(this.card2())
    cards.push(this.card3())
    cards.push(this.card4())
    this.store.commit('setTestUsers', users)
    this.store.commit('setTestCards', cards)
    if (isMobileBrowser()) {
      this.store.commit('setTestUser', mobuser1)
    } else {
      this.store.commit('setTestUser', pcuser)
    }
  }

  user1() {
    // console.log('td this.gdata.baseUrl', this.gdata.baseUrl)
    const user = {
      recId: 201,
      nickName: 'pc99',
      mediaMaxSize: {
        orderer: 200,
        tester: 200,
        handler: 200
      }
    }
    return user
  }

  user2() {
    const user = {
      recId: 202,
      nickName: 'mob202',
      mediaMaxSize: {
        orderer: 200,
        tester: 200,
        handler: 200
      }
    }
    return user
  }

  user3() {
    const user = {
      recId: 203,
      nickName: 'mob203',
      mediaMaxSize: {
        orderer: 200,
        tester: 200,
        handler: 200
      }
    }
    return user
  }

  card1() {
    const card = {
      recId: 123,
      level: 'basic',
      tester: {
        recId: 202,
        nickName: 'mob202'
      },
      orderer: {
        recId: 201,
        nickName: 'pc99'
      }
    }
    return card
  }

  card2() {
    const card = {
      recId: 124,
      level: 'advanced',
      tester: {
        recId: 201,
        nickName: 'pc99'
      },
      orderer: {
        recId: 202,
        nickName: 'mob202'
      }
    }
    return card
  }

  card3() {
    const card = {
      recId: 125,
      level: 'phone',
      tester: {
        recId: 201,
        nickName: 'pc99'
      },
      orderer: {
        recId: 202,
        nickName: 'mob202'
      }
    }
    return card
  }

  card4() {
    const card = {
      recId: 126,
      level: 'phone',
      tester: {
        recId: 203,
        nickName: 'mob203'
      },
      orderer: {
        recId: 201,
        nickName: 'pc99'
      }
    }
    return card
  }

  card5() {
    const card = {
      recId: 127,
      level: 'advanced',
      tester: {
        recId: 202,
        nickname: 'mob202'
      },
      orderer: {
        recId: 203,
        nickname: 'mob203'
      }
    }
    return card
  }

  testInvoice() {
    const invoice = {
      recId: 126,
      level: 'advanced',
      tester: {
        recId: 202,
        nickname: 'mob202'
      },
      orderer: {
        recId: 203,
        nickname: 'mob203'
      }
    }
    return invoice
  }
}
