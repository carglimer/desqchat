/* eslint-disable prettier/prettier */
// import Vue from 'vue'

// export const eventBus = new Vue()
export const dummyImage = {
  status: 'dummy',
  dropZoneHeight: 170
}
export const imageStatus = {
  DUMMY: 'dummy',
  NOT_UPLOADED: 'not_uploaded',
  UPLOADING: 'uploading',
  UPLOADED: 'uploaded',
  FAILED: 'failed'
}
export const animations = {
  HEART_BEAT: 'heartBeat',
  ZOOM_IN: 'zoomIn',
  ZOOM_OUT: 'zoomOut'
}

export const submitFileMixIn = {
  data() {
    return {
      errorIn: false,
      uploadPercentage: 0,
      // animatedType: '',
      imageStatus,
      imagesCounter: 0, // imis dasatvlelad, tu kvela surati aitvirta an daerorda
      animations
    }
  },
  methods: {
    handleUploadPercentage(uploadPercentage) {
      this.uploadPercentage = uploadPercentage
    },
    handleSuccess(image, res, afterUploadCallback) {
      this.imagesCounter++
      // this.hasError = false
      image.status = imageStatus.UPLOADED
      this.configButtons(image, false, true)
      this.$g.clog('image= ' + image.file.name)
      // this.$con('image= ' + image.file.name)
      this.$g.clog('SUCCESS!! ' + JSON.stringify(res))
      if (res && res.data) {
        image.relpath = res.data.relpath
      }
      this.$g.clog('image.relpath ', image.relpath)
      this.$g.clog('image.isLastImage ', image.isLastImage)
      if (afterUploadCallback && this.imagesCounter === this.images.length) {
        // mashin atvirtvis bolo suratia
        afterUploadCallback(this.images)
      }
    },
    handleErr(image, afterUploadCallback) {
      this.imagesCounter++
      image.status = imageStatus.FAILED
      this.$g.clog('handleUploadError!! ')
      this.errorIn = true
      // this.animatedType = animations.HEART_BEAT
      this.$g.clog('image.animatedType= ' + image.animatedType)
      if (afterUploadCallback && this.imagesCounter === this.images.length) {
        // mashin atvirtvis bolo suratia
        afterUploadCallback(this.images)
      }
      setTimeout(() => {
        this.errorIn = false
        // this.animatedType = animations.ZOOM_OUT
        this.configButtons(image, false, false)
        // this.$con("image.animatedType= " + image.animatedType)
        this.$g.clog('image.file.name= ' + image.file.name)
      }, 0)
    },
    configButtons(image, removeDisable, uploadDisable) {
      image.removeDisable = removeDisable
      image.uploadDisable = uploadDisable
    },
    // uploaUrl = 'https://us-central1-fb-cloud-functions1.cloudfunctions.net/uploadFile'
    submitFile(image, afterUploadCallback) {
      this.$g.clog('h this.$g.store.getters.user = ' + this.$g.store.getters.user)
      if (image.status === imageStatus.DUMMY || !this.$g.store.getters.user) {
        return
      }
      this.$g.clog('h image.status= ' + image.status)
      image.status = imageStatus.UPLOADING
      image.avatar = false
      this.configButtons(image, true, true)
      const payload = {
        image,
        user: this.$g.store.getters.user,
        handleErr: this.handleErr,
        handleSuccess: this.handleSuccess,
        handleUploadPercentage: this.handleUploadPercentage,
        afterUploadCallback
      }
      this.$g.clog('payload ', JSON.stringify(payload))
      // uploadFoto(this, payload)
    },
    loadPicFromPc(e, maxSize) {
      this.$g.clog('loadFileFromPc! ', e.srcElement)
      const files = e.target.files || e.dataTransfer.files
      if (!files.length) return
      this.$g.clog('h files.length= ' + files.length)

      for (let i = 0; i < files.length; i++) {
        const file = files[i]
        if (!file.type.match('image')) continue
        // this.createImage(item, files[0])
        const image = {}
        image.file = file
        this.$g.clog('h file.length= ' + file)
        image.status = imageStatus.NOT_UPLOADED
        const reader = new FileReader()
        let canvas, ctx, img2
        reader.onloadend = (e) => {
          // image.src = reader.result
          const img = new Image()
          img.src = reader.result
          img.onload = () => {
            let width = img.naturalWidth || img.width
            let height = img.naturalHeight || img.height
            this.$g.clog('img.width ', width)
            this.$g.clog('img.height ', height)
            const origSize = (img.src.length * 3) / 4
            if (maxSize && origSize >= maxSize) {
              const koeff = +Math.sqrt(origSize / maxSize).toFixed(2) // koeff >= 1
              // koeff = 1
              this.$g.clog('koeff ', koeff)
              const maxWidth = width / koeff
              const maxHeight = height / koeff
              this.$g.clog('maxWidth ', maxWidth)
              this.$g.clog('maxHeight ', maxHeight)
              width = width / koeff
              height = height / koeff
              if (!canvas) {
                canvas = document.createElement('canvas')
              }
              canvas.width = width
              canvas.height = height
              if (!ctx) {
                ctx = canvas.getContext('2d')
              }
              if (!img2) {
                img2 = document.createElement('img')
              }
              this.$g.clog('width ', width)
              this.$g.clog('height ', height)
              ctx.drawImage(img, 0, 0, width, height)
              const dataurl = canvas.toDataURL(image.file.type)
              // const dataurl = canvas.toDataURL('image/jpeg')
              this.$g.clog('dataurl.length ', dataurl.length)
              image.src = dataurl
              this.$g.clog('h image.ratio= ' + img.src.length / dataurl.length)
            }
            image.width = width
            image.height = height
            image.ratio = width / height // ** 2
            this.images.push(image)
          }
          image.src = img.src
          this.$g.clog('h image.src.length= ' + image.src.length)
          // boloscinaze dasvams
          // this.images.splice(this.images.length - 1, 0, image)
          // item.image = reader.result
        }
        reader.readAsDataURL(file)
      }
    }
  }
}
