// video recorder playType size
const global = {}
export function stopStream() {
  // this.$g.clog('window.stream ', window.stream)
  this.$g.clog('global.video.srcObject ', global.video && global.video.srcObject)
  if (global.video && global.video.srcObject) {
    global.video.srcObject.getTracks().forEach((track) => {
      track.stop()
    })
    global.video.srcObject = null
  }
}
// video aris html tag video
// canvas aris html tag canvas, sadac daixateba videodan gadaghebuli surati
export function snapshot(canvas, cb) {
  // const video = document.getElementById('monitor')
  // const canvas = document.getElementById('photo')
  this.$g.clog('global.video.srcObject ', global.video.srcObject)
  this.$g.clog('global.video.videoWidth ', global.video.videoWidth)
  canvas.width = global.video.videoWidth
  canvas.height = global.video.videoHeight
  /* if (size) {
      canvas.width = size.width // video.videoWidth
      canvas.height = size.height // video.videoHeight
    } */
  // shutter.onclick = () => canvas.getContext('2d').drawImage(video, 0, 0);
  // canvas.getContext('2d').drawImage(video, (video.videoWidth - size.width), (video.videoHeight - size.height))
  canvas.getContext('2d').drawImage(global.video, 0, 0)
  const image = {
    isPhoto: true,
    src: canvas.toDataURL(),
    width: canvas.width,
    height: canvas.height
  }
  this.$g.clog('image ', image)
  stopStream()
  this.$g.clog('image2 ')
  cb(image) // ->imageSrc
  // })
  /* } catch (err) {
    console.error(err)
  } */
}

export function startVideo(videoData) {
  this.$g.clog('startVideo navigator.mediaDevices = ' + navigator.mediaDevices)
  // const videoEl = document.querySelector('video')
  // const audioSelectEl = document.querySelector('select#audioSource')
  // const videoSelectEl = document.querySelector('select#videoSource')
  /* videoEl.onloadeddata = function() {
    ctx.dialog = true
  } */
  // const videoEl = videoData.videoEl
  global.video = videoData.videoEl
  const audioSelectEl = videoData.audioSelectEl
  const videoSelectEl = videoData.videoSelectEl
  global.size = videoData.size

  if (!navigator.mediaDevices) {
    // window.alert(this.$g.t('qchat.snackbar.no_media_devices'))
    this.$g.showSnackbar(this.$g.t('qchat.snackbar.no_media_devices'), 2000, 'error')
    this.$g.clog('No media divices was found! ')
    return
  }
  for (const el in navigator.mediaDevices) {
    this.$g.clog('mediaDevice ', el)
  }
  navigator.mediaDevices
    .enumerateDevices()
    .then(addDevices)
    .then(getStream)
    // .then((ctx.dialog = true))
    .catch(handleError)

  if (audioSelectEl) {
    audioSelectEl.onchange = getStream
  }
  if (videoSelectEl) {
    videoSelectEl.onchange = getStream
  }
  // ctx.dialog = true
  function addDevices(deviceInfos) {
    for (let i = 0; i !== deviceInfos.length; ++i) {
      const deviceInfo = deviceInfos[i]
      this.$g.clog('deviceInfo ', deviceInfo)
      // deviceInfo.deviceId = i
      const option = document.createElement('option')
      option.value = deviceInfo.deviceId
      if (audioSelectEl && deviceInfo.kind === 'audioinput') {
        option.text = deviceInfo.label || 'microphone ' + (audioSelectEl.length + 1)
        audioSelectEl.appendChild(option)
      } else if (videoSelectEl && deviceInfo.kind === 'videoinput') {
        option.text = deviceInfo.label || 'camera ' + (videoSelectEl.length + 1)
        videoSelectEl.appendChild(option)
      } else {
        this.$g.clog('Found another kind of device: ', deviceInfo)
      }
    }
  }
  function handleError(error) {
    console.error('Error: ', error)
  }
}
function getStream() {
  // stopStream()
  this.$g.clog('getStream ')
  let audio = false
  let video = true
  if (global.playType) {
    audio = global.playType.audio
    video = global.playType.video
  }
  // constraints aq aris api -> https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia
  const constraints = { audio, video }
  // video: { width: { exact: 500 }, height: { exact: 520 } }
  if (global.size) {
    constraints.video.width = {
      width: { exact: global.size.width },
      height: { exact: global.size.height }
    }
  }
  navigator.mediaDevices
    .getUserMedia(constraints)
    .then(gotStream)
    .then((data) => this.$g.clog('gotStream ', data))
    .catch(handleError)

  function gotStream(stream) {
    // window.stream = stream // make stream available to console
    global.video.srcObject = stream
    global.video.captureStream = global.video.captureStream || global.video.mozCaptureStream
    // global.videoStream = stream
    return new Promise((resolve) => (global.video.onplaying = resolve))
  }
  function handleError(error) {
    console.error('Error: ', error)
  }
}

// aqedan aris -> https://developer.mozilla.org/en-US/docs/Web/API/MediaStream_Recording_API/Recording_a_media_element
// const videoRecordingMaxTimeMS = 5000

function startRecordingProcess(stream, lengthInMS) {
  let timeout
  const wait = (delayInMS) => {
    return new Promise((resolve) => {
      timeout = setTimeout(resolve, delayInMS)
    })
  }
  global.recorder = new MediaRecorder(stream)
  const data = []
  global.recorder.ondataavailable = (event) => {
    // event.data -> es erti didi blob obieqtia
    if (event.data) {
      data.push(event.data)
      this.$g.clog('data.push ', event.data)
      // cavshalot maqsimaluri drois brzaneba rom gvaqvs micemuli
      if (timeout) {
        this.$g.clog('clearTimeout ')
        clearTimeout(timeout)
        timeout = null
      }
    }
  }
  global.recorder.start()
  this.$g.clog(global.recorder.state + ' for ' + lengthInMS / 1000 + ' seconds...')
  const stopped = new Promise((resolve, reject) => {
    global.recorder.onstop = resolve
    global.recorder.onerror = (event) => reject(event.name)
  })
  const recorded = wait(lengthInMS).then(() => {
    stopRecorder()
    return Promise.resolve(recorded).then(() => data)
  })
  // Promise.race nishnavs romelic moascrebs
  return Promise.resolve(stopped).then(() => data)
}
export function stopRecorder() {
  this.$g.clog('global.recorder.state ', global.recorder.state)
  // recorder.stop() fires recorder.ondataavailable -> data -> callback addImage
  global.recorder && global.recorder.state === 'recording' && global.recorder.stop()
}
export function stop(stream) {
  stream.getTracks().forEach((track) => track.stop())
}
// recordingData = { image, downloadButton, videoRecordingMaxTimeMS }
export function startRecording(recordingData) {
  const addImage = recordingData.cb
  global.playType = recordingData.playType
  // getStream()
  const image = recordingData.image
  const downloadButton = recordingData.downloadButton
  const videoRecordingMaxTimeMS = recordingData.videoRecordingMaxTimeMS
  startRecordingProcess(global.video.captureStream(), videoRecordingMaxTimeMS)
    .then((recordedChunks) => {
      this.$g.clog('recordedChunks ', recordedChunks)
      const recordedBlob = new Blob(recordedChunks, { type: 'video/webm' })
      this.$g.clog('recordedBlob.size ', recordedBlob.size)
      image.src = URL.createObjectURL(recordedBlob)
      image.isVideo = true
      image.size = recordedBlob.size
      image.type = recordedBlob.type
      downloadButton.href = image.src
      downloadButton.download = 'RecordedVideo.webm'
      addImage()
      this.$g.clog('Successfully recorded ' + recordedBlob.size + ' bytes of ' + recordedBlob.type + ' media.')
    })
    .catch((err) => {
      this.$g.clog('err ', err)
    })
}

export function startSimpleVideo(size) {
  this.$g.clog('startVideo navigator.mediaDevices = ' + navigator.mediaDevices)
  /* const constraints = {
    video: true
  } */
  const hdConstraints = {
    video: { width: { exact: size.width }, height: { exact: size.height } }
  }
  const video = document.querySelector('video')
  navigator.mediaDevices.getUserMedia(hdConstraints).then((stream) => {
    video.srcObject = stream
  })
}
/* const constraints = {
    audio: {
      // deviceId: { exact: audioSelectEl.value }
    },
    video: {
      // deviceId: { exact: videoSelectEl.value }
      // { width: 1280, height: 720 }
    }
  } */
/* function capture() {
  this.captureIcon = 'mdi-circle-outline'
  // setTimeout(() => (this.captureIcon = 'mdi-camera-iris'), 200)
  this.showCanvas = true
  this.$nextTick(() => {
    this.canvas = this.$refs.canvas
    const canvas = this.$refs.canvas
    this.canvas.getContext('2d').drawImage(this.video, 0, 0, this.size.canvasWidth, this.size.canvasHeight)
    this.imageAdder(canvas.toDataURL('image/webp'))
    this.showCanvas = false
    this.fotoStream && this.fotoStream.getTracks().forEach((track) => track.stop())
    this.open = false
  })
} */
// videoEl aris tvit html tag video
// audioSelectEl aris combobox audio device-ebis chamohatvalit
// videoSelectEl aris combobox video device-ebis chamohatvalit
// videoEl, audioSelectEl, videoSelectEl, size
